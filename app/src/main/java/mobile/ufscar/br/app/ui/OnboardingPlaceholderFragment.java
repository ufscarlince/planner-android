package mobile.ufscar.br.app.ui;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import mobile.ufscar.br.app.R;

/**
 * As páginas de {@link mobile.ufscar.br.app.ui.OnboardingActivity} são mapeadas
 * para essa classe, visando assim a reutilização das {@link android.view.View}
 * criadas de cada página
 *
 * @see mobile.ufscar.br.app.ui.OnboardingPagerAdapter
 */

public class OnboardingPlaceholderFragment extends Fragment {

//    Argumento que representa o numero da pagina
    private static final String ARG_SECTION_NUMBER = "section_number";

    public OnboardingPlaceholderFragment() {
    }

    /**
     * Método que retorna a instancia do fragmento solicitado
     *
     * @param sectionNumber Número da página
     * @return instancia do fragment
     */
    public static OnboardingPlaceholderFragment newInstance(int sectionNumber) {

        OnboardingPlaceholderFragment fragment = new OnboardingPlaceholderFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = null;

        if (getArguments().getInt(ARG_SECTION_NUMBER) == 1) {
            rootView = inflater.inflate(R.layout.fragment_onboarding_one, container, false);
        } else if (getArguments().getInt(ARG_SECTION_NUMBER) == 2) {
            rootView = inflater.inflate(R.layout.fragment_onboarding_two, container, false);
        }
        return rootView;
    }
}