package mobile.ufscar.br.app.logic.receivers;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import java.util.Calendar;

import mobile.ufscar.br.app.config.Constants;
import mobile.ufscar.br.app.logic.WidgetAlarmReceiver;
import mobile.ufscar.br.app.utils.Utility;

/**
 * Recria o timer do Widget caso o tempo do sistema seja alterado.
 */

public class TimeChangeReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(final Context context, Intent intent) {
        Intent myIntent = new Intent(context, WidgetAlarmReceiver.class);
        PendingIntent mPendingIntent = PendingIntent.getBroadcast(context,
                Constants.DailyWidget.WIDGET_REQUEST_CODE, myIntent, Constants.DailyWidget.WIDGET_FLAG);
        AlarmManager alarmManager = (AlarmManager)context.getSystemService(
                Context.ALARM_SERVICE);
        alarmManager.cancel(mPendingIntent);
        mPendingIntent = PendingIntent.getBroadcast(context, Constants.DailyWidget.WIDGET_REQUEST_CODE,
                myIntent, Constants.DailyWidget.WIDGET_FLAG);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.roll(Calendar.DAY_OF_MONTH, true);
        calendar.set(Calendar.HOUR_OF_DAY, Constants.DailyWidget.WIDGET_UPDATE_HOUR);
        calendar.set(Calendar.MINUTE, Constants.DailyWidget.WIDGET_UPDATE_MINUTE);
        alarmManager.setRepeating(AlarmManager.RTC, calendar.getTimeInMillis(),
                AlarmManager.INTERVAL_DAY, mPendingIntent);
        Utility.updateWidget(context);
    }
}