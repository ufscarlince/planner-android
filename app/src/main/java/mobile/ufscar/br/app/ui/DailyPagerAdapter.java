package mobile.ufscar.br.app.ui;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import mobile.ufscar.br.app.R;
import mobile.ufscar.br.app.logic.UfscarApplication;

/**
 * Um {@link FragmentPagerAdapter} usado para controlar as tabs da dailyVision, cada tab é uma
 * instância de {@link DailyVisionTabFragment} referente ao dia da semana
 */

public class DailyPagerAdapter extends FragmentStatePagerAdapter {

    private List<String> mTabsNames;

    public DailyPagerAdapter(FragmentManager fm) {
        super(fm);
        mTabsNames = new ArrayList<>();
        String tabNames[] = UfscarApplication.getInstance().getResources().
                getStringArray(R.array.days_of_the_week_array);
        mTabsNames.addAll(Arrays.asList(tabNames));
    }

    @Override
    public Fragment getItem(int position) {
        return DailyVisionTabFragment.getInstance(position);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mTabsNames.get(position);
    }

    @Override
    public int getCount() {
        return mTabsNames.size();
    }
}