package mobile.ufscar.br.app.ui;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.AutocompletePredictionBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLngBounds;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

import mobile.ufscar.br.app.R;
import mobile.ufscar.br.app.utils.Utility;

/**
 * {@link ArrayAdapter} modificado para receber e tratar os resultados provenientes da
 * GooglePlaces API.
 */

public class PlacesAutoCompleteAdapter
        extends ArrayAdapter<PlacesAutoCompleteAdapter.PlaceAutocomplete> implements Filterable {

    private static final String TAG = "PlaceAutocomplete";
    private static final String STARTING_AUTO_COMPLETE_QUERY =
            "Iniciando a consulta do autocomplete para: ";
    private static final String ERROR_GETTING_AUTOCOMPLETE_PREDICTION_CALL =
            "Erro ao pegar a chamada da previsão do autocomplete: ";
    private static final String QUERY_COMPLETED = "Consulta completada. Recebido ";
    private static final String PREDICTIONS = " previsões.";
    private static final String GOOGLE_API_CLIENT_NOT_CONNECTED =
            "Cliente Google API não está conectado para consulta no autocomplete";

//    Resultados retornados pelo adapter.
    private ArrayList<PlaceAutocomplete> mResultList;
//    Administra as requisições do Autocomplete.
    private GoogleApiClient mGoogleApiClient;
//    Limites territorias usados como referencia para as requisições da Places Geo Data autocomplete
//    API.
    private LatLngBounds mBounds;
//    Filtro para o autocomplete utilizado para restringir as consultas para um tipo específico de
//    lugares.
    private AutocompleteFilter mPlaceFilter;
    private View mView;

    /**
     * Inicializa com recurso para linhas de texto e limites de consulta do autocomplete.
     *
     * @see ArrayAdapter#ArrayAdapter(Context, int)
     */
    public PlacesAutoCompleteAdapter (Context context, int resource, GoogleApiClient googleApiClient,
                                      LatLngBounds bounds, AutocompleteFilter filter, View view) {
        super(context, resource);
        mGoogleApiClient = googleApiClient;
        mBounds = bounds;
        mPlaceFilter = filter;
        mView = view;
    }

    /**
     * Define limites para todas as consultas subsequentes.
     */
    public void setBounds (LatLngBounds bounds) {
        mBounds = bounds;
    }

    /**
     * Retorna o número de resultados recebidos na última consulta no autocomplete.
     */
    @Override
    public int getCount() {
        return mResultList.size();
    }

    /**
     * Retorna um item da última consulta no autocomplete.
     */
    @Override
    public PlaceAutocomplete getItem(int position) {
        return mResultList.get(position);
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
//                Pula a consulta do autocomplete se nenhuma restrição é dada.
                if (constraint != null) {
//                    Consulta a API do autocomplete para a string de restrição de busca.
                    mResultList = getAutocomplete(constraint);
                    if (mResultList != null) {
//                        A API conseguiu retornar resultados.
                        results.values = mResultList;
                        results.count = mResultList.size();
                    }
                }
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0) {
//                    A API retornou ao menos um resultado, atualiza os dados.
                    notifyDataSetChanged();
                } else {
//                    A API não retornou nenhum resultado, invalida o conjunto de dados.
                    notifyDataSetInvalidated();
                }
            }
        };
        return filter;
    }

    private ArrayList<PlaceAutocomplete> getAutocomplete(CharSequence constraint) {
        if (mGoogleApiClient.isConnected()) {
            Log.i(TAG, STARTING_AUTO_COMPLETE_QUERY + constraint);

//            Envia a consulta para a API do autocompplete e recebe um PendingIntent que vai conter
//            os resultados quando a consulta se completar.
            PendingResult<AutocompletePredictionBuffer> results =
                    Places.GeoDataApi
                            .getAutocompletePredictions(mGoogleApiClient, constraint.toString(),
                                    mBounds, mPlaceFilter);

//             Este método deve ter sido chamado fora da main UI Thread. Bloquear e esperar por,
//             no máximo, 60s para um resultado da API.
            AutocompletePredictionBuffer autocompletePredictions = results
                    .await(60, TimeUnit.SECONDS);

//            Confirma que a consulta foi completada com sucesso, caso contrário retorna null.
//            A constante 7 se refere ao StatusCode NETWORK_ERROR
            final Status status = autocompletePredictions.getStatus();
            if (!status.isSuccess()) {
                if(status.getStatusCode() != 7) {
                    Utility.showSnackbar(mView, getContext().getResources()
                            .getString(R.string.places_auto_complete_error_contacting_api) +
                            status.toString());
                }

                Log.e(TAG, ERROR_GETTING_AUTOCOMPLETE_PREDICTION_CALL + status.toString());
                autocompletePredictions.release();
                return null;
            }

            Log.i(TAG, QUERY_COMPLETED + autocompletePredictions.getCount()
                    + PREDICTIONS);

//            Copia os resultados na nossa própria estrutura de dado porque não podemos segurar no
//            buffer. O objeto AutoCompletePrediction encapsula a resposta da API
//            (place_ID e description).
            Iterator<AutocompletePrediction> iterator = autocompletePredictions.iterator();
            ArrayList resultList = new ArrayList<>(autocompletePredictions.getCount());
            while (iterator.hasNext()) {
                AutocompletePrediction prediction = iterator.next();
//                Pega os resultados da previsão e copia em um novo objeto Placeautocomplete.
                resultList.add(new PlaceAutocomplete(prediction.getPlaceId(),
                        prediction.getFullText(null)));
            }

//            Libera o buffer agora que todos os dados foram copiados.
            autocompletePredictions.release();

            return resultList;
        }
        Log.e(TAG, GOOGLE_API_CLIENT_NOT_CONNECTED);
        return null;
    }

    /**
     * Holder para os resultados da Places Geo Data Autocomplete API.
     */
    public class PlaceAutocomplete {

        public CharSequence placeId;
        public CharSequence description;

        PlaceAutocomplete(CharSequence placeId, CharSequence description) {
            this.placeId = placeId;
            this.description = description;
        }

        @Override
        public String toString() {
            return description.toString();
        }
    }
}
