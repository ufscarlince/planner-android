package mobile.ufscar.br.app.logic;

import java.util.List;

import mobile.ufscar.br.app.model.EventModel;
import mobile.ufscar.br.app.model.UfscarResponseError;

/**
 * Callback usado por {@link DefermentService} quando houve sucesso ou não em buscar o deferimento
 * do SIGA.
 */

public interface EventServiceListener {

    /**
     * Callback quando houve sucesso em buscar o deferimento
     *
     * @param eventModelList Lista com os eventos buscados no deferimento.
     */
    void loadCompleted(List<EventModel> eventModelList);

    /**
     * Callback quando não houve sucesso em buscar o deferimento
     *
     * @param error erro da requisição
     */
    void loadFailed(UfscarResponseError error);
}