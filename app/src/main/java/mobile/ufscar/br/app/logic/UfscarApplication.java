package mobile.ufscar.br.app.logic;

import android.app.PendingIntent;
import android.content.Intent;

import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;
import mobile.ufscar.br.app.config.ConstantsActions;
import mobile.ufscar.br.app.model.ConfigurationModel;

public class UfscarApplication extends VolleyManager {

    private static UfscarApplication sInstance;

    public static UfscarApplication getInstance() {
        if (sInstance == null) {
            sInstance = new UfscarApplication();
        }
        return sInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        sInstance = this;

        if (CacheManager.getInstance().getConfigCache() == null) {
            CacheManager.getInstance().setConfigCache(new ConfigurationModel(true, false));
        }

        boolean alarmOff = (PendingIntent.getBroadcast(this, 0,
                new Intent(ConstantsActions.CHECK_CARDAPIO), PendingIntent.FLAG_NO_CREATE) == null);
        if (alarmOff) {
//            Seta o alarm para o próximo horário
            sendBroadcast(new Intent(ConstantsActions.MENU_IS_UP_TO_DATE_INTENT));
        }
    }
}