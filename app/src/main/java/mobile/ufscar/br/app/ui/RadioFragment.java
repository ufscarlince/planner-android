package mobile.ufscar.br.app.ui;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import mobile.ufscar.br.app.R;
import mobile.ufscar.br.app.logic.RadioService;
import mobile.ufscar.br.app.logic.RadioServiceListener;
import mobile.ufscar.br.app.utils.Utility;

import static mobile.ufscar.br.app.utils.Utility.checkConnection;

/**
 * Controla o tocador da rádio e implementa os métodos de {@link RadioServiceListener} e utiliza a
 * classe {@link RadioService} como serviço para comunicação entre o aplicativo e o servidor da
 * rádio UFSCar.
 */

public class RadioFragment extends Fragment implements RadioServiceListener {

//    Indica se RadioService está ligado a essa Activity
    private boolean mIsBound;
//    Indica a necessidade de checar o estado da rádio (usado somente no começo da Activity)
    private boolean mRadioCheck;
    private RadioService mRadioService = null;
    private MediaPlayer mMediaPlayer;
    private ImageButton mIbPlay;
    private FrameLayout mRadioLoading;
    private LinearLayout mLlRadio;
    private LinearLayout mDragonRadioLl;
    private Button mbtnDragonRadio;
    private TextView mDragonMessage;
    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            mRadioService = ((RadioService.RadioBinder) service).getService();
            mRadioService.registerClient(RadioFragment.this);
            mMediaPlayer = mRadioService.getMediaPlayer();
            mIsBound = true;
            if (mMediaPlayer == null) {
                RadioService.radioFailedAlert(getActivity()).show();
                mRadioService.stopRadio();
            } else {
                if (mRadioCheck) {
                    checkRadio();
                    mRadioCheck = false;
                }
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mRadioService = null;
            mMediaPlayer = null;
            mIsBound = false;
            setLayoutRadio(false);
        }
    };

    /**
     * Faz a ligação entre {@link MainActivity} e {@link RadioService}
     */
    void doBindService() {
        getActivity().bindService(new Intent(getActivity(),
                RadioService.class), mConnection, Context.BIND_ABOVE_CLIENT);
    }

    /**
     * Desfaz ligação entre {@link MainActivity} e {@link RadioService}
     */
    void doUnbindService() {
        if (mIsBound) {
            mIsBound = false;
            mRadioService.unRegisterClient();
            getActivity().unbindService(mConnection);
            mRadioService = null;
            mMediaPlayer = null;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_radio, container, false);
        mIbPlay = (ImageButton) rootView.findViewById(R.id.ibPlayButton);

        mRadioLoading = (FrameLayout) rootView.findViewById(R.id.flLoadingRadio);
        mRadioLoading.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mRadioService.stopRadio();
            }
        });

        mLlRadio = (LinearLayout) rootView.findViewById(R.id.llRadio);
        mDragonRadioLl = (LinearLayout) rootView.findViewById(R.id.llDragonRadio);
        mbtnDragonRadio = (Button) rootView.findViewById(R.id.btnDragonRadio);
        mDragonMessage = (TextView) rootView.findViewById(R.id.tvDragonRadioPhrase);

        mIbPlay.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!checkConnection(getContext())) {
                    setDragonRadioLayout();
                } else {
                    if (!isRadioServiceRunning()) {
                        getActivity().startService(new Intent(getActivity(), RadioService.class));
                        MainActivity.mRadioStarting = true;

                    }
                    if (mRadioService == null) {
                        doBindService();
                        mIsBound = false;
                        setLayoutRadio(true);
                    }
                    if (mIsBound) {
                        if (mRadioService.isReady()) {
                            if (!mMediaPlayer.isPlaying()) mRadioService.playRadio();
                            else {
                                mRadioService.stopRadio();
                            }
                        } else {
                            mRadioService.stopRadio();
                        }
                    }
                }
            }
        });

        mbtnDragonRadio.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkConnection(getContext()) && !Utility.isMobileDataBlocked(getContext())) {
                    mDragonRadioLl.setVisibility(View.GONE);
                    mLlRadio.setVisibility(View.VISIBLE);
                }
            }
        });

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!checkConnection(getContext()) || Utility.isMobileDataBlocked(getContext())) {
            setDragonRadioLayout();
        } else {
            if (mRadioService == null) {
                mIsBound = false;
                if (isRadioServiceRunning()) {
                    mRadioCheck = true;
                    mRadioLoading.setVisibility(View.VISIBLE);
                    doBindService();
                } else {
                    setLayoutRadio(false);
                }
            } else {
                checkRadio();
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        doUnbindService();
    }

     /**
     * Seta as visibilidades necessárias para o fragmento do dragao da radio funcionar.
     */
    public void setDragonRadioLayout() {
        if(Utility.isMobileDataBlocked(getContext()))
            mDragonMessage.setText(R.string.data_usage_blocked_dragon_msg);
        else
            mDragonMessage.setText(R.string.no_connection_error_dragon_msg);
        mLlRadio.setVisibility(View.GONE);
        mDragonRadioLl.setVisibility(View.VISIBLE);
    }

     /**
     * Altera o modo do layout {@link R.layout#fragment_radio}
     *
     * @param tocarRadio boolean que quando true coloca o layout como se estivesse tocando algo e
     *                   caso false coloca o layout como se não estivesse tocando nada
     */
    private void setLayoutRadio(boolean tocarRadio) {
        if (tocarRadio) {
            mIbPlay.setImageResource(R.drawable.ic_button_stop);
            mRadioLoading.setVisibility(View.VISIBLE);
        } else {
            mIbPlay.setImageResource(R.drawable.ic_button_play);
            mRadioLoading.setVisibility(View.GONE);
        }
    }

    /**
     * Verifica o estado da rádio e ajusta o layout {@link R.layout#fragment_radio}
     * apropriadamente
     */
    private void checkRadio() {
        if (!mRadioService.isStarting()) {
            if (mRadioService.isReady()) {
                if (mMediaPlayer.isPlaying()) {
                    setLayoutRadio(true);
                    mRadioLoading.setVisibility(View.GONE);
                } else {
                    setLayoutRadio(false);
                }
            } else {
                setLayoutRadio(false);
            }
        } else {
            setLayoutRadio(true);
        }
    }

    /**
     * Verifica se o rádio está tocando, mais seguro que usar um static boolean para verificação
     *
     * @return true se RadioService estiver rodando e false caso contrário
     * @see <a href=
     * "http://stackoverflow.com/questions/600207/how-to-check-if-a-service-is-running-on-android">
     * http://stackoverflow.com/questions/600207/how-to-check-if-a-service-is-running-on-android</a>
     */
    private boolean isRadioServiceRunning() {
        ActivityManager manager =
                (ActivityManager) getActivity().getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service :
                manager.getRunningServices(Integer.MAX_VALUE)) {
            if (RadioService.class.getName().equals(service.service.getClassName())) return true;
        }
        return false;
    }

    @Override
    public void radioStartingToPlay() {
        setLayoutRadio(true);
        MainActivity.mRadioStarting = true;
    }

    @Override
    public void radioPlaying() {
        setLayoutRadio(true);
        mRadioLoading.setVisibility(View.GONE);
        MainActivity.mRadioStarting = false;
    }

    @Override
    public void radioStopped() {
        setLayoutRadio(false);
        doUnbindService();
    }

    @Override
    public void radioPaused() {
        setLayoutRadio(false);
    }

    @Override
    public void radioFailed() {
        RadioService.radioFailedAlert(getActivity()).show();
    }
}