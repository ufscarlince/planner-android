package mobile.ufscar.br.app.logic;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import mobile.ufscar.br.app.config.Constants;
import mobile.ufscar.br.app.model.EventModel;
import mobile.ufscar.br.app.model.LocalModel;
import mobile.ufscar.br.app.model.ScheduleModel;

public class Manager {

    public static List<EventModel> getEventList() {

        List<EventModel> eventList = new ArrayList<>();

        EventModel event = new EventModel("Arquitetura de Computadores",
                new ScheduleModel(0, 8, 10),
                new LocalModel("AT9", "212", Constants.SAO_CARLOS, -21.979008, -47.881181));
        eventList.add(event);

        event = new EventModel("Calculo 1",
                new ScheduleModel(1, 10, 12),
                new LocalModel("AT10", "121", Constants.SAO_CARLOS, -21.982562, -47.884646));
        eventList.add(event);

        event = new EventModel("Calculo 1",
                new ScheduleModel(3, 8, 10),
                new LocalModel("AT10", "121", Constants.SAO_CARLOS, -21.982562, -47.884646));
        eventList.add(event);

        event = new EventModel("Fenomeno de Transporte",
                new ScheduleModel(1, 14, 18),
                new LocalModel("AT5", "99", Constants.SAO_CARLOS, -21.982182, -47.879348));
        eventList.add(event);

        event = new EventModel("Banco de Dados",
                new ScheduleModel(2, 14, 16),
                new LocalModel("AT9", "210", Constants.SAO_CARLOS, -21.979008, -47.881181));
        eventList.add(event);

        event = new EventModel("LFA",
                new ScheduleModel(2, 16, 18),
                new LocalModel("AT9", "210", Constants.SAO_CARLOS, -21.979008, -47.881181));
        eventList.add(event);

        event = new EventModel("Analise de Investimentos",
                new ScheduleModel(3, 16, 18),
                new LocalModel("AT4", "40", Constants.SAO_CARLOS, -21.982303, -47.883902));
        eventList.add(event);

        event = new EventModel("Sociologia",
                new ScheduleModel(4, 8, 12),
                new LocalModel("AT8", "72", Constants.SAO_CARLOS, -21.988539, -47.879278));
        eventList.add(event);

        return eventList;
    }

    /**
     * Recebe o dia da semana e uma lista de eventos e retorna uma lista ordenada com todos
     * os eventos daquele dia
     */
    public static List<EventModel> getEventDaily(int weekday) {
        List<EventModel> eventList = CacheManager.getInstance().getEventCache();
        if (eventList != null) {
            int listSize = eventList.size();
            int scheduleaux = 0;
            int position = 0;

            List<EventModel> listDay = new ArrayList<>();


//            Preenche uma lista com os eventos do dia
            for (int i = 0; i < listSize; i++) {
                if (eventList.get(i).getSchedule().getWeekDay() == weekday) {
                    listDay.add(eventList.get(i));
                }
            }

//            Ordena a lista de acordo com o StartTime de cada evento, pelo método selectionsort
            for (int i = 0; i <= listDay.size() - 1; i++) {
                scheduleaux = listDay.get(i).getSchedule().getStartTime();
                position = i;
                for (int j = i + 1; j < listDay.size(); j++) {
                    if (scheduleaux > listDay.get(j).getSchedule().getStartTime()) {
                        position = j;
                        scheduleaux = listDay.get(j).getSchedule().getStartTime();
                    }
                }

                EventModel eventAux = new EventModel(listDay.get(position).getName(),
                        listDay.get(position).getSchedule(),
                        listDay.get(position).getLocal());
                listDay.set(position, listDay.get(i));
                listDay.set(i, eventAux);

            }

            return listDay;
        }
        return new ArrayList<>();
    }

    /**
     * Recebe uma lista ordenada de eventos do dia e retorna uma lista ordenada
     * com eventos vazios, completando assim todas as horas do dia.
     */

    public static List<EventModel> getEventEmpty(List<EventModel> list, int day) {
        List<EventModel> listEmpty = new ArrayList<>();
        int listSize = list.size();

        if (!list.isEmpty()) {
            if (list.get(0).getSchedule().getStartTime() > ScheduleModel.START_TIME) {
                listEmpty.add(new EventModel(new ScheduleModel(day, ScheduleModel.START_TIME,
                        list.get(0).getSchedule().getStartTime())));
                listEmpty.add(list.get(0));
            } else {
                listEmpty.add(list.get(0));
            }

            for (int i = 1; i < listSize; i++) {
                if (list.get(i).getSchedule().getStartTime() >
                        list.get(i - 1).getSchedule().getFinishTime()) {
                    listEmpty.add(new EventModel(new ScheduleModel(day,
                            list.get(i - 1).getSchedule().getFinishTime(),
                            list.get(i).getSchedule().getStartTime())));
                    listEmpty.add(list.get(i));
                } else {
                    listEmpty.add(list.get(i));
                }
            }

            if (list.get(listSize - 1).getSchedule().getFinishTime() < ScheduleModel.FINISH_TIME) {
                listEmpty.add(new EventModel(new ScheduleModel(day,
                        list.get(listSize - 1).getSchedule().getFinishTime(),
                        ScheduleModel.FINISH_TIME)));
            }
        } else {
            listEmpty.add(new EventModel(new ScheduleModel(day, ScheduleModel.START_TIME,
                    ScheduleModel.FINISH_TIME)));
        }

        return listEmpty;
    }

    /**
     * Recebe um evento e encontra sua posição na lista.
     *
     * @param event evento a ser procurado na lista
     * @return indice do evento na lista.
     */
    public static int getEventPosition(EventModel event) {
        List<EventModel> eventList = CacheManager.getInstance().getEventCache();
        int index = 0;
        while (index <= eventList.size()) {
            EventModel eventAux = eventList.get(index);
            if (event.equals(eventAux)) {
                return index;
            } else {
                index++;
            }
        }
        return -1;
    }


    /**
     * Retorna o evento que esta na lista na posicao passada como parametro
     */
    public static EventModel getEventInPosition(int index) {
        List<EventModel> eventList = CacheManager.getInstance().getEventCache();
        return eventList.get(index);
    }
}
