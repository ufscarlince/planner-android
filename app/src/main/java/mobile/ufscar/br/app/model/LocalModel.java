package mobile.ufscar.br.app.model;

public class LocalModel {

    private String mName;
    private String mComplement;
    private int mCampus;
    private double mLatitude;
    private double mLongitude;


    public LocalModel() {
    }

    public LocalModel(String name, String complement, int campus, double latitude,
                      double longitude) {
        this.mName = name;
        this.mComplement = complement;
        this.mCampus = campus;
        this.mLatitude = latitude;
        this.mLongitude = longitude;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public String getComplement() {
        return mComplement;
    }

    public int getCampus() {
        return mCampus;
    }

    public void setCampus(int campus) {
        this.mCampus = campus;
    }

    public void setComplement(String complement) {
        this.mComplement = complement;
    }

    public double getLatitude() {
        return mLatitude;
    }

    public void setLatitude(double latitude) {
        this.mLatitude = latitude;
    }

    public double getLongitude() {
        return mLongitude;
    }

    public void setLongitude(double longitude) {
        this.mLongitude = longitude;
    }
}
