package mobile.ufscar.br.app.config;

public class Constants {

    public static final int SEGUNDA = 0;
    public static final int TERCA = 1;
    public static final int QUARTA = 2;
    public static final int QUINTA = 3;
    public static final int SEXTA = 4;
    public static final int SABADO = 5;
    public static final int DOMINGO = 6;
    public static final int ARARAS = 0;
    public static final int LAGOA_DO_SINO = 1;
    public static final int SAO_CARLOS = 2;
    public static final int SOROCABA = 3;
    public static final int OUTROS = 4;
    public static final int COMPLEMENT_SIZE = 3;
    public static final String COMPLEMENT_AUX = "Sala";
    public static final String LOCAL_AUX = "AT";
    public static final int LOCAL_NAME_SIZE = 4;
    public static final double AT01_LATITUDE = -21.988008;
    public static final double AT01_LONGITUDE = -47.879022;
    public static final double AT02_LATITUDE = -21.987732;
    public static final double AT02_LONGITUDE = -47.878995;
    public static final double AT03_LATITUDE = -21.982055;
    public static final double AT03_LONGITUDE = -47.881321;
    public static final double AT04_LATITUDE = -21.982326;
    public static final double AT04_LONGITUDE = -47.883947;
    public static final double AT05_LATITUDE = -21.982192;
    public static final double AT05_LONGITUDE = -47.879310;
    public static final double AT06_LATITUDE = -21.978427;
    public static final double AT06_LONGITUDE = -47.881096;
    public static final double AT07_LATITUDE = -21.982017;
    public static final double AT07_LONGITUDE = -47.878285;
    public static final double AT08_LATITUDE = -21.988439;
    public static final double AT08_LONGITUDE = -47.879415;
    public static final double AT09_LATITUDE = -21.979005;
    public static final double AT09_LONGITUDE = -47.881234;
    public static final double AT10_LATITUDE = -21.982589;
    public static final double AT10_LONGITUDE = -47.884627;
    public static final int RADIO_NOTIFICATION_ID = 1;
    public static final String AUTHORIZATION_KEY = "Authorization";
    public static final String TIME_ZONE_API = "GMT";
    public static final String ELEMENT_MENU = "cardapio";
    public static final String CLASS_MENU = "periodo";
    public static final String AUX_LUNCH_TAG = "div";
    public static final String MENU_NOT_DEFINED = "Não Definido.";
    public static final long DAY_MILLISECOND = 24L * 60L * 60L * 1000L;
    public static final int NUM_TABS = 3;
    public static final String MENU_DINNER = "Jantar";
    public static final String MENU_LUNCH = "Almoço";
    public static final String USER_NAME_LOGIN = "username";
    public static final String AUTHORIZATION_HEADER = "Bearer ";
    public static final String USER_PASSWORD_LOGIN = "password";
    public static final String FAKE_TOKEN = "XXXXX";
    public static final String DEFERMENT_JSON_PARSING_KEY = "data";
    public static final String JSON_LOGIN_ERROR_TAG = "NotesLoginFragment";
    public static final String REQUEST_DEFAULT_TAG = "REQUEST_DEFAULT_TAG";
    /**
     * Indica o primeiro horário que será chamado a verificação do almoço
     */
    public static final int LUNCH_BEGINNING = 10;
    /**
     * Indica o último horário que será chamado a verificação do almoço
     */
    public static final int LUNCH_ENDING = 14;
    /**
     * Indica o primeiro horário que será chamado a verificação da janta
     */
    public static final int DINNER_BEGINNING = 16;
    /**
     * Indica o último horário que será chamado a verificação da janta
     */
    public static final int DINNER_ENDING = 19;

    /**
     * Código de quando foi possível averiguar o erro do servidor
     */

    public class UfscarError {
        public static final int DEFAULT_HEADER_CODE = -1;
        public static final int AUTH_INVALID_TOKEN = 441;
        public static final int PAGE_NOT_FOUND = 404;
        public static final int GENERIC_FAIL_PARSING_JSON = 1;
        public static final String JSON_CODE_KEY = "code";
        public static final String JSON_MESSAGE_KEY = "message";
        public static final String JSON_ERRORS_KEY = "errors";
        public static final String FAILED_AUTHENTICATION_RESPONSE =
                "java.io.IOException: No authentication challenges found";
        public static final String CHAR_SET_UTF8 = "UTF-8";
    }

    public class MenuError {
        public static final int NOT_DEFINED = 2;
        public static final int IS_SUNDAY = 3;
    }

    public class TimeRequestError {
        public static final int CURRENT_TIME_MULTIPLE_REQUESTS_ERROR = 503;
        public static final String CURRENT_TIME_MULTIPLE_REQUESTS_ERROR_MESSAGE =
                "A pressa é inimiga da perfeição.";
    }

    public class TimeRequest {
        public static final String JSON_TIME_KEY = "time";
        public static final String RECEIVED_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'";
        public static final String TIMEZONE_ID = "UTC";
        public static final long TIMEZONE_ADJUST = 10800000;

    }

    public class Urls{
        public static final String DEFERMENT =
                "https://sistemas-qa.ufscar.br/sagui-api/siga/deferimento";
        public static final String CURRENT_TIME =
                "https://sistemas.ufscar.br/sagui-api/time";
        public static final String MENU_SC = "http://www2.ufscar.br/restaurantes-universitario/cardapio";
        public static final String LOGIN = "https://sistemas-qa.ufscar.br/sagui-api/api/login";
    }

    public class CacheLocation {
        public static final String CACHE_NAME = "locations";
        public static final String ARARAS_KEY = "araras";
        public static final String LAGOA_DO_SINO_KEY = "lagoa do sino";
        public static final String SAO_CARLOS_KEY = "sao carlos";
        public static final String SOROCABA_KEY = "sorocaba";
        public static final String INVALID_CAMPUS = "Invalid campus: ";
    }

    public class CacheManager {
        public static final String CACHE_NAME = "cache";
        public static final String CACHE_EVENT_KEY = "event";
        public static final String CACHE_LOGIN_KEY = "login";
        public static final String CACHE_MENU_KEY = "menu";
        public static final String CACHE_CONFIG_KEY = "config";
        public static final String CACHE_FIRST_LAUNCH_KEY = "first launch";
        public static final String CACHE_DEFERMENT_STATUS = "status user";
	    public static final String CACHE_STATUS_LOGIN_USER = "status login user";
	    public static final String CACHE_FIRST_TAB_MAP_KEY = "first tab map";
        public static final String CACHE_ENABLE_MAP_KEY = "enable map";
    }

    public class CardapioReceiver {
        public static final String LOG_TAG = "ALARM";
        public static final String CHECK_CARDAPIO = "CHECK CARDAPIO";
        public static final String UP_TO_DATE = "UP TO DATE";
        public static final String SUNDAY = "SUNDAY";
        public static final String NOTIFICATION = "NOTIFICATION";
        public static final String FAILED = "FAILED";
        public static final String SUCCESS_TIME = "SUCCESS TIME";
        public static final String FAILURE_TIME = "FAILURE TIME";
        public static final String BOOT = "BOOT";
        public static final String TIMEZONE = "TIMEZONE";
        public static final String TIME = "TIME";
        public static final String INVALID_ACTION = "Invalid Action: ";
        public static final String INVALID_RINGTONE = "Invalid Ringtone!";
        public static final String ALARM_TO_NEXT_PERIOD = "ALARM TO NEXT PERIOD";
        public static final String BEFORE_LUNCH = "ANTES do almoço";
        public static final String AFTER_BEGINNING_DINNER = "DEPOIS do começo da janta";
        public static final String BETWEEN_BEGINNING_LUNCH_BEGINNING_DINNER =
                "DEPOIS do começo do almoço até o começo da janta";
        public static final String ALARM_TO_NEXT_DAY = "ALARM TO NEXT DAY";
        public static final String SOMETHING_FAILED = "SOMETHING FAILED";
    }

    /**
     * Estas constantes são intrínsecamente relacionadas à construção de
     * {@link mobile.ufscar.br.app.ui.MainPagerAdapter}
     */
    public class MainPagerAdapter {
        public static final int DAILY_VISION = 0;
        public static final int MENU_RESTAURANT = 1;
        public static final int UFSCAR_RADIO = 2;
        public static final int MAPS = 3;
    }

    public class Notification {
        public static final String MENU_RESTAURANT_TAB_KEY = "menu tab";
        public static final String RADIO_TAB_KEY = "radio tab";
    }

    public class DailyWidget {
        public static final int WIDGET_UPDATE_HOUR = 0;
        public static final int WIDGET_UPDATE_MINUTE = 0;
        public static final int WIDGET_FLAG = 0;
        public static final int WIDGET_REQUEST_CODE = 0;
        public static final String WIDGET_INTENT = "WIDGET_INTENT";
    }

    public class DailyVisionFragment {
        /**
         * int usado para saber se uma tab está selecionada
         */
        public static final int SELECTED = 0;

        /**
         * int usado para saber se uma tab não está selecionada
         */
        public static final int NOT_SELECTED = 1;

        /**
         * int usado para saber se uma tab é do dia atual
         */
        public static final int CURRENT_DAY = 2;
    }

    public class DailyTabFragment{
        public static final int DELETE_EVENT = 2;
        public static final int EDIT_EVENT = 1;
        public static final int MAP_EVENT = 0;
    }

    public class RadioService{
        public static final String RADIO_AUDIO_FOCUS_CHANGE_DEFAULT = "focusChange desconhecido: ";
        public static final float MEDIA_PLAYER_LEFT_VOLUME = 0.1f;
        public static final float MEDIA_PLAYER_RIGHT_VOLUME = 0.1f;
    }

    public class MainActivity {
        public static final String EXIT_ACTIVITY_MESSAGE = "Pressione novamente para sair.";
        public static final int EXIT_ACTIVITY_TIME = 2000;
    }

    public class EditVision {
        public static final String DEFAULT_WEEK_DAY = "Dia incorreto";
        public static final String BLANK_EVENT_NAME_WARNING = "Campo obrigatório";
        public static final String INCORRECT_STRING_FORMAT = "String no formato incorreto";
    }

    public class SaoCarlosBounds {
        public static final double SAO_CARLOS_NORTH_LATITUDE = -21.959933;
        public static final double SAO_CARLOS_SOUTH_LATITUDE = -22.077147;
        public static final double SAO_CARLOS_EAST_LONGITUDE = -47.840868;
        public static final double SAO_CARLOS_WEST_LOGINTUDE = -47.951247;
    }

    public class MapFragment{
        public static final int MAP_DEFAULT_ZOOM = 17;
        public static final int MAP_UFSCAR_ZOOM = 15;
        public static final double UFSCAR_LATITUDE = -21.984643;
        public static final double UFSCAR_LONGITUDE = -47.879286;
        public static final double UFSCAR_SOROCABA_LATITUDE = -23.582069;
        public static final double UFSCAR_SOROCABA_LONGITUDE = -47.524078;
        public static final double UFSCAR_ARARAS_LATITUDE = -22.311498;
        public static final double UFSCAR_ARARAS_LONGITUDE = -47.384785;
        public static final double UFSCAR_LAG_SINO_LATITUDE = -23.598082;
        public static final double UFSCAR_LAG_SINO_LONGITUDE = -48.529608;
    }
}