package mobile.ufscar.br.app.logic;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import mobile.ufscar.br.app.config.Constants;
import mobile.ufscar.br.app.model.LocalModel;

//TODO: 09/10/16 - Alguns campus estão com locais fantasias, substituir quando houver os pontos
//TODO: reais. Somentem o São Carlos possui os pontos reais no momento

/**
 * Usa uma {@link SharedPreferences} como cache para listas de locais.
 */

public class CacheLocation {

    private static CacheLocation sInstance;

    private CacheLocation() {
    }

    public static CacheLocation getInstance() {
        if (sInstance == null) {
            sInstance = new CacheLocation();
        }
        return sInstance;
    }

    private Context getContext() {
        return UfscarApplication.getInstance();
    }

    /**
     * Guarda uma lista de locais para um campus específico
     *
     * @param campus int com o campus desejado, possíveis campi definidos em {@link Constants}
     * @throws IllegalStateException se for passado um campus inexistente
     */
    public void setLocations(int campus, List<LocalModel> list)
            throws IllegalStateException {
        SharedPreferences preferences = getContext().
                getSharedPreferences(Constants.CacheLocation.CACHE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

//        Transforma a lista em JSON
        String listJsonString = new Gson().toJson(list);
        switch (campus) {
            case Constants.ARARAS: {
                editor.putString(Constants.CacheLocation.ARARAS_KEY, listJsonString);
                break;
            }
            case Constants.LAGOA_DO_SINO: {
                editor.putString(Constants.CacheLocation.LAGOA_DO_SINO_KEY, listJsonString);
                break;
            }
            case Constants.SAO_CARLOS: {
                editor.putString(Constants.CacheLocation.SAO_CARLOS_KEY, listJsonString);
                break;
            }
            case Constants.SOROCABA: {
                editor.putString(Constants.CacheLocation.SOROCABA_KEY, listJsonString);
                break;
            }
            default:
                throw new IllegalStateException(Constants.CacheLocation.INVALID_CAMPUS + campus);
        }
        editor.apply();
    }

    /**
     * Verifica se o campus passado pelo parâmetro é um campus válido
     *
     * @param campus int que indica o campus
     * @return true se há localização para aquele campus e caso contrário, false
     * @see Constants#ARARAS
     * @see Constants#LAGOA_DO_SINO
     * @see Constants#SAO_CARLOS
     * @see Constants#SOROCABA
     */
    public static boolean isCampus(int campus) {
        return !(campus != Constants.ARARAS && campus != Constants.LAGOA_DO_SINO &&
                campus != Constants.SAO_CARLOS && campus != Constants.SOROCABA);
    }

    /**
     * Guarda os locais default dos campi
     */
    public void setDefaultLocations() {
        SharedPreferences preferences = getContext().
                getSharedPreferences(Constants.CacheLocation.CACHE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        editor.putString(Constants.CacheLocation.ARARAS_KEY, new Gson().toJson(getLocalAraras()));
        editor.putString(Constants.CacheLocation.LAGOA_DO_SINO_KEY,
                new Gson().toJson(getLocalLagoaDoSino()));
        editor.putString(Constants.CacheLocation.SAO_CARLOS_KEY,
                new Gson().toJson(getLocalSaoCarlos()));
        editor.putString(Constants.CacheLocation.SOROCABA_KEY,
                new Gson().toJson(getLocalSorocaba()));
        editor.apply();
    }

    /**
     * Retorna as localizações de um campus específico
     *
     * @param campus int com o campus desejado, possíveis campus definidos em {@link Constants}
     * @return List com os LocalModel do campus passado pelo parâmetro ou vazia
     * @throws IllegalStateException se for passado um campus inexistente
     */
    public List<LocalModel> getLocations(int campus) throws IllegalStateException {
        SharedPreferences preferences = getContext().
                getSharedPreferences(Constants.CacheLocation.CACHE_NAME, Context.MODE_PRIVATE);

        List<LocalModel> localCampus = new ArrayList<>();
        String localCampusJsonString;
        switch (campus) {
            case Constants.ARARAS: {
                localCampusJsonString = preferences.getString(
                        Constants.CacheLocation.ARARAS_KEY, null);
                break;
            }
            case Constants.LAGOA_DO_SINO: {
                localCampusJsonString = preferences.getString(
                        Constants.CacheLocation.LAGOA_DO_SINO_KEY, null);
                break;
            }
            case Constants.SAO_CARLOS: {
                localCampusJsonString = preferences.getString(
                        Constants.CacheLocation.SAO_CARLOS_KEY, null);
                break;
            }
            case Constants.SOROCABA: {
                localCampusJsonString = preferences.getString(
                        Constants.CacheLocation.SOROCABA_KEY, null);
                break;
            }
            default:
                throw new IllegalStateException(Constants.CacheLocation.INVALID_CAMPUS + campus);
        }
        if (localCampusJsonString != null) {
            Type type = new TypeToken<List<LocalModel>>() {}.getType();
            localCampus = new Gson().fromJson(localCampusJsonString, type);
        }

        return localCampus;
    }

    /**
     * Preenche uma lista com os locais do campus Araras
     *
     * @return List<LocalModel> como os locais do campus
     */
    private List<LocalModel> getLocalAraras() {
        int campus = Constants.ARARAS;
        List<LocalModel> LocalAraras = new ArrayList<>();

//        oficiais do campus
        LocalAraras.add(new LocalModel("ARARAS 1", "", campus, -22.314620, -47.383616));
        LocalAraras.add(new LocalModel("ARARAS 2", "", campus, -22.310532, -47.383466));
        LocalAraras.add(new LocalModel("ARARAS 3", "", campus, -22.306768, -47.387763));
        LocalAraras.add(new LocalModel("ARARAS 4", "", campus, -22.314214, -47.388890));

        return LocalAraras;
    }

    /**
     * Preenche uma lista com os locais do campus Lagoa do Sino
     *
     * @return List<LocalModel> com os locais do campus
     */
    private List<LocalModel> getLocalLagoaDoSino() {
        int campus = Constants.LAGOA_DO_SINO;
        List<LocalModel> LocalLagoaDoSino = new ArrayList<>();

//        oficiais do campus
        LocalLagoaDoSino.add(new LocalModel("LGS 1", "", campus, -23.599572, -48.529808));
        LocalLagoaDoSino.add(new LocalModel("LGS 2", "", campus, -23.599572, -48.529808));
        LocalLagoaDoSino.add(new LocalModel("LGS 3", "", campus, -23.599572, -48.529808));
        LocalLagoaDoSino.add(new LocalModel("LGS 4", "", campus, -23.599572, -48.529808));

        return LocalLagoaDoSino;
    }

    /**
     * Preenche uma lista com os locais do campus São Carlos
     *
     * @return List<LocalModel> com os locais do campus
     */
    public static List<LocalModel> getLocalSaoCarlos() {
        int campus = Constants.SAO_CARLOS;
        List<LocalModel> LocalSaoCarlos = new ArrayList<>();
        LocalSaoCarlos.add(new LocalModel("AT1", "", campus, -21.988008, -47.879022));
        LocalSaoCarlos.add(new LocalModel("AT2", "", campus, -21.987732, -47.878995));
        LocalSaoCarlos.add(new LocalModel("AT3", "", campus, -21.982055, -47.881321));
        LocalSaoCarlos.add(new LocalModel("AT4", "", campus, -21.982326, -47.883947));
        LocalSaoCarlos.add(new LocalModel("AT5", "", campus, -21.982192, -47.879310));
        LocalSaoCarlos.add(new LocalModel("AT6", "", campus, -21.978427, -47.881096));
        LocalSaoCarlos.add(new LocalModel("AT7", "", campus, -21.982017, -47.878285));
        LocalSaoCarlos.add(new LocalModel("AT8", "", campus, -21.988439, -47.879415));
        LocalSaoCarlos.add(new LocalModel("AT9", "", campus, -21.979005, -47.881234));
        LocalSaoCarlos.add(new LocalModel("AT10", "", campus, -21.982589, -47.884627));
        LocalSaoCarlos.add(new LocalModel("Anexos I e II do DEBE ", "", campus,
                -21.983749, -47.877098));
        LocalSaoCarlos.add(new LocalModel("Anfiteatro Bento Prado Junior", "", campus,
                -21.983671, -47.881519));
        LocalSaoCarlos.add(new LocalModel("Auditório José Carlos Nogueira", "", campus,
                -21.983624, -47.880421));
        LocalSaoCarlos.add(new LocalModel("Biblioteca Comunitária", "", campus,
                -21.982926, -47.883121));
        LocalSaoCarlos.add(new LocalModel("Casa de Vegetação do DB", "", campus,
                -21.983134, -47.879943));
        LocalSaoCarlos.add(new LocalModel("DeAmo", "", campus, -21.977932, -47.882722));
        LocalSaoCarlos.add(new LocalModel("Dep de Artes e Comunicação", "", campus,
                -21.987953, -47.881312));
        LocalSaoCarlos.add(new LocalModel("Dep de Biologia", "", campus, -21.983028, -47.878672));
        LocalSaoCarlos.add(new LocalModel("Dep de Ciências Fisiológicas", "", campus,
                -21.983483, -47.877915));
        LocalSaoCarlos.add(new LocalModel("Dep de Ciências Sociais", "", campus,
                -21.988556, -47.882465));
        LocalSaoCarlos.add(new LocalModel("Dep de Computação", "", campus, -21.979724, -47.880630));
        LocalSaoCarlos.add(new LocalModel("Dep de Ecologia e Biologia Evolutiva", "", campus,
                -21.983176, -47.878372));
        LocalSaoCarlos.add(new LocalModel("Dep de Educação Física e Motricidade Humana", "", campus,
                -21.987674, -47.881997));
        LocalSaoCarlos.add(new LocalModel("Dep de Enfermagem", "", campus, -21.978081, -47.880999));
        LocalSaoCarlos.add(new LocalModel("Dep de Eng Civíl", "", campus, -21.981779, -47.880347));
        LocalSaoCarlos.add(new LocalModel("Dep de Eng de Materiais", "", campus,
                -21.981476, -47.880908));
        LocalSaoCarlos.add(new LocalModel("Dep de Eng de Produção", "", campus,
                -21.982223, -47.883101));
        LocalSaoCarlos.add(new LocalModel("Dep de Eng Química", "", campus,
                -21.981920, -47.882210));
        LocalSaoCarlos.add(new LocalModel("Dep de Estatística", "", campus,
                -21.981677, -47.883350));
        LocalSaoCarlos.add(new LocalModel("Dep de Filosofia e Met da Ciência", "", campus,
                -21.988489, -47.880806));
        LocalSaoCarlos.add(new LocalModel("Dep de Fisioterapia", "", campus,
                -21.977681, -47.880342));
        LocalSaoCarlos.add(new LocalModel("Dep de Gerontologia", "", campus,
                -21.978471, -47.879677));
        LocalSaoCarlos.add(new LocalModel("Dep de Letras", "", campus, -21.988902, -47.880546));
        LocalSaoCarlos.add(new LocalModel("Dep de Matemática", "", campus, -21.979549, -47.881211));
        LocalSaoCarlos.add(new LocalModel("Dep de Medicina", "", campus, -21.978009, -47.880188));
        LocalSaoCarlos.add(new LocalModel("Dep de Química", "", campus, -21.983982, -47.881540));
        LocalSaoCarlos.add(new LocalModel("Dep de Sociologia", "", campus, -21.988099, -47.882652));
        LocalSaoCarlos.add(new LocalModel("Dep de Terapia Ocupacional", "", campus,
                -21.977544, -47.880877));
        LocalSaoCarlos.add(new LocalModel("Dep de Física", "", campus, -21.983858, -47.883592));
        LocalSaoCarlos.add(new LocalModel("Dep de Psicologia", "", campus, -21.988367, -47.880086));
        LocalSaoCarlos.add(new LocalModel("Ginásio Poliesportivo", "", campus,
                -21.987254, -47.880874));
        LocalSaoCarlos.add(new LocalModel("Lab de Biopolímeros", "", campus,
                -21.980700, -47.879180));
        LocalSaoCarlos.add(new LocalModel("Lab de Caracterização Estrutural ", "", campus,
                -21.980568, -47.880624));
        LocalSaoCarlos.add(new LocalModel("Lab de Destilação", "", campus, -21.983128, -47.880387));
        LocalSaoCarlos.add(new LocalModel("Lab de Engenharia do Produto", "", campus,
                -21.981720, -47.883467));
        LocalSaoCarlos.add(new LocalModel("Lab de Ensino de Química", "", campus,
                -21.984327, -47.880888));
        LocalSaoCarlos.add(new LocalModel("Lab de Materiais Vitreos", "", campus,
                -21.981229, -47.880185));
        LocalSaoCarlos.add(new LocalModel("Lab de Musicalização do DAC", "", campus,
                -21.988453, -47.883104));
        LocalSaoCarlos.add(new LocalModel("Lab de Pesquisa do DECiv", "", campus,
                -21.981229, -47.879698));
        LocalSaoCarlos.add(new LocalModel("Lab de Reciclagem", "", campus,
                -21.981154, -47.881639));
        LocalSaoCarlos.add(new LocalModel("Lab Interdisciplinar de Eletroquímica e Cerâmica", "",
                campus, -21.983462, -47.881093));
        LocalSaoCarlos.add(new LocalModel("Lab NET", "", campus, -21.981314, -47.879226));
        LocalSaoCarlos.add(new LocalModel("Moradia Estudantil", "", campus,
                -21.987876, -47.878142));
        LocalSaoCarlos.add(new LocalModel("Nulem", "", campus, -21.980805, -47.878013));
        LocalSaoCarlos.add(new LocalModel("Pavilhão de Ginástica", "", campus,
                -21.986713, -47.882484));
        LocalSaoCarlos.add(new LocalModel("Restaurante Universitário", "", campus,
                -21.984950, -47.882696));
        LocalSaoCarlos.add(new LocalModel("Sala de Ensaio da Orquestra", "", campus,
                -21.987781, -47.882878));
        LocalSaoCarlos.add(new LocalModel("Teatro de Bolso", "", campus, -21.987825, -47.881538));
        LocalSaoCarlos.add(new LocalModel("Teatro Florestan Fernandes", "", campus,
                -21.982962, -47.882567));
        LocalSaoCarlos.add(new LocalModel("USE", "", campus, -21.978002, -47.882446));

        return LocalSaoCarlos;
    }

    /**
     * Preenche uma lista com os locais do campus Sorocaba
     *
     * @return List<LocalModel> com os locais do campus
     */
    private List<LocalModel> getLocalSorocaba() {
        int campus = Constants.SOROCABA;
        List<LocalModel> LocalSorocaba = new ArrayList<>();

//        oficiais do campus
        LocalSorocaba.add(new LocalModel("SOROCABA 1", "", campus, -23.582055, -47.524167));
        LocalSorocaba.add(new LocalModel("SOROCABA 2", "", campus, -23.580720, -47.524870));
        LocalSorocaba.add(new LocalModel("SOROCABA 3", "", campus, -23.581605, -47.527037));
        LocalSorocaba.add(new LocalModel("SOROCABA 4", "", campus, -23.583277, -47.525096));

        return LocalSorocaba;
    }

    /**
     * Transforma uma lista de LocalModel em uma lista com os nomes desses LocalModel
     *
     * @return List com os nomes dos LocalModel do parâmetro
     * @see LocalModel
     */
    public static List<String> getNameLocations(List<LocalModel> list) {
        List<String> strings = new ArrayList<>();

        for (int i = 0; i < list.size(); i++) {
            strings.add(list.get(i).getName());
        }

        return strings;
    }

    /**
     * Procura na lista retorna o {@link LocalModel} referente ao nome fornecido
     *
     * @param localName nome do respectivo local
     * @param campus campus do respectivo local
     * @return {@link LocalModel} procurado
     * @see Constants#SAO_CARLOS
     * @see Constants#SOROCABA
     * @see Constants#LAGOA_DO_SINO
     * @see Constants#ARARAS
     */
    public LocalModel getSingleLocal(String localName, int campus) {
        List<LocalModel> localList = getLocations(campus);
        LocalModel local = new LocalModel();

        for (int i = 0; i < localList.size(); i++) {
            if (localList.get(i).getName().equals(localName)) {
                local = localList.get(i);
                break;
            } else {
                local = new LocalModel("", "", Constants.SAO_CARLOS,
                        Constants.MapFragment.UFSCAR_LATITUDE,
                        Constants.MapFragment.UFSCAR_LONGITUDE);
            }
        }
        return local;
    }
}