package mobile.ufscar.br.app.model;

/**
 * Classe para receber o JSONObject do servico de Login, utilizando Gson
 */
public class LoginResponseModel {

    private String username;
    private String access_token;

    public LoginResponseModel(String username, String access_token) {
        this.username = username;
        this.access_token = access_token;
    }

    public String getUsername() {
        return username;
    }

    public String getAccess_token() {
        return access_token;
    }
}


