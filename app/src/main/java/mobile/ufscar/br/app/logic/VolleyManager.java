package mobile.ufscar.br.app.logic;

import android.app.Application;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.Volley;

import mobile.ufscar.br.app.config.Constants;

/**
 * Classe para utilização em Requisiçoes de serviços paralelos, utilizando a biblioteca Volley
 * Tendo um objeto com os parametros utilizados nas requisiçoes
 */
public class VolleyManager extends Application {

    private static VolleyManager sInstance;
    private RequestQueue mRequestQueue;

    @Override
    public void onCreate(){
        super.onCreate();
        sInstance = this;
        mRequestQueue = Volley.newRequestQueue(getApplicationContext());
    }

    public static synchronized VolleyManager getInstance() {
        if (sInstance == null) {
            sInstance = new VolleyManager();
        }
        return sInstance;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        req.setTag(TextUtils.isEmpty(tag) ? Constants.REQUEST_DEFAULT_TAG : tag);

        VolleyLog.d("Adding request to queue: %s", req.getUrl());

        mRequestQueue.add(req);
    }


    public <T> void addToRequestQueue(Request<T> req) {
        addToRequestQueue(req, null);
    }

    public VolleyManager cancelAll(String tag) {
        mRequestQueue.cancelAll(tag);
        return sInstance;
    }
}
