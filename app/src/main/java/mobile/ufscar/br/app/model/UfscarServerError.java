package mobile.ufscar.br.app.model;

import org.json.JSONObject;

import mobile.ufscar.br.app.config.Constants;

/**
 * Model contendo as especifidades do erro retornado pelo servidor
 */

public class UfscarServerError {

    /**
     * Código de erro específico de cada servidor, por exemplo, imagine que o usuário tenha sua
     * sessão expirada, o servidor pode retornar como erro o código
     * {@link java.net.HttpURLConnection#HTTP_UNAUTHORIZED} e juntamente mandar um JSON explicando
     * o erro, com um código SESSAO_EXPIRADA. Como nem todos os servidores especifícam seus erros,
     * caso o servidor mande nenhuma informação adicional será atribuído o código
     * {@link Constants.UfscarError#GENERIC_FAIL_PARSING_JSON}
     */
    private int mCode;
    /**
     * Mensagem retornada pelo servidor, quando há mensagens retornadas, senão pode ser alguma
     * mensagem definida em strings.xml
     */
    private String mMessage;

    public UfscarServerError() {
    }

    public UfscarServerError(int code, String message) {
        this.mCode = code;
        this.mMessage = message;
    }

    public UfscarServerError(JSONObject object) {
        this.mCode = object.optInt(Constants.UfscarError.JSON_CODE_KEY);
        this.mMessage = object.optString(Constants.UfscarError.JSON_MESSAGE_KEY);
    }

    public int getCode() {
        return mCode;
    }

    public void setCode(int code) {
        this.mCode = code;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        this.mMessage = message;
    }

    @Override
    public String toString() {
        return "UfscarServerError{" +
                "code = " + mCode +
                ", message = '" + mMessage + '\'' +
                '}';
    }
}
