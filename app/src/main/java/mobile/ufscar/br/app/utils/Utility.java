package mobile.ufscar.br.app.utils;

import android.content.Context;

import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.view.View;

import java.util.HashMap;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import mobile.ufscar.br.app.config.Constants;
import mobile.ufscar.br.app.logic.CacheManager;
import mobile.ufscar.br.app.logic.DailyWidgetProvider;
import mobile.ufscar.br.app.model.ConfigurationModel;
import mobile.ufscar.br.app.model.EventModel;
import mobile.ufscar.br.app.model.LocalModel;
import mobile.ufscar.br.app.model.LoginResponseModel;
import mobile.ufscar.br.app.model.RestaurantMenuModel;
import mobile.ufscar.br.app.model.ScheduleModel;

public class Utility {

    /**
     * Pega hora em String e retorna hora em int
     *
     * @param time String com alguma hora do dia (formato "xx...", x é um número)
     * @return int formado pelos primeiros dois caracteres ou -1 se não forem números
     * @throws NumberFormatException caso a String não esteja da forma especificada
     */
    public static int timeToInt(String time) throws NumberFormatException {
        return Integer.parseInt(String.copyValueOf(time.toCharArray(), 0, 2));
    }

    /**
     * Método mostra uma mensagem utilizando {@link Snackbar} na tela durante alguns segundos
     *
     * @param view         na qual será exibido o SnackBar, sugerido que sejá {@link CoordinatorLayout}
     *                     pois assim na ação do SnackBar alguns items podem ser movidos para cima
     * @param charSequence frase que irá aparecer na tela
     * @see Snackbar#show()
     * @see Snackbar#LENGTH_SHORT
     */
    public static void showSnackbar(View view, CharSequence charSequence) {
        Snackbar snackbar = Snackbar.make(view, charSequence, Snackbar.LENGTH_SHORT);
        snackbar.show();
    }

    /**
     * Deixar o Horário de um ScheduleModel no formato adequado como uma String
     *
     * @param scheduleModel a ser formatado
     * @return String contendo o horário formatado
     */
    public static String formatScheduleEvent(ScheduleModel scheduleModel) {
        String schedule;
        int startTime = scheduleModel.getStartTime();
        int finishTime = scheduleModel.getFinishTime();
        if (finishTime == 24) {
            schedule = hourToString(startTime) + ":00 - " + hourToString(finishTime - 1) + ":59";
        } else {
            schedule = hourToString(startTime) + ":00 - " + hourToString(finishTime) + ":00";
        }
        return schedule;
    }

    /**
     * Transforma uma hora de um inteiro em uma String
     *
     * @param hour a ser formatada
     * @return String com a hora formatada
     */
    public static String hourToString(int hour) {
        if (hour < 10) return "0" + hour;
        else return hour + "";
    }

    /**
     * Pega várias String com informações sobre um evento e retorna um Evento
     *
     * @param name       String com o nome do evento a ser salvo
     * @param day        String com o dia do evento a ser salvo
     * @param startTime  String com o horário que começa o evento a ser salvo
     *                   (formato "xx:xx"/ x é um número)
     * @param finishTime String com o horário que termina o evento a ser salvo
     *                   (formato "xx:xx"/ x é um número)
     * @return EventModel com os dados dos parâmetros ou null se o horário não for válido
     * @throws NumberFormatException caso as Strings não estejam da forma especificada
     */
    @Nullable
    public static EventModel toEventModel(String name, LocalModel localModel,
                                          String day, String startTime, String finishTime)
            throws NumberFormatException {
        int dayInt = dayToInt(day);
        if (finishTime.equals("23:59")) {
            finishTime = "24:00";
        }
        if (dayInt != -1 && scheduleCondition(startTime, finishTime)) {
            return new EventModel(name,
                    new ScheduleModel(dayInt, timeToInt(startTime), timeToInt(finishTime)),
                    localModel);
        }
        return null;
    }

    /**
     * Verifica o Horário do Evento
     *
     * @param start String com o horário que começa o evento (formato "xx:xx"/ x é um número)
     * @param end   String com o horário que termina o evento (formato "xx:xx"/ x é um número)
     * @return true, se for um horário válido e término > início, senão, false
     * @throws NumberFormatException caso as Strings não estejam da forma especificada
     */
    public static boolean scheduleCondition(String start, String end) throws NumberFormatException {
        int inicio;
        int termino;
        inicio = Integer.parseInt(String.copyValueOf(start.toCharArray(), 0, 2) +
                String.copyValueOf(start.toCharArray(), 3, 2));
        termino = Integer.parseInt(String.copyValueOf(end.toCharArray(), 0, 2) +
                String.copyValueOf(end.toCharArray(), 3, 2));
        return termino > inicio;
    }

    /**
     * Pega um dia em String e retorna uma representação do mesmo em int
     *
     * @param day String com algum dia da semana
     * @return int que representa o dia da semana ou -1 caso day não seja um dia da semana
     */
    public static int dayToInt(String day) {
        String aux = day.toLowerCase();
        switch (aux) {
            case "segunda":
                return Constants.SEGUNDA;
            case "terca":
                return Constants.TERCA;
            case "terça":
                return Constants.TERCA;
            case "quarta":
                return Constants.QUARTA;
            case "quinta":
                return Constants.QUINTA;
            case "sexta":
                return Constants.SEXTA;
            case "sábado":
                return Constants.SABADO;
            case "domingo":
                return Constants.DOMINGO;
            default:
                return -1;
        }
    }

    /**
     * Transforma uma String qualquer em uma String formatada
     *
     * @param name String qualquer
     * @return String formatada (Ex: "Exemplo de Texto")
     */
    public static String formatName(String name) {
        String nameFormatted = "";

        if (name != null) {
            String aux = name.toUpperCase();
            int espaco = 0;
            while (aux.contains(" ")) {
                espaco = aux.indexOf(" ");
                String palavra = aux.substring(0, espaco).toLowerCase();
                if (palavra.length() < 4) {
                    nameFormatted = nameFormatted + aux.substring(0, espaco).toLowerCase() + " ";
                } else {
                    nameFormatted = nameFormatted + aux.substring(0, 1) +
                            aux.substring(1, espaco).toLowerCase() + " ";
                }
                aux = aux.substring(espaco + 1);
            }
            if (espaco != 0) {
                nameFormatted = nameFormatted + aux.substring(0, 1) +
                        aux.substring(1).toLowerCase();
            }
        }
        return nameFormatted;
    }

    /**
     * Verifica se a list de {@link RestaurantMenuModel} do CacheManager está atualizado(Dia atual)
     *
     * @param currentTime Tempo em milisegundos atual, pego através da API
     * @param zone        A zona do horário passado
     */
    public static boolean isMenuCacheUpToDate(long currentTime, TimeZone zone) {
        List<RestaurantMenuModel> restaurantMenuModels = CacheManager.getInstance().getMenuCache();
        if (restaurantMenuModels.isEmpty()) return false;

        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone(Constants.TIME_ZONE_API));
        calendar.setTimeInMillis(restaurantMenuModels.get(0).getCurrentTime());
        Calendar currentCalendar = Calendar.getInstance(zone);
        currentCalendar.setTimeInMillis(currentTime);

//        Se for depois do almoço e no cache não conter janta, o cache está desatualizado
        if (currentCalendar.get(Calendar.HOUR_OF_DAY) >= Constants.LUNCH_ENDING) {
            boolean found = false;
            int i = 0;
            while (i < restaurantMenuModels.size() && !found) {
                if (!restaurantMenuModels.get(i).isLunch()) found = true;
                i++;
            }
            if (!found) return false;
        }

//        Se for estiver no mesmo dia, retorna true
//        Está verificando se o dia atual é igual ao dia da variável no Cache
//        Está verificando se não é o mesmo dia, más outra semana e mesmo dia
        return calendar.get(Calendar.DAY_OF_WEEK) == currentCalendar.get(Calendar.DAY_OF_WEEK) &&
                (currentTime - restaurantMenuModels.get(0).getCurrentTime())
                        < Constants.DAY_MILLISECOND;
    }

    /**
     * Este método deixa a Localização no formato adequado, de acordo com o tipo de evento
     *
     * @param localModel Local a ser formatado
     * @return retorna a String localização formatada
     */
    public static String formatLocation(LocalModel localModel) {
        String complement = localModel.getComplement();
        if (complement != null && !complement.isEmpty() && !complement.equals(" ")) {
            return localModel.getName() + " - " + localModel.getComplement();
        } else {
            return localModel.getName();
        }
    }

    /**
     * Chama o serviço de atualização do Widget.
     *
     * @param context Context do APP.
     */
    public static void updateWidget(Context context) {
//        Cria um Broadcast para atualizar o Widget depois de alterar o cache.
        Intent intent = new Intent(context, DailyWidgetProvider.class);
        intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
        int ids[] = AppWidgetManager.getInstance(context).getAppWidgetIds(
                new ComponentName(context, DailyWidgetProvider.class));
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, ids);
        context.sendBroadcast(intent);
    }

    /**
     * Constroi um HashMap com o token de acesso para ser o Header das APIS.
     *
     * @return MAP com KEY Authorization contendo "Bearer + access_token"
     */
    public static Map<String, String> getAuthorizationHeader() {
        Map<String, String> headers = new HashMap<>();
        if (CacheManager.getInstance().getLoginCache() != null) {
            headers.put(Constants.AUTHORIZATION_KEY, Constants.AUTHORIZATION_HEADER
                    + CacheManager.getInstance().getLoginCache().getAccess_token());
        } else {
            headers.put(Constants.AUTHORIZATION_KEY, Constants.AUTHORIZATION_HEADER +
                    Constants.FAKE_TOKEN);
        }
        return headers;
    }

    /**
     * Método que faz analise de um JSONObject utlizando a biblioteca Gson
     * Assim a classe {@link LoginResponseModel} já tem um objeto instanciado
     * De acordo com os seus atributos
     * Para obter sucesso, os atributos da classe devem ter o mesmo nome das Key do JSONObject
     *
     * @param jsonObject   JSONObject obtido na requisição
     * @param definedClass Classe que terá um objeto instanciado e receberá JSONObject
     * @return Objeto da classe definida
     */
    public static Object parseJsonToObject(JSONObject jsonObject, Class definedClass) {

        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();

        return gson.fromJson(jsonObject.toString(), definedClass);
    }

    /**
     * Método que verifica se o celular possui conexão válida com a internet.
     * @return True se há conexão, false caso contrário.
     */
    public static boolean checkConnection(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetWorkInfo = connectivityManager.getActiveNetworkInfo();

        return activeNetWorkInfo != null && activeNetWorkInfo.isConnected();
    }

    /**
     * Checa o tipo de conexão usada pelo aplicativo (Wifi ou Móvel) e também se o uso de dados pelo
     * aplicativo está desativado (através do {@Link CacheManager}).
     *
     * @return True se o tipo de conexão usada é somente Móvel e se o uso de dados está desativado,
     * false caso contrário.
     */
    public static boolean isMobileDataBlocked(Context context) {
        ConnectivityManager conManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        boolean isMobile = conManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
                .isConnectedOrConnecting();
        boolean isWifi = conManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI)
                .isConnectedOrConnecting();

        ConfigurationModel configurationModel = CacheManager.getInstance().getConfigCache();

        return (isMobile && !isWifi) && configurationModel.isDataUsageBlocked();
    }
}

//  // COMENTÁRIOS
//
//  /**
//   * Descrição
//   *
//   * @param A colcar parâmetro somente se necessário
//   * @return quando return não for void
//   * @throws se tiver exception
//  */
//  void função(int A){
//    Comentários dentro de função somente com // e com 2 espaços da identação normal
////    Este é um comentário
//  }
//
//  // COMMITS
//
//  T#2105 [TAG]: --------
//
//  Possíveis Tags:
//     DOC
//     REFACT
//     FIX
//     STYLE
//