package mobile.ufscar.br.app.logic;

import android.content.Context;
import android.content.Intent;
import android.text.Html;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import mobile.ufscar.br.app.R;
import mobile.ufscar.br.app.config.Constants;
import mobile.ufscar.br.app.config.ConstantsActions;
import mobile.ufscar.br.app.model.RestaurantMenuModel;
import mobile.ufscar.br.app.model.UfscarResponseError;
import mobile.ufscar.br.app.model.UfscarServerError;
import mobile.ufscar.br.app.utils.ServiceResponseError;
import mobile.ufscar.br.app.utils.Utility;

/**
 * Classe que implementa {@link MenuRestaurantService}, usada para buscar o cardápio do site
 * do restaurante universitário da Ufscar - Campus São Carlos
 */

public class MenuRestaurantServiceImpl implements MenuRestaurantService {

    private static final String TYPE_DIVIDER = "/";

    /**
     * Busca o cardápio do dia atual no site da Universidade Campi Sao Carlos
     * Atualiza os objetos de cardapio no {@link CacheManager} em caso de sucesso
     *
     * @param listener          Listener a ser chamado em caso de sucesso ou falha, por ser null,
     *                          sendo null será enviado um intent com action
     *                          {@link ConstantsActions#MENU_NOTIFICATION_INTENT} caso dê
     *                          certo, caso contrário será enviado um intent com action
     *                          {@link ConstantsActions#MENU_FAILED_INTENT}, ou com
     *                          {@link ConstantsActions#MENU_IS_SUNDAY_INTENT} ou com
     *                          {@link ConstantsActions#MENU_IS_UP_TO_DATE_INTENT} dependendo
     *                          da situação
     * @param currentTimeMillis Hora atual, usada para verificar se o Cache está atualizado,
     *                          para setar os novos Models e saber se é domingo
     * @param zone              A zona do horário passado
     * @see Utility#isMenuCacheUpToDate
     */
    @Override
    public void getMenu(final MenuRestaurantListener listener,
                        final long currentTimeMillis, final TimeZone zone) {
//        calendar, recebe o tempo real, vindo da API
        final Calendar calendar = Calendar.getInstance(zone);
        calendar.setTimeInMillis(currentTimeMillis);

//        Aos domingo não tem o serviço de cardápio
        if (calendar.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY) {

//            Utilizando a biblioteca Volley para realizar a conexão
            final StringRequest request = new StringRequest(Request.Method.GET,
                    Constants.Urls.MENU_SC,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
//                            Aos sábados somente é feito o parser do almoço
                            boolean isSaturday = true;
                            Calendar calendar = Calendar.getInstance(zone);
                            calendar.setTimeInMillis(currentTimeMillis);
                            if (calendar.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY) {
                                isSaturday = false;
                            }

//                            Listas para armazenar os itens de cardápios presentes no site
                            StringBuffer stringBufferListLunch = new StringBuffer();
                            StringBuffer stringBufferListDinner = new StringBuffer();
//                            Utilizando a biblioteca Jsoup para buscar dados
//                            específicos em um Documento HTML

                            Document doc = Jsoup.parse(response);

                            try {

//                                No site: I) Pegando a Tag do cardápio II) Pegando a class com
//                                almoço e jantar
                                Element elementMenu = doc.getElementById(Constants.ELEMENT_MENU);
                                Elements menu = elementMenu.getElementsByClass(Constants.
                                        CLASS_MENU);

//                                Separando o almoço
                                Element lunch = menu.get(0);
                                Elements aux = lunch.getElementsByTag(Constants.AUX_LUNCH_TAG);
                                aux.remove(0);
                                aux.remove(0);
                                aux.remove(0);

//                                Criando uma lista com os itens do almoço
                                for (Element dish : aux) {
                                    stringBufferListLunch.append(dish.text());
                                    stringBufferListLunch.append("\n");
                                }

//                                criando uma lista com os itens do jantar
                                if (!isSaturday) {
                                    Element dinner = menu.get(1);
                                    aux = dinner.getElementsByTag(Constants.AUX_LUNCH_TAG);
                                    aux.remove(0);
                                    aux.remove(0);
                                    aux.remove(0);

                                    for (Element dish : aux) {
                                        stringBufferListDinner.append(dish.text());
                                        stringBufferListDinner.append("\n");
                                    }
                                }

//                                Tratamento de possíveis erros no parser do site
                            } catch (Exception e) {
                                listener.updateFailed(new UfscarResponseError(
                                        HttpURLConnection.HTTP_NOT_FOUND,
                                        new UfscarServerError(
                                                HttpURLConnection.HTTP_NOT_FOUND,
                                                getContext().getResources().getString(
                                                        R.string.generic_error
                                                ))));
                                return;
                            }

//                            Separando a StringBuffer em subStrings com cada item do cardápio
//                            Criando uma lista desses
                            List<String> listDishesLunch = new ArrayList<>();
                            List<String> listDishesDinner = new ArrayList<>();

//                            Separando os elementos
                            String auxLunch[] = stringBufferListLunch.toString().split("\n");
                            String auxDinner[] = stringBufferListDinner.toString().split("\n");

                            for (int i = 0; i < auxLunch.length; i++) {
                                try {
                                    String subStringLunch[] = auxLunch[i].split(":");
                                    listDishesLunch.add(subStringLunch[1].
                                            substring(1, subStringLunch[1].length()));

//                                    Verifica se vai precisa ser feito o parser do jantar
//                                    Se não for Sábado
                                    if (!isSaturday) {
                                            String subStringDinner[] = auxDinner[i].split(":");
                                            listDishesDinner.add(subStringDinner[1].
                                                    substring(1, subStringDinner[1].length()));
                                    }
                                } catch (Exception e) {
                                    listener.updateFailed(new UfscarResponseError(
                                            HttpURLConnection.HTTP_NOT_FOUND,
                                            new UfscarServerError(
                                                    HttpURLConnection.HTTP_NOT_FOUND,
                                                    getContext().getResources().getString(
                                                            R.string.generic_error
                                                    ))));
                                    return;
                                }
                            }

                            List<RestaurantMenuModel> restaurantMenuModelList = new ArrayList<>();
//                            Setando os objetos RestaurantMenuModel com os itens docardápio
//                            Adicionando os objetos de Cardapio na lista de Cardapio
                            restaurantMenuModelList.add(new RestaurantMenuModel(
                                            listDishesLunch.get(0),
                                            listDishesLunch.get(1),
                                            listDishesLunch.get(2),
                                            listDishesLunch.get(3),
                                            listDishesLunch.get(4),
                                            listDishesLunch.get(5),
                                            listDishesLunch.get(6),
                                            true,
                                            currentTimeMillis)
                            );
//                            Verifica se vai precisa ser feito o parser do jantar
                            if (!isSaturday) {
                                restaurantMenuModelList.add(new RestaurantMenuModel(
                                                listDishesDinner.get(0),
                                                listDishesDinner.get(1),
                                                listDishesDinner.get(2),
                                                listDishesDinner.get(3),
                                                listDishesDinner.get(4),
                                                listDishesDinner.get(5),
                                                listDishesDinner.get(6),
                                                false,
                                                currentTimeMillis)
                                );
                            }

                            validateMenu(restaurantMenuModelList, listener,
                                    currentTimeMillis, zone);
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (listener != null) {
                        listener.updateFailed(ServiceResponseError.parseError(error));
                    } else {
                        getContext().sendBroadcast(new Intent(ConstantsActions.MENU_FAILED_INTENT));
                    }
                }
            });
            VolleyManager.getInstance().addToRequestQueue(request);
        } else {
//            Hojé é DOMINGO ! Não tem cardápio
//            Solicitação do Fragmento ou do serviço de Notificacao ? Verifica
            if (listener != null) {
                listener.updateFailed(new UfscarResponseError(Constants.MenuError.IS_SUNDAY,
                        new UfscarServerError(Constants.MenuError.IS_SUNDAY,
                                Html.fromHtml(getContext().getResources().getString
                                        (R.string.menu_is_sunday_error_message)).toString())));
            } else {
                getContext().sendBroadcast(new Intent(ConstantsActions.MENU_IS_SUNDAY_INTENT));
            }
        }
    }

    /**
     * Verifica se o cardápio vindo do site é válido e caso seja, ele é formatado e enviado através
     * do listener ou através de intents como está descrito em {@link #getMenu}
     *
     * @param list              Cardápio a ser válido
     * @param listener          Listener para informar sucesso ou falha do serviço, pode ser null
     * @param currentTimeMillis Hora atual, usada para verificar se o cardápio mudou
     * @param zone              A zona do horário passado
     * @see #isMenuChange
     */
    private void validateMenu(List<RestaurantMenuModel> list,
                              MenuRestaurantListener listener,
                              long currentTimeMillis,
                              TimeZone zone) {
//        Verificando se alguma refeição está definido no SITE ou não,
//        se algo não está definido numa refeição, ela será apagada
        int i = 0;
        while (i < list.size()) {
            RestaurantMenuModel model = list.get(i);
            if (model.getMainDish().equals(Constants.MENU_NOT_DEFINED) ||
                    model.getSideDish().equals(Constants.MENU_NOT_DEFINED) ||
                    model.getRiceDish().equals(Constants.MENU_NOT_DEFINED) ||
                    model.getBeanDish().equals(Constants.MENU_NOT_DEFINED) ||
                    model.getSaladDish().equals(Constants.MENU_NOT_DEFINED) ||
                    model.getDessertDish().equals(Constants.MENU_NOT_DEFINED) ||
                    model.getDrink().equals(Constants.MENU_NOT_DEFINED)) {
                list.remove(i);
            } else {
                i++;
            }
        }

//        Se for antes do fim do almoço e tiver pelo menos o almoço definido, então
//        o cardápio é considerado definido, se for depois do fim do almoço e
//        tiver a janta definida, então o cardápio é considerado definido
        boolean isSetMenu = false;
        boolean found = false;
        int j = 0;
        Calendar calendar = Calendar.getInstance(zone);
        calendar.setTimeInMillis(currentTimeMillis);
        if (calendar.get(Calendar.HOUR_OF_DAY) <= Constants.LUNCH_ENDING) {
            while (j < list.size() && !found) {
                if (list.get(j).isLunch()) found = true;
                j++;
            }
        } else {
            while (j < list.size() && !found) {
                if (!list.get(j).isLunch()) found = true;
                j++;
            }
        }
        if (found) isSetMenu = true;

//        Tratamento dos dois casos possíveis
//        I) Cardápio está definido
//        II) Cardápio não está definido
        if (isSetMenu) {
            list = formatMenu(list);
            boolean changedMenu = isMenuChange(list, currentTimeMillis, zone);
//            Cardápio esta definido
//            Seta o CacheManager com a lista de Cardapio
//            Solicitação do Fragmento ou do serviço de Notificacao ? Verifica
            CacheManager.getInstance().setMenuCache(list);
            if (listener != null) {
                listener.updateLayout(list);
            } else {
                if (changedMenu) {
//                    Cardápio foi alterado
                    getContext().sendBroadcast(new Intent(
                            ConstantsActions.MENU_NOTIFICATION_INTENT));
                } else {
//                    Cardápio já estava atualizado
                    getContext().sendBroadcast(new Intent(
                            ConstantsActions.MENU_IS_UP_TO_DATE_INTENT));
                }
            }
        } else {
//            Cardápio não esta definido
//            Solicitação do Fragmento ou do serviço de Notificacao ? Verifica
            if (listener != null) {
//                Cardápio não definido
                listener.updateFailed(new UfscarResponseError(Constants.MenuError.NOT_DEFINED,
                        new UfscarServerError(Constants.MenuError.NOT_DEFINED,
                                Html.fromHtml(getContext().getResources().getString
                                        (R.string.menu_not_defined_error_message)).toString())));
            } else {
//                Cardápio não está definido
                getContext().sendBroadcast(new Intent(ConstantsActions.MENU_FAILED_INTENT));
            }
        }
    }

    private Context getContext() {
        return UfscarApplication.getInstance();
    }

    /**
     * Retorna Verdadeiro se algum item do cardapio foi alterado
     * Ou se o cardápio não estiver no dia atual
     * Se for antes do fim do almoço, o cardápio será considerado mudado se pelo menos um
     * dos itens da lista passada por parâmetro não estiver no cache e se for depois do
     * almoço, o cardápio será considerado mudado se a janta passada na lista do parâmetro
     * não estiver no cache
     *
     * @param listMenu    Lista com os itens de cardápio
     * @param currentTime tempo real, usado para verificar se o Cache está atualizado
     * @param zone        A zona do horário passado
     * @return um booleano com True se o cardapio mudou e False se está atualizado
     */
    private boolean isMenuChange(List<RestaurantMenuModel> listMenu, long currentTime,
                                        TimeZone zone) {
        List<RestaurantMenuModel> restaurantMenuModels = CacheManager.getInstance().getMenuCache();
        int sizeList = listMenu.size();
//        Cache vázio, cardapio não está atualizado
        if (restaurantMenuModels.isEmpty()) {
            return true;
        }
//        Cache com Data não atual, cardápio não está atualizado
        else if (!Utility.isMenuCacheUpToDate(currentTime, zone)) {
            return true;
        } else {
//            Cardapio pode ter mudado!
            Calendar calendar = Calendar.getInstance(zone);
            calendar.setTimeInMillis(currentTime);
            if (calendar.get(Calendar.HOUR_OF_DAY) < Constants.LUNCH_ENDING) {
                for (int i = 0; i < sizeList; i++) {
                    RestaurantMenuModel model = listMenu.get(i);
                    int j = 0;
                    boolean found = false;
                    while (j < restaurantMenuModels.size() && !found) {
                        if (restaurantMenuModels.get(j).equals(model)) found = true;
                        j++;
                    }
                    if (!found) return true;
                }
            } else {
                for (int i = 0; i < sizeList; i++) {
                    RestaurantMenuModel model = listMenu.get(i);
                    if (!model.isLunch()) {
                        int j = 0;
                        boolean found = false;
                        while (j < restaurantMenuModels.size() && !found) {
                            if (restaurantMenuModels.get(j).equals(model)) found = true;
                            j++;
                        }
                        if (!found) return true;
                    }
                }
            }
        }
//        Cardapio estava atualizado
        return false;
    }

    /**
     * Formata os campos dos {@link RestaurantMenuModel} da lista, separando o cardapio normal
     * do vegetariano
     *
     * @param list Lista de models que se deseja formatar
     * @return lista formatada
     * @see #formatDish
     */
    private List<RestaurantMenuModel> formatMenu(List<RestaurantMenuModel> list) {
        for (int i = 0; i < list.size(); i++) {
            RestaurantMenuModel model = list.get(i);
            if (model.getMainDish().contains(TYPE_DIVIDER)) {
                model.setMainDish(formatDish(model.getMainDish()));
            }
            if (model.getSideDish().contains(TYPE_DIVIDER)) {
                model.setSideDish(formatDish(model.getSideDish()));
            }
            if (model.getRiceDish().contains(TYPE_DIVIDER)) {
                model.setRiceDish(formatDish(model.getRiceDish()));
            }
            if (model.getBeanDish().contains(TYPE_DIVIDER)) {
                model.setBeanDish(formatDish(model.getBeanDish()));
            }
            if (model.getSaladDish().contains(TYPE_DIVIDER)) {
                model.setSaladDish(formatDish(model.getSaladDish()));
            }
            if (model.getDessertDish().contains(TYPE_DIVIDER)) {
                model.setDessertDish(formatDish(model.getDessertDish()));
            }
            if (model.getDrink().contains(TYPE_DIVIDER)) {
                model.setDrink(formatDish(model.getDrink()));
            }
            list.set(i, model);
        }
        return list;
    }

    /**
     * Formata a refeição, deixando uma linha entre a opção normal e a vegetariana
     *
     * @param dish Reifeição que se quer formatar
     * @return refeição formatada
     */
    private String formatDish(String dish) {
//        Encontrou barra que separa refeição vegetariana da normal
        boolean found = false;
        int index = dish.indexOf(TYPE_DIVIDER);
        while (!found) {
//            Procura a primeira letra antes da barra, caso seja 'c' então a barra é desconsiderada
//            pois entende-se que se quer dizer "c/" (com)
            int aux = 1;
            while ((index - aux) >= 0 && dish.charAt(index - aux) == " ".charAt(0)) aux++;
            if (dish.charAt(index - aux) == "c".charAt(0)) {
//                o indice antigo serve para verificar se há alguma outra barra caso seja um "c/"
//                pois se não houver barra indexOf() irá retornar -1 e caso contrário é porque tem
//                alguma outra barra depois do "c/"
                int oldIndex = index;
                index += dish.substring(index + 1).indexOf(TYPE_DIVIDER) + 1;
//            Se a barra for somente em "c/" retorna a refeição
                if (index == oldIndex) return dish;
            } else {
                found = true;
            }
        }
        int j = 1;
        boolean finished = false;
//        Pega o índice da primeira letra antes da barra
        while (!finished) {
            char symbol = dish.charAt(index - j);
            if (symbol != " ".charAt(0)) finished = true;
            else j++;
        }

        int k = 1;
        finished = false;
//        Pega o índice da primeira letra depois da barra
        while (!finished) {
            char symbol = dish.charAt(index + k);
            if (symbol != " ".charAt(0)) finished = true;
            else k++;
        }
        return dish.substring(0, index - j + 1) + "\n" + dish.substring(index + k);
    }
}