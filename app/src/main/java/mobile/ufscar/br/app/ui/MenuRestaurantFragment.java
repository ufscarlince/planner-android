package mobile.ufscar.br.app.ui;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Html;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.net.HttpURLConnection;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import mobile.ufscar.br.app.R;
import mobile.ufscar.br.app.config.Constants;
import mobile.ufscar.br.app.logic.CacheManager;
import mobile.ufscar.br.app.logic.CurrentTimeMillisServiceImpl;
import mobile.ufscar.br.app.logic.CurrentTimeServiceListener;
import mobile.ufscar.br.app.logic.MenuRestaurantService;
import mobile.ufscar.br.app.logic.MenuRestaurantServiceImpl;
import mobile.ufscar.br.app.logic.MenuRestaurantListener;
import mobile.ufscar.br.app.logic.UfscarApplication;
import mobile.ufscar.br.app.model.RestaurantMenuModel;
import mobile.ufscar.br.app.model.UfscarResponseError;
import mobile.ufscar.br.app.model.UfscarServerError;
import mobile.ufscar.br.app.utils.Utility;

/**
 * Fragmento da tela do Cardápio
 */

public class MenuRestaurantFragment extends Fragment implements MenuRestaurantListener,
        CurrentTimeServiceListener {

    private ListView mMenuRestauranteLv;
    private LinearLayout mDragonLl;
    private TextView mDragonPhraseTv;
    private LinearLayout mLoading;
    private MenuRestaurantService mService;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private CurrentTimeMillisServiceImpl mCurrentTimeService;
    private Button mbtnDragonMenu;

    private Context getApplication() {
        return UfscarApplication.getInstance();
    }

    /**
     * Atualiza e mostra o Fragmento com o cardápio do dia atual, Campi Sao Carlos
     * Caso o horário do celular do usuário tenha passado do almoço, só será mostrado a janta
     *
     * @param list Lista com os {@link RestaurantMenuModel} para ser populados na ListView
     */
    private void updateMenuRestaurantSC(List<RestaurantMenuModel> list) {
//        Seta o adapter com a lista passada por parâmetro
        if (getContext() != null) {
            MenuRestaurantAdapter menuRestaurantAdapter =
                    new MenuRestaurantAdapter(getCurrentMenu(list), getContext());
            mMenuRestauranteLv.setAdapter(menuRestaurantAdapter);
        }
    }

    /**
     * Remove qualquer almoço que esteja na lista de {@link RestaurantMenuModel}
     *
     * @param list Lista qualquer de modelos
     * @return Lista sem os modelos de almoço
     */
    private List<RestaurantMenuModel> removeLunch(List<RestaurantMenuModel> list) {
        int i = 0;
        while (i < list.size()) {
            if (list.get(i).isLunch()) list.remove(i);
            else i++;
        }
        return list;
    }

    /**
     * Retorna a lista a ser mostrada na ListView, depois das 14h remove-se os almoços
     *
     * @param list Lista a ser mostrada na ListView
     * @return Lista com almoço e janta, antes das 14h ou lista só com janta, depois das 14h
     */
    private List<RestaurantMenuModel> getCurrentMenu(List<RestaurantMenuModel> list) {
        Calendar calendar = Calendar.getInstance();
        if (calendar.get(Calendar.HOUR_OF_DAY) < Constants.LUNCH_ENDING) return list;
        else return removeLunch(list);
    }

    public MenuRestaurantFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_menu_restaurant, container, false);
        mMenuRestauranteLv = (ListView) rootView.findViewById(R.id.lvMenuRestaurant);
        mDragonLl = (LinearLayout) rootView.findViewById(R.id.llDragon);
        mDragonPhraseTv = (TextView) rootView.findViewById(R.id.tvDragonPhrase);
        mLoading = (LinearLayout) rootView.findViewById(R.id.llLoading);
        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.srlMenu);
        mbtnDragonMenu = (Button) rootView.findViewById(R.id.btnDragonMenu);

        mService = new MenuRestaurantServiceImpl();
        mCurrentTimeService = new CurrentTimeMillisServiceImpl();

        /**
         * Faz o swipe layout funcionar só quando mostra o primeiro elemento
         * @see <a href="http://nlopez.io/swiperefreshlayout-with-listview-done-right/">
         * http://nlopez.io/swiperefreshlayout-with-listview-done-right/</a>
         */
        mMenuRestauranteLv.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount,
                                 int totalItemCount) {
                int topRowVerticalPosition =
                        (mMenuRestauranteLv == null || mMenuRestauranteLv.getChildCount() == 0) ?
                                0 : mMenuRestauranteLv.getChildAt(0).getTop();
                mSwipeRefreshLayout.setEnabled(firstVisibleItem == 0 &&
                        topRowVerticalPosition >= 0);
            }
        });

//        Quando o usuário efetuar um swipe para baixo no Fragmento
//        Uma animação é mostrada e assim o cardápio é atualizado através do método)
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if(!Utility.isMobileDataBlocked(getContext())) {
                    mSwipeRefreshLayout.setRefreshing(false);
                    mDragonLl.setVisibility(View.GONE);
                    mMenuRestauranteLv.setVisibility(View.GONE);
                    mLoading.setVisibility(View.VISIBLE);
                    retrieveMenuFromRefresh(mMenuRestauranteLv.getVisibility() == View.VISIBLE);
                } else {
                    mMenuRestauranteLv.setVisibility(View.GONE);
                    mLoading.setVisibility(View.GONE);
                    mDragonPhraseTv.setText(R.string.data_usage_blocked_dragon_msg);
                    mDragonLl.setVisibility(View.VISIBLE);
                    mSwipeRefreshLayout.setRefreshing(false);
                }
            }
        });

//        Ação por motivo de bug na versao atual do swipe refresh
        mDragonLl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        mbtnDragonMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!Utility.isMobileDataBlocked(getContext())) {
                    mSwipeRefreshLayout.setRefreshing(false);
                    mDragonLl.setVisibility(View.GONE);
                    mMenuRestauranteLv.setVisibility(View.GONE);
                    mLoading.setVisibility(View.VISIBLE);
                    retrieveMenuFromRefresh(false);
                }
                else {
                    mDragonPhraseTv.setText(R.string.data_usage_blocked_dragon_msg);
                }
            }
        });

        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    /**
     * Requisição do cardápio através da ação do usuário de dar um swipe para baixo no Fragmento
     * Chamando assim o método OnRefreshListener que por sua fez chama este método
     * Primeiro se tenta receber o horário
     * da API, depois de recebido o horário, busca-se o cardápio.
     * Há tratamentos diferentes dependendo de como a tela se apresenta para o usuário quando ele
     * pede a requisição.
     * Caso um cardápio esteja sendo mostrado, entende-se que o usuário quer somente verificar
     * se houve alguma mudança, qualquer falha remostrará o cardápio antes da requisição e será
     * informado o erro através de um SnackBar.
     * Caso uma mensagem de erro esteja sendo mostrada, qualquer falha em obter o cardápio
     * reescreverá a mensagem mostrada antes da requisição
     * @see MenuRestaurantFragment#mSwipeRefreshLayout
     * @see SwipeRefreshLayout#setRefreshing(boolean)
     * @see Utility#showSnackbar(View, CharSequence)
     * @see SwipeRefreshLayout#setOnRefreshListener(SwipeRefreshLayout.OnRefreshListener)
     */
    private void retrieveMenuFromRefresh(final boolean menuWasVisible) {
        mCurrentTimeService.getCurrentTime(new CurrentTimeServiceListener() {
            @Override
            public void onGetTimeSuccess(long currentTime) {
                if (menuWasVisible) {
                    mService.getMenu(new MenuRestaurantListener() {
                        @Override
                        public void updateLayout(List<RestaurantMenuModel> list) {
                            MenuRestaurantFragment.this.updateLayout(list);
                        }

                        @Override
                        public void updateFailed(UfscarResponseError error) {
                            if (getContext() != null) {
                                mLoading.setVisibility(View.GONE);
                                mMenuRestauranteLv.setVisibility(View.VISIBLE);
                            }
                            String sentence;
                            switch (error.getStatusCode()) {
                                case Constants.MenuError.NOT_DEFINED: {
                                    sentence = error.getServerErrorList().get(0).getMessage();
                                    break;
                                }
                                case Constants.MenuError.IS_SUNDAY: {
                                    sentence = error.getServerErrorList().get(0).getMessage();
                                    break;
                                }
                                case HttpURLConnection.HTTP_UNAVAILABLE: {
                                    sentence = getApplication().getString(R.string.
                                            no_connection_error);
                                    break;
                                }
                                case HttpURLConnection.HTTP_NOT_FOUND: {
                                    sentence = getApplication().getString(R.string.generic_error);
                                    break;
                                }
                                default: {
                                    sentence = getApplication().getString(R.string.
                                            menu_common_error_message);
                                }
                            }
                            Utility.showSnackbar(mSwipeRefreshLayout, sentence);
                            mSwipeRefreshLayout.setRefreshing(false);
                        }
                    }, currentTime, TimeZone.getTimeZone(Constants.TIME_ZONE_API));
                } else {
                    mService.getMenu(MenuRestaurantFragment.this,
                            currentTime, TimeZone.getTimeZone(Constants.TIME_ZONE_API));
                }
            }

            @Override
            public void onGetTimeFailure(UfscarResponseError error) {
                if (getContext() != null) mLoading.setVisibility(View.GONE);
                if ((error.getStatusCode() == Constants.TimeRequestError.
                        CURRENT_TIME_MULTIPLE_REQUESTS_ERROR) &&
                        error.getServerErrorList().get(0).getCode()
                                !=HttpURLConnection.HTTP_UNAVAILABLE) {

                    Utility.showSnackbar(mSwipeRefreshLayout, Constants.TimeRequestError.
                            CURRENT_TIME_MULTIPLE_REQUESTS_ERROR_MESSAGE );

                    if (getContext() != null) {
                        if (menuWasVisible) {
                            mMenuRestauranteLv.setVisibility(View.VISIBLE);
                        } else mDragonLl.setVisibility(View.VISIBLE);
                    }
                } else {
                    if (menuWasVisible) {
                        if (getContext() != null) {
                            mMenuRestauranteLv.setVisibility(View.VISIBLE);
                        }

                        Utility.showSnackbar(mSwipeRefreshLayout, getApplication().
                                getString(R.string.no_connection_error));

                    } else {
                        MenuRestaurantFragment.this.updateFailed(error);
                    }
                }
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    @Override
    public void onStart() {
        if(!Utility.isMobileDataBlocked(getContext())) {
            mDragonLl.setVisibility(View.GONE);
            mMenuRestauranteLv.setVisibility(View.GONE);
            mLoading.setVisibility(View.VISIBLE);
            mCurrentTimeService.getCurrentTime(this);
        } else {
            mMenuRestauranteLv.setVisibility(View.GONE);
            mDragonPhraseTv.setText(R.string.data_usage_blocked_dragon_msg);
            mDragonLl.setVisibility(View.VISIBLE);
        }
        super.onStart();
    }

    /**
     * Sucesso em pegar o cardápio do site, o cardápio é mostrado ao usuário se a lista não for
     * vazia, a lista mostrada ao usuário pode estar vazia quando já se passou do almoço e a
     * lista vindo do site só contém almoço
     *
     * @param list lista proveniente do site, pode conter só almoço ou só janta ou os dois
     */
    @Override
    public void updateLayout(List<RestaurantMenuModel> list) {
        if (getCurrentMenu(list).isEmpty()) {
            this.updateFailed(new UfscarResponseError(Constants.MenuError.NOT_DEFINED,
                    new UfscarServerError(Constants.MenuError.NOT_DEFINED,
                            Html.fromHtml(getContext().getResources().getString
                                    (R.string.menu_not_defined_error_message)).toString())));
        } else {
            mLoading.setVisibility(View.GONE);
            mDragonLl.setVisibility(View.GONE);
            updateMenuRestaurantSC(list);
            mMenuRestauranteLv.setVisibility(View.VISIBLE);
        }
//        Desativando a animação de atualização se está estiver sendo exibida
//        No caso, se o usuário deu um swipe no fragmento
        mSwipeRefreshLayout.setRefreshing(false);
    }

    /**
     * Falha em pegar o cardápio do site, o dragão é mostrado junto de uma mensagem de acordo
     * com o erro
     *
     * @param error Erro ocorrido, não necessáriamente algum erro relacionado ao servidor, mas
     *              qualquer situação que não resultou em conseguir o cardápio, erros tratados
     *              especificamente são os abaixo e qualquer outro será tratado como erro
     *              de servidor
     * @see mobile.ufscar.br.app.config.Constants.MenuError#NOT_DEFINED
     * @see mobile.ufscar.br.app.config.Constants.MenuError#IS_SUNDAY
     * @see HttpURLConnection#HTTP_UNAVAILABLE
     */
    @Override
    public void updateFailed(UfscarResponseError error) {
        if (getContext() != null) {
            mMenuRestauranteLv.setVisibility(View.GONE);
            mLoading.setVisibility(View.GONE);

            switch (error.getStatusCode()) {
                case Constants.MenuError.NOT_DEFINED: {
                    mDragonPhraseTv.setText(error.getServerErrorList().get(0).getMessage());
                    break;
                }
                case Constants.MenuError.IS_SUNDAY: {
                    mDragonPhraseTv.setText(error.getServerErrorList().get(0).getMessage());
                    break;
                }
                case HttpURLConnection.HTTP_UNAVAILABLE: {
                    mDragonPhraseTv.setText(Html.fromHtml(getContext().getResources().getString(
                            R.string.no_connection_error_empty_state_message)));
                    break;
                }
                case HttpURLConnection.HTTP_NOT_FOUND: {
                    mDragonPhraseTv.setText(Html.fromHtml(getContext().getResources().getString(
                            R.string.generic_error)));
                    break;
                }
                default: {
//                    Qualquer outro erro é tratado como um erro genérico
                    mDragonPhraseTv.setText(Html.fromHtml(getContext().getResources().getString(
                            R.string.menu_common_error_message)));
                }
            }

            mDragonLl.setVisibility(View.VISIBLE);
        }
//        Desativando a animação de atualização se está estiver sendo exibida
//        No caso, se o usuário deu um swipe no fragmento
        mSwipeRefreshLayout.setRefreshing(false);
    }

    /**
     * Sucesso em pegar o horário da API, primeiro se verifica se o cardápio do cache está no mesmo
     * dia que o horário da API, se estiver, o cardápio do cache é mostrado, se não estiver, tenta
     * pegar o cardápio do site
     *
     * @param currentTime horário da API
     * @see Utility#isMenuCacheUpToDate
     */
    @Override
    public void onGetTimeSuccess(long currentTime) {
//        Com o tempo real resgatado da API
//        Podemos chamar o método HtmlParser, que recebe esse
        if (Utility.isMenuCacheUpToDate(currentTime,
                TimeZone.getTimeZone(Constants.TIME_ZONE_API))) {
            MenuRestaurantFragment.this.updateLayout(CacheManager.getInstance().getMenuCache());
        } else {
            mService.getMenu(this, currentTime,
                    TimeZone.getTimeZone(Constants.TIME_ZONE_API));
        }
    }

    /**
     * Tratamento de falha da API de login, primeiro se verifica se o cardápio do cache está no
     * mesmo dia que o celular do usuário, se estiver, o cardápio do cache é mostrado, se não
     * estiver, o dragão será mostrado com uma mensagem de erro
     *
     * @param error erro que a API manda, já transformado em UfscarResponseError
     * @see Utility#isMenuCacheUpToDate
     * @see Constants.TimeRequestError#CURRENT_TIME_MULTIPLE_REQUESTS_ERROR
     */
    @Override
    public void onGetTimeFailure(UfscarResponseError error) {
        if (getContext() != null) {
            Calendar calendar = Calendar.getInstance();
            if (Utility.isMenuCacheUpToDate(calendar.getTimeInMillis(), TimeZone.getDefault())) {
                MenuRestaurantFragment.this.updateLayout(CacheManager.getInstance().getMenuCache());
            } else {
                this.updateFailed(error);
            }
        }
    }
}