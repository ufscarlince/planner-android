package mobile.ufscar.br.app.model;

public class ScheduleModel {

    private int mWeekDay;
    private int mStartTime;
    private int mFinishTime;
    private String mSala;

    public static final int START_TIME = 0;
    public static final int FINISH_TIME = 24;


    public ScheduleModel() {
    }

    public ScheduleModel(int weekDay, int startTime, int finishTime, String sala) {
        this.mWeekDay = weekDay;
        this.mStartTime = startTime;
        this.mFinishTime = finishTime;
        this.mSala = sala;
    }

    public ScheduleModel(int weekDay, int startTime, int finishTime) {
        this.mWeekDay = weekDay;
        this.mStartTime = startTime;
        this.mFinishTime = finishTime;
    }

    public int getWeekDay() {
        return mWeekDay;
    }

    public boolean setWeekDay(int weekDay) {
        if ((weekDay > 7) || (weekDay < 0)) {
            return false;
        } else {
            this.mWeekDay = weekDay;
            return true;
        }
    }

    public int getStartTime() {
        return mStartTime;
    }

    public boolean setStartTime(int startTime) {
        if (startTime > 23) {
            return false;
        } else {
            this.mStartTime = startTime;
            return true;
        }
    }

    public int getFinishTime() {
        return mFinishTime;
    }

    public boolean setFinishTime(int finishTime) {
        if (finishTime > 24) {
            return false;
        } else {
            this.mFinishTime = finishTime;
            return true;
        }
    }

    public String getSala() {
        return mSala;
    }

    public void setSala(String mSala) {
        this.mSala = mSala;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (o == this) {
            return true;
        }
        if (o.getClass() != getClass()) {
            return false;
        }
        ScheduleModel rhs = (ScheduleModel) o;
        return mWeekDay == rhs.mWeekDay && mStartTime == rhs.mStartTime &&
                mFinishTime == rhs.mFinishTime;
    }
}
