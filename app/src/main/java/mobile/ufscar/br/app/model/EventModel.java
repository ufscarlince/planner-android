package mobile.ufscar.br.app.model;

public class EventModel {

    private boolean mRealEvent;
    private String mName;
    private ScheduleModel mSchedule;
    private LocalModel mLocal;

    public EventModel(String name, ScheduleModel schedule, LocalModel local) {
        this.setLocal(local);
        this.setName(name);
        this.setSchedule(schedule);
        this.setRealEvent(true);
    }

    public EventModel(ScheduleModel schedule) {
        this.setRealEvent(false);
        this.setSchedule(schedule);
    }

    public boolean isRealEvent() {
        return mRealEvent;
    }

    public void setRealEvent(boolean realEvent) {
        this.mRealEvent = realEvent;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public LocalModel getLocal() {
        return mLocal;
    }

    public void setLocal(LocalModel local) {
        this.mLocal = local;
    }

    public ScheduleModel getSchedule() {
        return mSchedule;
    }

    public void setSchedule(ScheduleModel schedule) {
        this.mSchedule = schedule;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (o == this) {
            return true;
        }
        if (o.getClass() != getClass()) {
            return false;
        }
        EventModel rhs = (EventModel) o;
        return mSchedule.equals(rhs.mSchedule);
    }
}