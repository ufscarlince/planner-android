package mobile.ufscar.br.app.logic;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import mobile.ufscar.br.app.R;
import mobile.ufscar.br.app.config.Constants;
import mobile.ufscar.br.app.model.EventModel;
import mobile.ufscar.br.app.utils.Utility;

/**
 * Essa é a classe do Adapter do list view do Daily Widget
 */

public class DailyWidgetListProvider implements RemoteViewsService.RemoteViewsFactory {

    private List<EventModel> eventList = new ArrayList<EventModel>();
    private Context context = null;
    private PendingIntent mPendingIntent;
    public DailyWidgetListProvider(Context context, Intent intent) {
        this.context = context;
        populateListItem();
    }

    /**
     * Busca a lista de eventos no cache.
     */
    private void populateListItem() throws IllegalStateException{
        Calendar calendar = Calendar.getInstance();
        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
        switch (dayOfWeek) {
            case Calendar.SUNDAY:
                dayOfWeek = Constants.DOMINGO;
                break;
            case Calendar.MONDAY:
                dayOfWeek = Constants.SEGUNDA;
                break;
            case Calendar.TUESDAY:
                dayOfWeek = Constants.TERCA;
                break;
            case Calendar.WEDNESDAY:
                dayOfWeek = Constants.QUARTA;
                break;
            case Calendar.THURSDAY:
                dayOfWeek = Constants.QUINTA;
                break;
            case Calendar.FRIDAY:
                dayOfWeek = Constants.SEXTA;
                break;
            case Calendar.SATURDAY:
                dayOfWeek = Constants.SABADO;
                break;
            default:
                throw new IllegalStateException("Invalid day: " + dayOfWeek);

        }
        eventList = Manager.getEventDaily(dayOfWeek);
    }

    @Override
    public int getCount() {
        return eventList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    /**
     * Popula os itens da listView
     */
    @Override
    public RemoteViews getViewAt(int position) {
        RemoteViews remoteView = new RemoteViews(
                context.getPackageName(), R.layout.daily_widget_list_row);
        EventModel eventItem = eventList.get(position);
        remoteView.setTextViewText(R.id.tvEventName, eventItem.getName());
        remoteView.setTextViewText(R.id.tvSchedule, Utility.
                formatScheduleEvent(eventItem.getSchedule()));
        remoteView.setTextViewText(R.id.tvLocal, Utility.formatLocation(eventItem.getLocal()));
        return remoteView;
    }

    @Override
    public RemoteViews getLoadingView() {
        return null;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    /**
     * Chamado quando o Widget é criado.
     */
    @Override
    public void onCreate() {
//        Cria um timer para atualizar o Widget a meia noite
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.roll(Calendar.DAY_OF_MONTH, true);
        calendar.set(Calendar.HOUR_OF_DAY, Constants.DailyWidget.WIDGET_UPDATE_HOUR);
        calendar.set(Calendar.MINUTE, Constants.DailyWidget.WIDGET_UPDATE_MINUTE);
        Intent myIntent = new Intent(context, WidgetAlarmReceiver.class);
        mPendingIntent = PendingIntent.getBroadcast(context, Constants.DailyWidget.WIDGET_REQUEST_CODE,
                myIntent, Constants.DailyWidget.WIDGET_FLAG);
        AlarmManager alarmManager = (AlarmManager)context.getSystemService(
                Context.ALARM_SERVICE);
        alarmManager.setRepeating(AlarmManager.RTC, calendar.getTimeInMillis(),
                AlarmManager.INTERVAL_DAY, mPendingIntent);
    }

    /**
     * Chamado quando há alterações no conjunto de dados.
     */
    @Override
    public void onDataSetChanged() {
        populateListItem();
    }

    /**
     * Chamado quando o Widget é excluido.
     */
    @Override
    public void onDestroy() {
//        Cancela o Alarme que atualiza o Widget.
        AlarmManager alarmManager = (AlarmManager)context.getSystemService(
                Context.ALARM_SERVICE);
        alarmManager.cancel(mPendingIntent);
    }
}