package mobile.ufscar.br.app.logic;

public interface RadioServiceListener {

    void radioStartingToPlay();
    void radioPlaying();
    void radioStopped();
    void radioPaused();
    void radioFailed();
    void setDragonRadioLayout();
}
