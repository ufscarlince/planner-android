package mobile.ufscar.br.app.logic;

import java.util.List;

import mobile.ufscar.br.app.model.RestaurantMenuModel;
import mobile.ufscar.br.app.model.UfscarResponseError;

/**
 * Callback usado por {@link MenuRestaurantService} quando houve sucesso ou não em gerar o Cardápio
 * do dia
 */

public interface MenuRestaurantListener {

    /**
     * Callback chamado quando houve sucesso em gerar um cardápio válido, ao receber esse método
     * {@link CacheManager} já terá a lista passada por parâmetro, então não é necessário
     * atualizá-lo
     *
     * @param list Lista gerada pela requisição, contendo o cardápio do almoço e da janta
     */
    void updateLayout(List<RestaurantMenuModel> list);

    /**
     * Callback chamado quando não foi possível gerar um cardápio válido
     *
     * @param error Erro ocorrido, não necessáriamente algum erro relacionado ao servidor, mas
     *              qualquer situação que não resultou em conseguir o cardápio, além dos códigos
     *              retornados pelo servidor, o código de {@link UfscarResponseError} pode ser os
     *              abaixo
     * @see mobile.ufscar.br.app.config.Constants.MenuError#IS_SUNDAY
     * @see mobile.ufscar.br.app.config.Constants.MenuError#NOT_DEFINED
     */
    void updateFailed(UfscarResponseError error);
}
