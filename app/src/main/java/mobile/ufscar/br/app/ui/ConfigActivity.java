package mobile.ufscar.br.app.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;

import mobile.ufscar.br.app.R;

/**
 * Activity que mostra as preferências do usuário
 */

public class ConfigActivity extends AppCompatActivity {

    private ImageButton mBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_config);

        mBack = (ImageButton) findViewById(R.id.ibBack);
        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        Toolbar toolbar = (Toolbar) findViewById(R.id.tbConfig);
        setSupportActionBar(toolbar);

        if (savedInstanceState == null) {
            ConfigFragment configFragment = new ConfigFragment();

            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragmentConfig, configFragment)
                    .commit();
        }
    }
}