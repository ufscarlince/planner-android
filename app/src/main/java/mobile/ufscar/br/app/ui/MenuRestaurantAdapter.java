package mobile.ufscar.br.app.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import mobile.ufscar.br.app.R;
import mobile.ufscar.br.app.config.Constants;
import mobile.ufscar.br.app.model.RestaurantMenuModel;

/**
 * Adapter para popular uma listView dada uma lista de {@link RestaurantMenuModel}
 */

public class MenuRestaurantAdapter extends BaseAdapter {

    private List<RestaurantMenuModel> mRestaurantMenuModels;
    private LayoutInflater mLayoutInflater;

    public MenuRestaurantAdapter(List<RestaurantMenuModel> restaurantMenuModels, Context context) {
        this.mRestaurantMenuModels = restaurantMenuModels;
        this.mLayoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return mRestaurantMenuModels.size();
    }

    @Override
    public RestaurantMenuModel getItem(int position) {
        return mRestaurantMenuModels.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolderRestaurantMenu viewHolder;
        RestaurantMenuModel menu = mRestaurantMenuModels.get(position);

        View view = convertView;
        if (view == null) {
            view = mLayoutInflater.inflate(R.layout.menu_restaurant_cell, parent, false);

            TextView tvLunchDinner = (TextView) view.findViewById(R.id.tvLunchDinner);
            TextView tvMainDish = (TextView) view.findViewById(R.id.tvMainDish);
            TextView tvSideDish = (TextView) view.findViewById(R.id.tvSideDish);
            TextView tvRiceDish = (TextView) view.findViewById(R.id.tvRiceDish);
            TextView tvBeanDish = (TextView) view.findViewById(R.id.tvBeanDish);
            TextView tvSaladDish = (TextView) view.findViewById(R.id.tvSaladDish);
            TextView tvDessertDish = (TextView) view.findViewById(R.id.tvDessertDish);
            TextView tvDrink = (TextView) view.findViewById(R.id.tvDrink);

            viewHolder = new ViewHolderRestaurantMenu(
                    tvLunchDinner, tvMainDish, tvSideDish, tvRiceDish, tvBeanDish, tvSaladDish,
                    tvDessertDish, tvDrink);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolderRestaurantMenu) view.getTag();
        }

        if (menu.isLunch()) viewHolder.getTvLunchDinner().setText(Constants.MENU_LUNCH);
        else viewHolder.getTvLunchDinner().setText(Constants.MENU_DINNER);
        viewHolder.getTvMainDish().setText(menu.getMainDish());
        viewHolder.getTvSideDish().setText(menu.getSideDish());
        viewHolder.getTvRiceDish().setText(menu.getRiceDish());
        viewHolder.getTvBeanDish().setText(menu.getBeanDish());
        viewHolder.getTvSaladDish().setText(menu.getSaladDish());
        viewHolder.getTvDessertDish().setText(menu.getDessertDish());
        viewHolder.getTvDrink().setText(menu.getDrink());

        return view;
    }
}
