package mobile.ufscar.br.app.logic;

import org.json.JSONObject;

/**
 * Interface utilizada para realizar a requisição de autenticação do usuário,
 * Token de Acesso
 */

public interface LoginService {

    /**
     * Método utilizado para realizar a autenticação do usuário
     *
     * @param loginListener Contendo os métodos a serem implementados em caso de sucesso
     *                      ou falha
     * @param loginBody Corpo da requisição feita ao servidor
     *                  Formato: {"password": "RA/CPF", "username": "SENHA"}
     * @see LoginListener
     */
    void getLoginToken(final LoginListener loginListener,
                       final JSONObject loginBody);
}

