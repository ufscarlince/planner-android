package mobile.ufscar.br.app.ui;

import android.widget.TextView;

public class ViewHolderRestaurantMenu {

    private TextView mTvLunchDinner;
    private TextView mTvMainDish;
    private TextView mTvSideDish;
    private TextView mTvRiceDish;
    private TextView mTvBeanDish;
    private TextView mTvSaladDish;
    private TextView mTvDessertDish;
    private TextView mTvDrink;

    public ViewHolderRestaurantMenu(TextView tvLunchDinner, TextView tvMainDish,
                                    TextView tvSideDish, TextView tvRiceDish,
                                    TextView tvBeanDish, TextView tvSaladDish,
                                    TextView tvDessertDish, TextView tvDrink) {
        this.mTvLunchDinner = tvLunchDinner;
        this.mTvMainDish = tvMainDish;
        this.mTvSideDish = tvSideDish;
        this.mTvRiceDish = tvRiceDish;
        this.mTvBeanDish = tvBeanDish;
        this.mTvSaladDish = tvSaladDish;
        this.mTvDessertDish = tvDessertDish;
        this.mTvDrink = tvDrink;
    }

    public TextView getTvLunchDinner() {
        return mTvLunchDinner;
    }

    public void setTvLunchDinner(TextView tvLunchDinner) {
        this.mTvLunchDinner = tvLunchDinner;
    }

    public TextView getTvSideDish() {
        return mTvSideDish;
    }

    public void setTvSideDish(TextView tvSideDish) {
        this.mTvSideDish = tvSideDish;
    }

    public TextView getTvMainDish() {
        return mTvMainDish;
    }

    public void setTvMainDish(TextView tvMainDish) {
        this.mTvMainDish = tvMainDish;
    }

    public TextView getTvRiceDish() {
        return mTvRiceDish;
    }

    public void setTvRiceDish(TextView tvRiceDish) {
        this.mTvRiceDish = tvRiceDish;
    }

    public TextView getTvBeanDish() {
        return mTvBeanDish;
    }

    public void setTvBeanDish(TextView tvBeanDish) {
        this.mTvBeanDish = tvBeanDish;
    }

    public TextView getTvSaladDish() {
        return mTvSaladDish;
    }

    public void setTvSaladDish(TextView tvSaladDish) {
        this.mTvSaladDish = tvSaladDish;
    }

    public TextView getTvDessertDish() {
        return mTvDessertDish;
    }

    public void setTvDessertDish(TextView tvDessertDish) {
        this.mTvDessertDish = tvDessertDish;
    }

    public TextView getTvDrink() {
        return mTvDrink;
    }

    public void setTvDrink(TextView tvDrink) {
        this.mTvDrink = tvDrink;
    }
}
