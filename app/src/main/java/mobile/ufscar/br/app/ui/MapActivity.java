package mobile.ufscar.br.app.ui;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import mobile.ufscar.br.app.R;

/**
 * Contém o fragmento do mapa {@link MapFragment}.
 * Utilizada quando se pressiona e segura em um evento em {@link DailyVisionFragment} e se escolhe a
 * opção "Visualizar o mapa" e em {@link EditVisionActivity} ao clicar no botão de mapa.
 */

public class MapActivity extends AppCompatActivity {

    public static final String VISUALIZE_MAP = "visualizar mapa";

    private Toolbar mToolbar;
    private ImageButton mBack;
    private TextView mDone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        setContentView(R.layout.activity_map);

        mToolbar = (Toolbar) findViewById(R.id.tbMapActivity);
        setSupportActionBar(mToolbar);

        mBack = (ImageButton) findViewById(R.id.ibBack);
        mDone = (TextView) findViewById(R.id.tvDone);

        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ComponentName activity = getCallingActivity();
                String activityName = "";
                if (activity != null) {
                    activityName = activity.getClassName();
                }

                String compareName = EditVisionActivity.class.getName();
                if (activity != null &&
                        activityName.equals(compareName)) {
                    Intent intent = new Intent();
                    intent.putExtra(EditVisionActivity.PLACE_PICK,
                            MapFragment.getAutoCompleteText());
                    setResult(Activity.RESULT_OK, intent);
                    MapFragment.resetAutoCompleteText();
                }
                onBackPressed();
            }
        });

        if (intent != null && intent.getBooleanExtra(VISUALIZE_MAP, false)) {
            mDone.setVisibility(View.GONE);
        }

        if (savedInstanceState == null) {
            MapContainerFragment mapFragment = new MapContainerFragment();

            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragmentMap, mapFragment)
                    .commit();
        }
    }
}