package mobile.ufscar.br.app.logic;

/**
 * Interface da requisição para buscar o horário
 */

public interface CurrentTimeMillisService {

    /**
     * Busca o horário na API
     *
     * @param listener que contem os métodos para o caso de sucesso ou falha, pode ser passado
     *                 como null, nesse caso são mandados broadcasts com as actions necessárias
     */
    void getCurrentTime(final CurrentTimeServiceListener listener);
}