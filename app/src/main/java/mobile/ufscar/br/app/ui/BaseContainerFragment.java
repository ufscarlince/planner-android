package mobile.ufscar.br.app.ui;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import mobile.ufscar.br.app.R;

/**
 * Classe base para layouts que somente mudam fragments
 */

public abstract class BaseContainerFragment extends Fragment {

    /**
     * Realiza a troca de um {@link Fragment} por outro, é possível salvar o fragment anterior caso
     * necessário posteriormente
     *
     * @param fragment que se tornará o atual
     * @param addToBackStack {@link Boolean} que indica se vai ser preciso salvar ou não o anterior
     */
    public void replaceFragment(Fragment fragment, boolean addToBackStack) {
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        if (addToBackStack) {
            transaction.addToBackStack(null);
        }
        transaction.replace(R.id.flContainer, fragment);
        transaction.commit();
        getChildFragmentManager().executePendingTransactions();
    }

    /**
     * Mostra o fragment anterior, desde que tenha sido salvo
     *
     * @return true se foi possível mostrar o fragment anterior ou false caso contrário
     */
    public boolean popFragment() {
        if (getChildFragmentManager().getBackStackEntryCount() > 0) {
            getChildFragmentManager().popBackStack();
            return true;
        }
        return false;
    }
}