package mobile.ufscar.br.app.ui;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;

import mobile.ufscar.br.app.R;
import mobile.ufscar.br.app.logic.CacheManager;

/**
 * Activity para criar e controlar o Onbarding inicial do aplicativo
 */

public class OnboardingActivity extends Activity {

    private OnboardingPagerAdapter mOnboardingPagerAdapter;
//    View page que terá o conteúdo da página requerida
    private ViewPager mViewPager;
    private Button mOnboardingButtom;
    private ImageView mButtomMarkOne;
    private ImageView mButtomMarkTwo;

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_onboarding);

//        Ocultando a statusBar
        this.getWindow().setFlags(WindowManager.LayoutParams.
                FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

//        Criando um adapter que irá gerenciar os fragmentos utilizados
        mOnboardingPagerAdapter = new OnboardingPagerAdapter(getFragmentManager());

        mViewPager = (ViewPager) findViewById(R.id.vpOnboardingActivity);
        mViewPager.setAdapter(mOnboardingPagerAdapter);
        mOnboardingButtom = (Button) findViewById(R.id.btnOnboarding);
        mButtomMarkOne = (ImageView) findViewById(R.id.ivOnboardingMarkOne);
        mButtomMarkTwo = (ImageView) findViewById(R.id.ivOnboardingMarkTwo);

//        Primeira ação do botão, na abertura do onboarding
        mOnboardingButtom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewPager.setCurrentItem(1);
            }
        });

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset,
                                       int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        mOnboardingButtom.setText(getApplication().getApplicationContext().
                                getText(R.string.onboarding_activity_buttom_msg_one));
                        mButtomMarkOne.setImageResource(R.drawable.onboarding_circle_red);
                        mButtomMarkTwo.setImageResource(R.drawable.onboarding_circle_white);

                        mOnboardingButtom.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mOnboardingButtom.setText(getApplication().getApplicationContext().
                                        getText(R.string.onboarding_activity_buttom_msg_two));
                                mViewPager.setCurrentItem(1);
                            }
                        });
                        break;
                    case 1:
                        mOnboardingButtom.setText(getApplication().getApplicationContext().
                                getText(R.string.onboarding_activity_buttom_msg_two));
                        mButtomMarkOne.setImageResource(R.drawable.onboarding_circle_white);
                        mButtomMarkTwo.setImageResource(R.drawable.onboarding_circle_red);

                        mOnboardingButtom.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
//                                Chamando Login
                                Intent intent = new Intent(getApplication(), LoginActivity.class);
                                CacheManager.getInstance().setFirstLaunchCache(false);
                                startActivity(intent);
                                finish();
                            }
                        });
                        break;
                }
            }
            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }
}