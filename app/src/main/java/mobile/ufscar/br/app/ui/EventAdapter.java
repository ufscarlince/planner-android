package mobile.ufscar.br.app.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import mobile.ufscar.br.app.R;
import mobile.ufscar.br.app.model.EventModel;
import mobile.ufscar.br.app.utils.Utility;

/**
 * Esta classe , se baseia no adapter para a listView presente em Daily Fragment
 */

public class EventAdapter extends BaseAdapter {

    private LayoutInflater mLayoutInflater;
    private List<EventModel> mObjects;

    public EventAdapter(List<EventModel> objects, Context context) {
        this.mObjects = objects;
        mLayoutInflater = LayoutInflater.from(context);
    }

//    As células da ListView possuem dois layouts diferentes, logo em tipo de View, retorna se 2
    @Override
    public int getViewTypeCount() {
        return 2;
    }

//    Aqui define qual o tipo de layout a ser usado, de acordo com o Layout
    @Override
    public int getItemViewType(int position) {
        return mObjects.get(position).isRealEvent() ? 1 : 0;
    }

    @Override
    public int getCount() {
        return mObjects.size();
    }

    @Override
    public EventModel getItem(int position) {
        return mObjects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

//    Escolhe o layout de acordo com o tipo de evento e preenche esse com os dados do evento
//    Retorna a View escolhida
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolderEvent viewHolderEvent = null;
        View view = convertView;
        EventModel lvItem = mObjects.get(position);

        boolean lvItemType = mObjects.get(position).isRealEvent();

        if (view == null) {

            if (lvItemType) {
                view = mLayoutInflater.inflate(R.layout.event_cell, parent, false);

                TextView tvScheduleEvent = (TextView) view.findViewById(R.id.tvScheduleEvent);
                TextView tvNameEventCell = (TextView) view.findViewById(R.id.tvNameEventCell);
                TextView tvLocation = (TextView) view.findViewById(R.id.tvLocation);

                viewHolderEvent = new ViewHolderEvent(tvScheduleEvent, tvNameEventCell, tvLocation);
                view.setTag(viewHolderEvent);
            } else {
                view = mLayoutInflater.inflate(R.layout.empty_cell, parent, false);

                TextView tvScheduleEmptyCell = (TextView) view.findViewById(R.id.tvScheduleEmptyCell);

                viewHolderEvent = new ViewHolderEvent(tvScheduleEmptyCell);
                view.setTag(viewHolderEvent);
            }
        } else {
            viewHolderEvent = (ViewHolderEvent) view.getTag();
        }

        if (lvItemType) {
            viewHolderEvent.getTvScheduleEvent().setText(
                    Utility.formatScheduleEvent(lvItem.getSchedule()));
            viewHolderEvent.getTvNameEventCell().setText(lvItem.getName());
            viewHolderEvent.getTvLocation().setText(Utility.formatLocation(lvItem.getLocal()));
        } else
            viewHolderEvent.getTvScheduleEmptyCell().setText(
                    Utility.formatScheduleEvent(lvItem.getSchedule()));

        return view;
    }
}
