package mobile.ufscar.br.app.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import mobile.ufscar.br.app.R;

/**
 * Container dos {@link android.support.v4.app.Fragment} do mapa
 */

public class MapContainerFragment extends BaseContainerFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_container, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        replaceFragment(new MapEnableFragment(), false);
    }
}