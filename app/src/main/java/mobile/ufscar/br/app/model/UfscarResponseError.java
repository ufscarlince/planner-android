package mobile.ufscar.br.app.model;

import java.util.ArrayList;
import java.util.List;

import mobile.ufscar.br.app.utils.ServiceResponseError;

/**
 * Erros de requisição são remapeados para essa model, o remapeamento é feito por
 * {@link ServiceResponseError}
 */

public class UfscarResponseError {

    /**
     * Código de erro vindo do servidor, no caso em que o usuário está sem internet ou a requisição
     * demorar demais, será atribuído {@link java.net.HttpURLConnection#HTTP_UNAVAILABLE}. Se
     * não for possível averiguar qual foi o erro, será atribuído
     * {@link java.net.HttpURLConnection#HTTP_INTERNAL_ERROR}
     */
    private int mStatusCode;
    /**
     * Lista contendo as respostas vindas do servidor
     * @see UfscarServerError
     */
    private List<UfscarServerError> mServerErrorList;

    public UfscarResponseError() {
    }

    public UfscarResponseError(int statusCode, List<UfscarServerError> serverErrorList) {
        this.mStatusCode = statusCode;
        this.mServerErrorList = serverErrorList;
    }

    public UfscarResponseError(int statusCode, UfscarServerError serverError) {
        this.mStatusCode = statusCode;
        this.mServerErrorList = new ArrayList<>();
        this.mServerErrorList.add(serverError);
    }

    public int getStatusCode() {
        return mStatusCode;
    }

    public void setStatusCode(int statusCode) {
        this.mStatusCode = statusCode;
    }

    public List<UfscarServerError> getServerErrorList() {
        return mServerErrorList;
    }

    public void setServerErrorList(List<UfscarServerError> serverErrorList) {
        this.mServerErrorList = serverErrorList;
    }

    @Override
    public String toString() {
        return "UfscarResponseError{" +
                "statusCode = " + mStatusCode +
                ", serverErrorList = " + mServerErrorList +
                '}';
    }
}
