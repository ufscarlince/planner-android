package mobile.ufscar.br.app.logic.receivers;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import java.util.Calendar;
import java.util.TimeZone;

import mobile.ufscar.br.app.R;
import mobile.ufscar.br.app.config.Constants;
import mobile.ufscar.br.app.config.ConstantsActions;
import mobile.ufscar.br.app.logic.CacheManager;
import mobile.ufscar.br.app.logic.CurrentTimeMillisServiceImpl;
import mobile.ufscar.br.app.logic.MenuRestaurantService;
import mobile.ufscar.br.app.logic.MenuRestaurantServiceImpl;
import mobile.ufscar.br.app.ui.MainActivity;

/**
 * Trata do receiver do Cardápio
 */

public class CardapioReceiver extends BroadcastReceiver {

    /**
     * @throws IllegalStateException se forem setadas nenhuma action já definida
     */
    @Override
    public void onReceive(Context context, Intent intent) {
        if (CacheManager.getInstance().getFirstLaunchCache()) {
            setAlarmToNextPeriod(context);
            return;
        }

        if (!CacheManager.getInstance().getConfigCache().isNotification() || CacheManager.
                getInstance().getConfigCache().isDataUsageBlocked()) return;

        switch (intent.getAction()) {
            case ConstantsActions.CHECK_CARDAPIO: {
                Log.d(Constants.CardapioReceiver.LOG_TAG,
                        Constants.CardapioReceiver.CHECK_CARDAPIO);
                CurrentTimeMillisServiceImpl currentTimeService =
                        new CurrentTimeMillisServiceImpl();
                currentTimeService.getCurrentTime(null);
                break;
            }

            case ConstantsActions.MENU_IS_UP_TO_DATE_INTENT: {
                Log.d(Constants.CardapioReceiver.LOG_TAG, Constants.CardapioReceiver.UP_TO_DATE);
                cancelAlarm(context);
                setAlarmToNextPeriod(context);
                break;
            }

            case ConstantsActions.MENU_IS_SUNDAY_INTENT: {
                Log.d(Constants.CardapioReceiver.LOG_TAG, Constants.CardapioReceiver.SUNDAY);
                cancelAlarm(context);
                setAlarmToNextDay(context);
                break;
            }

            case ConstantsActions.MENU_NOTIFICATION_INTENT: {
                Log.d(Constants.CardapioReceiver.LOG_TAG, Constants.CardapioReceiver.NOTIFICATION);
                Intent notificationIntent = new Intent(context, MainActivity.class);
                notificationIntent.putExtra(Constants.Notification.MENU_RESTAURANT_TAB_KEY,
                        Constants.MainPagerAdapter.MENU_RESTAURANT);
                notificationRu(context, notificationIntent);
                cancelAlarm(context);
                setAlarmToNextPeriod(context);
                break;
            }

            case ConstantsActions.MENU_FAILED_INTENT: {
                Log.d(Constants.CardapioReceiver.LOG_TAG, Constants.CardapioReceiver.FAILED);
                cancelAlarm(context);
                setFailedAlarm(context);
                break;
            }

            case ConstantsActions.TIME_SUCCESS_INTENT: {
                Log.d(Constants.CardapioReceiver.LOG_TAG, Constants.CardapioReceiver.SUCCESS_TIME);
                long currentTime = intent.getLongExtra(ConstantsActions.CURRENT_TIME, 0);
                if (currentTime != 0) {
                    MenuRestaurantService service = new MenuRestaurantServiceImpl();
                    service.getMenu(null, currentTime,
                            TimeZone.getTimeZone(Constants.TIME_ZONE_API));
                }
                break;
            }

            case ConstantsActions.TIME_FAILURE_INTENT: {
                Log.d(Constants.CardapioReceiver.LOG_TAG, Constants.CardapioReceiver.FAILURE_TIME);
                cancelAlarm(context);
                setFailedAlarm(context);
                break;
            }

            case Intent.ACTION_BOOT_COMPLETED: {
                Log.d(Constants.CardapioReceiver.LOG_TAG, Constants.CardapioReceiver.BOOT);
                setAlarmToNextPeriod(context);
                break;
            }

            case Intent.ACTION_TIMEZONE_CHANGED: {
                Log.d(Constants.CardapioReceiver.LOG_TAG, Constants.CardapioReceiver.TIMEZONE);
                cancelAlarm(context);
                setAlarmToNextPeriod(context);
                break;
            }

            case Intent.ACTION_TIME_CHANGED: {
                Log.d(Constants.CardapioReceiver.LOG_TAG, Constants.CardapioReceiver.TIME);
                cancelAlarm(context);
                setAlarmToNextPeriod(context);
                break;
            }

            default: {
                throw new IllegalStateException(Constants.CardapioReceiver.INVALID_ACTION +
                        intent.getAction());
            }
        }
    }

    /**
     * Define o alarm para a action {@link ConstantsActions#CHECK_CARDAPIO} nos melhores
     * horários para verificar o cardápio no site.
     *
     * @see Constants#LUNCH_BEGINNING
     * @see Constants#DINNER_BEGINNING
     */
    private void setAlarmToNextPeriod(Context context) {
        Log.d(Constants.CardapioReceiver.LOG_TAG, Constants.CardapioReceiver.ALARM_TO_NEXT_PERIOD);
        boolean alarmOff = (PendingIntent.getBroadcast(context, 0,
                new Intent(ConstantsActions.CHECK_CARDAPIO), PendingIntent.FLAG_NO_CREATE) == null);

        if (alarmOff) {
            Intent intent = new Intent(ConstantsActions.CHECK_CARDAPIO);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0);
            Calendar calendar = Calendar.getInstance();

            if (calendar.get(Calendar.HOUR_OF_DAY) < Constants.LUNCH_BEGINNING) {
                calendar.set(Calendar.HOUR_OF_DAY, Constants.LUNCH_BEGINNING);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);
                Log.d(Constants.CardapioReceiver.LOG_TAG, Constants.CardapioReceiver.BEFORE_LUNCH);
            } else if (calendar.get(Calendar.HOUR_OF_DAY) >= Constants.DINNER_BEGINNING) {
                calendar.add(Calendar.DAY_OF_MONTH, 1);
                calendar.set(Calendar.HOUR_OF_DAY, Constants.LUNCH_BEGINNING);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);
                Log.d(Constants.CardapioReceiver.LOG_TAG,
                        Constants.CardapioReceiver.AFTER_BEGINNING_DINNER);
            } else if ((calendar.get(Calendar.HOUR_OF_DAY) >= Constants.LUNCH_BEGINNING) &&
                    (calendar.get(Calendar.HOUR_OF_DAY) < Constants.DINNER_BEGINNING)) {
                calendar.set(Calendar.HOUR_OF_DAY, Constants.DINNER_BEGINNING);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);
                Log.d(Constants.CardapioReceiver.LOG_TAG,
                        Constants.CardapioReceiver.BETWEEN_BEGINNING_LUNCH_BEGINNING_DINNER);
            }

            AlarmManager alarmManager =
                    (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
        }
    }

    /**
     * Define o alarm para a action {@link ConstantsActions#CHECK_CARDAPIO}
     * só chamando no outro dia.
     */
    private void setAlarmToNextDay(Context context) {
        Log.d(Constants.CardapioReceiver.LOG_TAG, Constants.CardapioReceiver.ALARM_TO_NEXT_DAY);
        boolean alarmOff = (PendingIntent.getBroadcast(context, 0,
                new Intent(ConstantsActions.CHECK_CARDAPIO), PendingIntent.FLAG_NO_CREATE) == null);

        if (alarmOff) {
            Intent intent = new Intent(ConstantsActions.CHECK_CARDAPIO);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0);
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DAY_OF_MONTH, 1);
            calendar.set(Calendar.HOUR_OF_DAY, Constants.LUNCH_BEGINNING);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);

            AlarmManager alarmManager =
                    (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
        }
    }

    /**
     * Define o alarm para a action {@link ConstantsActions#CHECK_CARDAPIO}
     * chamando daqui meia hora.
     */
    private void setFailedAlarm(Context context) {
        Log.d(Constants.CardapioReceiver.LOG_TAG, Constants.CardapioReceiver.SOMETHING_FAILED);
        boolean alarmOff = (PendingIntent.getBroadcast(context, 0,
                new Intent(ConstantsActions.CHECK_CARDAPIO), PendingIntent.FLAG_NO_CREATE) == null);

        if (alarmOff) {
            Intent intent = new Intent(ConstantsActions.CHECK_CARDAPIO);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0);
            Calendar calendar = Calendar.getInstance();

//            Se passou do almoço, alarm para o começo da janta, se passou da janta, alarm para
//            o almoço de amanhã, se está no almoço ou na janta, alarm para daqui meia hora
            if (calendar.get(Calendar.HOUR_OF_DAY) >= Constants.DINNER_ENDING) {
                calendar.add(Calendar.DAY_OF_MONTH, 1);
                calendar.set(Calendar.HOUR_OF_DAY, Constants.LUNCH_BEGINNING);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);
            } else if (calendar.get(Calendar.HOUR_OF_DAY) >= Constants.LUNCH_ENDING &&
                    calendar.get(Calendar.HOUR_OF_DAY) <= Constants.DINNER_BEGINNING) {
                calendar.set(Calendar.HOUR_OF_DAY, Constants.DINNER_BEGINNING);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);
            } else calendar.add(Calendar.MINUTE, 30);

            AlarmManager alarmManager =
                    (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
        }
    }

    /**
     * Cancela o alarm para poder setar outros em outro horário
     */
    private void cancelAlarm(Context context) {
        PendingIntent.getBroadcast(context, 0, new Intent(ConstantsActions.CHECK_CARDAPIO),
                PendingIntent.FLAG_UPDATE_CURRENT).cancel();
    }

    /**
     * Chama a notificação do CardápioRu
     *
     * @throws IllegalStateException se for passado um ringtone null, não executa o play
     */
    private void notificationRu(Context context, Intent intent) {
        NotificationManager notificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);
//        Chama uma act nesse caso a MenuRestaurant quando há o clique na notificação
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent,
                PendingIntent.FLAG_CANCEL_CURRENT);
//        Builder para construção do título e ícone que serão vinculados à notificação
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
//        Construção das partes da notificação
        builder.setTicker(context.getResources().getString(R.string.notification_ru_ticker));
        builder.setContentTitle(context.getResources().getString(R.string.notification_ru_title));
        builder.setContentText(context.getResources().getString(R.string.notification_ru_text));
        builder.setSmallIcon(R.mipmap.ic_notification);
        builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(),
                R.drawable.icone_ufscar_planner));
        builder.setContentIntent(pendingIntent);
//        Constroe a notificação
        Notification notification = builder.build();
//        Faz vibrar ao receber a notificação
//        150ms parado, 300ms vibrando, 150ms parado, 600ms vibrando
        notification.vibrate = new long[]{150, 300, 150, 600};
        notification.flags = Notification.FLAG_AUTO_CANCEL;
        notificationManager.notify(R.mipmap.ic_notification, notification);

//        Trata o toque ao receber a notificação
        Uri sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Ringtone toque = RingtoneManager.getRingtone(context, sound);
        if(toque != null) {
            toque.play();
        } else {
            throw new IllegalStateException(Constants.CardapioReceiver.INVALID_RINGTONE);
        }
    }
}