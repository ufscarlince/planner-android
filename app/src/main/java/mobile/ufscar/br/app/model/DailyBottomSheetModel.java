package mobile.ufscar.br.app.model;
import mobile.ufscar.br.app.ui.DailyVisionTabFragment;

/**
 * Model usados para popular a {@link DailyVisionTabFragment#mLvBottomSheet}
 */

public class DailyBottomSheetModel {

    String mTitle;
    Integer mImage;

    public DailyBottomSheetModel(String title, Integer image) {
        this.mTitle = title;
        this.mImage = image;
    }

    public Integer getImage() {
        return mImage;
    }

    public void setImage(Integer mImage) {
        this.mImage = mImage;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String mTitle) {
        this.mTitle = mTitle;
    }
}