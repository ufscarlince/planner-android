package mobile.ufscar.br.app.logic;

import mobile.ufscar.br.app.model.LoginResponseModel;
import mobile.ufscar.br.app.model.UfscarResponseError;

/**
 * Callback a ser utilizado por {@link LoginService} no caso de sucesso ou falha
 * na obtenção do Token de Login
 */

public interface LoginListener {

    /**
     * Callback no caso de sucesso na autenticacao do usuário
     * Obtenção do Token de acesso.
     *
     * @param loginResponseModel model a ser salvo no Cache
     * @see LoginResponseModel
     * @see CacheManager#setLoginCache(LoginResponseModel)
     */
    void loginSuccessful(LoginResponseModel loginResponseModel);

    /**
     * Callback no caso de não tiver obtido sucesso na autenticação do usuário
     *
     * @param error Erro que ocorreu na requisição. Sendo tanto um erro retornado pelo servidor ou
     *              algum erro ocorrido internamente durante a requisição.
     * @see UfscarResponseError
     */
    void loginFailed(UfscarResponseError error);
}
