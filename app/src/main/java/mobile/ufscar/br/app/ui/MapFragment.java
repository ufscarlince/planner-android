package mobile.ufscar.br.app.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

import mobile.ufscar.br.app.R;
import mobile.ufscar.br.app.config.Constants;
import mobile.ufscar.br.app.logic.CacheLocation;
import mobile.ufscar.br.app.model.LocalModel;

import static mobile.ufscar.br.app.utils.Utility.checkConnection;
import static mobile.ufscar.br.app.utils.Utility.isMobileDataBlocked;

/**
 * Fragmento que contém o mapa. Usado após o clique do botão em {@link MapEnableFragment}.
 * Também usado em {@link MapActivity}.
 */

public class MapFragment extends Fragment implements OnMapReadyCallback {

    public static final String LOCATION_NAME_KEY = "Local";
    public static final String LOCATION_LATITUDE_KEY = "latitude";
    public static final String LOCATION_LONGITUDE_KEY = "longitude";
    public static final String MAP_FRAGMENT_NAME = "fragmentMap";
    public static final String CAMPUS_NAME_KEY = "campus";
    public static double CAMPUS_LONGITUDE;
    public static double CAMPUS_LATITUDE;

    private GoogleMap mMap;
    private AutoCompleteTextView mMapSearch;
    private List<LocalModel> mPlaces;
    private List<String> mPlacesNames;
    private static String mChosenPlace = "";
    private LinearLayout mllMap;
    private LinearLayout mllDragonMap;
    private Button mbtnDragonMap;
    private TextView mtvDragonMsg;

    private static final double DOUBLE_EXTRA_DEFAULT = 0.0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Intent intent = getActivity().getIntent();
        View rootView = inflater.inflate(R.layout.fragment_map, container, false);

        int campus = 2;

        if (intent != null)
            campus = intent.getIntExtra(CAMPUS_NAME_KEY, 2);

        switch (campus) {
            case Constants.SAO_CARLOS:
                mPlaces = CacheLocation.getInstance().getLocations(Constants.SAO_CARLOS);
                CAMPUS_LONGITUDE = Constants.MapFragment.UFSCAR_LONGITUDE;
                CAMPUS_LATITUDE = Constants.MapFragment.UFSCAR_LATITUDE;
                break;
            case Constants.SOROCABA:
                mPlaces = new ArrayList<>();
                CAMPUS_LONGITUDE = Constants.MapFragment.UFSCAR_SOROCABA_LONGITUDE;
                CAMPUS_LATITUDE = Constants.MapFragment.UFSCAR_SOROCABA_LATITUDE;
                break;
            case Constants.ARARAS:
                mPlaces = new ArrayList<>();
                CAMPUS_LONGITUDE = Constants.MapFragment.UFSCAR_ARARAS_LONGITUDE;
                CAMPUS_LATITUDE = Constants.MapFragment.UFSCAR_ARARAS_LATITUDE;
                break;
            case Constants.LAGOA_DO_SINO:
                mPlaces = new ArrayList<>();
                CAMPUS_LONGITUDE = Constants.MapFragment.UFSCAR_LAG_SINO_LONGITUDE;
                CAMPUS_LATITUDE = Constants.MapFragment.UFSCAR_LAG_SINO_LATITUDE;
                break;
            default:
                mPlaces = new ArrayList<>();
                CAMPUS_LONGITUDE = Constants.MapFragment.UFSCAR_LONGITUDE;
                CAMPUS_LATITUDE = Constants.MapFragment.UFSCAR_LATITUDE;
        }

        FragmentManager fm = getChildFragmentManager();
        SupportMapFragment mapFragment = (SupportMapFragment) fm.
                findFragmentByTag(MAP_FRAGMENT_NAME);

        if (mapFragment == null) {
            mapFragment = new SupportMapFragment();
            FragmentTransaction ft = fm.beginTransaction();
            ft.add(R.id.mapFragment, mapFragment, MAP_FRAGMENT_NAME);
            ft.commit();
            fm.executePendingTransactions();
        }

        mMapSearch = (AutoCompleteTextView) rootView.findViewById(R.id.actvMapSearch);
        mllMap = (LinearLayout) rootView.findViewById(R.id.llMap);
        mllDragonMap = (LinearLayout) rootView.findViewById(R.id.llDragonMap);
        mbtnDragonMap = (Button) rootView.findViewById(R.id.btnDragonMap);
        mtvDragonMsg = (TextView) rootView.findViewById(R.id.tvDragonMapPhrase);

        mPlacesNames = CacheLocation.getNameLocations(mPlaces);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_dropdown_item_1line, mPlacesNames);
        mMapSearch.setAdapter(adapter);

        mMapSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                moveAndMarkPlace(mMap,
                        mPlaces.get(mPlacesNames.indexOf(parent.getItemAtPosition(position))).
                                getLatitude(),
                        mPlaces.get(mPlacesNames.indexOf(parent.getItemAtPosition(position)))
                                .getLongitude(),
                        mPlaces.get(mPlacesNames.indexOf(parent.getItemAtPosition(position)))
                                .getName());

                if (getActivity().getCurrentFocus() != null) {
                    InputMethodManager imm = (InputMethodManager) getActivity()
                            .getSystemService(getContext().INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getActivity().getCurrentFocus()
                            .getWindowToken(), 0);
                }

                if (mMapSearch.getText().toString().length() != 0)
                    mChosenPlace = mPlaces.get(mPlacesNames
                            .indexOf(parent.getItemAtPosition(position))).getName();
            }
        });

        mapFragment.getMapAsync(this);

        mbtnDragonMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkConnection(getContext()) && !isMobileDataBlocked(getContext())) {
                    mllDragonMap.setVisibility(View.GONE);
                    mllMap.setVisibility(View.VISIBLE);
                }
            }
        });

        if (!checkConnection(getContext()) || isMobileDataBlocked(getContext())) {
            mllMap.setVisibility(View.GONE);

            if(isMobileDataBlocked(getContext()))
                mtvDragonMsg.setText(R.string.data_usage_blocked_dragon_msg);
            else
                mtvDragonMsg.setText(R.string.no_connection_error_dragon_msg);

            mllDragonMap.setVisibility(View.VISIBLE);
        }

        if (intent != null && intent.getBooleanExtra(MapActivity.VISUALIZE_MAP, false)) {
            mllMap.setVisibility(View.GONE);
        }

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        Intent intent = getActivity().getIntent();

        if (!checkConnection(getContext()) || isMobileDataBlocked(getContext())) {
            mllMap.setVisibility(View.GONE);

            if(isMobileDataBlocked(getContext()))
                mtvDragonMsg.setText(R.string.data_usage_blocked_dragon_msg);
            else
                mtvDragonMsg.setText(R.string.no_connection_error_dragon_msg);

            mllDragonMap.setVisibility(View.VISIBLE);
        } else if (intent != null && intent.getBooleanExtra(MapActivity.VISUALIZE_MAP, false)) {
            mllMap.setVisibility(View.GONE);
        } else {
            mllMap.setVisibility(View.VISIBLE);
            mllDragonMap.setVisibility(View.GONE);
        }
    }

    private void moveAndMarkPlace(GoogleMap googleMap, double localLat, double localLong,
                                  String name) {
        googleMap.clear();
        LatLng local = new LatLng(localLat, localLong);
        int zoom = Constants.MapFragment.MAP_DEFAULT_ZOOM;
        if (local.latitude == CAMPUS_LATITUDE && local.longitude == CAMPUS_LONGITUDE) {
            zoom = Constants.MapFragment.MAP_UFSCAR_ZOOM;
        }
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(local, zoom));
        mMap.addMarker(new MarkerOptions()
                .title(name)
                .position(local));
    }

    public static String getAutoCompleteText() {
        return mChosenPlace;
    }

    public static void resetAutoCompleteText() {
        mChosenPlace = "";
    }

    /**
     * Inicia após o mapa ficar visível
     * recebe o mapa do tipo GoogleMap
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        LatLng ufscar = new LatLng(CAMPUS_LATITUDE, CAMPUS_LONGITUDE);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(ufscar,
                Constants.MapFragment.MAP_UFSCAR_ZOOM));

        if (getActivity().getIntent().hasExtra(LOCATION_NAME_KEY) &&
                getActivity().getIntent().hasExtra(LOCATION_LATITUDE_KEY)
                && getActivity().getIntent().hasExtra(LOCATION_LONGITUDE_KEY)) {
            if (!getActivity().getIntent().getStringExtra(LOCATION_NAME_KEY).equals("")) {
                moveAndMarkPlace(mMap, getActivity().getIntent()
                                .getDoubleExtra(LOCATION_LATITUDE_KEY, DOUBLE_EXTRA_DEFAULT),
                        getActivity().getIntent()
                                .getDoubleExtra(LOCATION_LONGITUDE_KEY, DOUBLE_EXTRA_DEFAULT),
                        getActivity().getIntent().getStringExtra(LOCATION_NAME_KEY));
            }
        }
    }
}