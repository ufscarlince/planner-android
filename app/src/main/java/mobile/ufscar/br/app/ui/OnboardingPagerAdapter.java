package mobile.ufscar.br.app.ui;

import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentPagerAdapter;

/**
 * Um {@link FragmentPagerAdapter} onde retorna a página específica e o
 * número de páginas
 *
 */

public class OnboardingPagerAdapter extends FragmentPagerAdapter {

    public OnboardingPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return OnboardingPlaceholderFragment.newInstance(position + 1);
    }

    @Override
    public int getCount(){
//        Total de 2 páginas no onboarding
        return 2;
    }
}