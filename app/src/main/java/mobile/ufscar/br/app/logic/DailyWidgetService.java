package mobile.ufscar.br.app.logic;

import android.content.Intent;
import android.widget.RemoteViewsService;

/**
 * Define o serviço que preenche a lista do Widget.
 */

public class DailyWidgetService extends RemoteViewsService {

    @Override
    public RemoteViewsFactory onGetViewFactory(Intent intent) {
        return (new DailyWidgetListProvider(this.getApplicationContext(), intent));
    }
}