package mobile.ufscar.br.app.model;

/**
 * Classe criada para guardar as opçoes do cardápio do Restaurante Universitário
 */
public class RestaurantMenuModel {

    private String mMainDish;
    private String mSideDish;
    private String mRiceDish;
    private String mBeanDish;
    private String mSaladDish;
    private String mDessertDish;
    private String mDrink;
    private boolean mLunch;
    private long mCurrentTime;

    /**
     * Busca o cardápio do dia atual no site da Universidade
     * Mostra esse na tela fragment_menu_restaurant
     *
     * @param mainDish    String que recebe o prato principal do Cardápio
     * @param sideDish    String que recebe a guarnição do Cardápio
     * @param riceDish    String que recebe os tipos de Arroz do Cardápio
     * @param beanDish    String que recebe os tipos de Feijão do Cardápio
     * @param saladDish   String que recebe as opçoes de Salada do Cardápio
     * @param dessertDish String que recebe as opçoes de Sobremesa do Cardápio
     * @param drink       String que recebe as opçoes de bebidas do Cardapio
     * @param lunch       true, se for almoço, caso contrário, false
     * @param time        número de milisegundos desde 01/01/1970 00:00:00.0 (Tempo atual)
     */
    public RestaurantMenuModel(String mainDish, String sideDish, String riceDish,
                               String beanDish, String saladDish, String dessertDish,
                               String drink, boolean lunch, long time) {
        this.mMainDish = mainDish;
        this.mSideDish = sideDish;
        this.mRiceDish = riceDish;
        this.mBeanDish = beanDish;
        this.mSaladDish = saladDish;
        this.mDessertDish = dessertDish;
        this.mDrink = drink;
        this.mLunch = lunch;
        this.mCurrentTime = time;
    }

    public String getMainDish() {
        return mMainDish;
    }

    public void setMainDish(String mainDish) {
        this.mMainDish = mainDish;
    }

    public String getSideDish() {
        return mSideDish;
    }

    public void setSideDish(String sideDish) {
        this.mSideDish = sideDish;
    }

    public String getRiceDish() {
        return mRiceDish;
    }

    public void setRiceDish(String riceDish) {
        this.mRiceDish = riceDish;
    }

    public String getBeanDish() {
        return mBeanDish;
    }

    public void setBeanDish(String beanDish) {
        this.mBeanDish = beanDish;
    }

    public String getSaladDish() {
        return mSaladDish;
    }

    public void setSaladDish(String saladDish) {
        this.mSaladDish = saladDish;
    }

    public String getDessertDish() {
        return mDessertDish;
    }

    public void setDessertDish(String dessertDish) {
        this.mDessertDish = dessertDish;
    }

    public String getDrink() {
        return mDrink;
    }

    public void setDrink(String drink) {
        this.mDrink = drink;
    }

    public boolean isLunch() {
        return mLunch;
    }

    public void setLunch(boolean lunch) {
        this.mLunch = lunch;
    }

    public long getCurrentTime() {
        return mCurrentTime;
    }

    public void setCurrentTime(long mCurrentTime) {
        this.mCurrentTime = mCurrentTime;
    }

//    Não verifica o atributo de tempo presente no objeto, somente os itens de cardápio
    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (o == this) return true;
        if (o.getClass() != getClass()) return false;
        RestaurantMenuModel rhs = (RestaurantMenuModel) o;
        return mMainDish.equals(rhs.mMainDish) && mSideDish.equals(rhs.mSideDish) &&
                mRiceDish.equals(rhs.mRiceDish) && mBeanDish.equals(rhs.mBeanDish) &&
                mSaladDish.equals(rhs.mSaladDish) && mDessertDish.equals(rhs.mDessertDish) &&
                mDrink.equals(rhs.mDrink) && mLunch == rhs.mLunch;
    }
}
