package mobile.ufscar.br.app.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import mobile.ufscar.br.app.R;
import mobile.ufscar.br.app.logic.CacheManager;
import mobile.ufscar.br.app.utils.Utility;

/**
 * Fragment anterior ao mapa, caso o usuário queira que o mapa seja mostrado ele deverá ativar o
 * mapa, ao contrário irá aparecer o dragão
 *
 * @see MapFragment
 * @see CacheManager#setEnableMap(boolean)
 * @see CacheManager#setFirstMap(boolean)
 * @see mobile.ufscar.br.app.R.layout#fragment_map_enable
 * @see MainActivity#alertMapShow()
 */

public class MapEnableFragment extends Fragment {

    private Button mBtnMapLaunch;
    private LinearLayout mLinearLayout;
    private TextView mTextView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_map_enable, container, false);
        mLinearLayout = (LinearLayout) view.findViewById(R.id.rlDragonMap);
        mLinearLayout.setVisibility(View.GONE);
        mBtnMapLaunch = (Button) view.findViewById(R.id.btnMapDragon);
        mTextView = (TextView) view.findViewById(R.id.tvDragonMap);

        if (CacheManager.getInstance().isEnableMap()) {
//            Acessando o mapa
            ((BaseContainerFragment) getParentFragment()).
                    replaceFragment(new MapFragment(), false);

        } else {

            mLinearLayout.setVisibility(View.VISIBLE);
            mTextView.setText(R.string.map_dragon_mensagem_enable);
            mBtnMapLaunch.setText(R.string.map_dragon_buttom_text_enable);
            mBtnMapLaunch.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Utility.checkConnection(getContext()) &&
                            !Utility.isMobileDataBlocked(getContext())) {
//                      Serviço de mapa é ativado
                        CacheManager.getInstance().setEnableMap(true);

//                      Acessando o mapa
                        ((BaseContainerFragment) getParentFragment()).
                                replaceFragment(new MapFragment(), false);
                    } else {
                        if(Utility.isMobileDataBlocked(getContext())) {
                            mTextView.setText(R.string.data_usage_blocked_dragon_msg);
                            mBtnMapLaunch.setText(R.string.try_again);
                        } else {
                            mTextView.setText(R.string.no_connection_error_dragon_msg);
                            mBtnMapLaunch.setText(R.string.try_again);
                        }
                    }
                }
            });
        }
        return view;
    }
}