package mobile.ufscar.br.app.logic;

import java.util.TimeZone;

import mobile.ufscar.br.app.config.ConstantsActions;

/**
 * Interface da requisição para buscar o cardápio do site do restaurante universitário da
 * Ufscar - Campus São Carlos
 */

public interface MenuRestaurantService {

    /**
     * Busca o cardápio do dia atual no site da Universidade Campi Sao Carlos
     * Atualiza os objetos de cardapio no {@link CacheManager} em caso de sucesso
     *
     * @param listener          Listener a ser chamado em caso de sucesso, por ser null, sendo
     *                          null será enviado um intent com action
     *                          {@link ConstantsActions#MENU_NOTIFICATION_INTENT} caso dê
     *                          certo, caso contrário será enviado um intent com action
     *                          {@link ConstantsActions#MENU_FAILED_INTENT}, ou com
     *                          {@link ConstantsActions#MENU_IS_SUNDAY_INTENT} ou com
     *                          {@link ConstantsActions#MENU_IS_UP_TO_DATE_INTENT} dependendo
     *                          da situação
     * @param currentTimeMillis Hora atual
     * @param zone              A zona do horário passado
     */
    void getMenu(MenuRestaurantListener listener, long currentTimeMillis, TimeZone zone);
}
