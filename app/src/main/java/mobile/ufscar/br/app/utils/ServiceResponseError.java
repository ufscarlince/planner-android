package mobile.ufscar.br.app.utils;

import android.content.Context;
import android.support.annotation.Nullable;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;

import mobile.ufscar.br.app.R;
import mobile.ufscar.br.app.config.Constants;
import mobile.ufscar.br.app.logic.UfscarApplication;
import mobile.ufscar.br.app.model.UfscarResponseError;
import mobile.ufscar.br.app.model.UfscarServerError;

/**
 * Classe que mapeia erros de requisições para {@link UfscarResponseError}
 */

public class ServiceResponseError {

    /**
     * Constrói um {@link UfscarResponseError} com base no {@link VolleyError} passado por
     * parâmetro, se houver conexão com o servidor, o código de erro será passado para o código
     * do ResponseError retornado e qualquer informação adicional do servidor será colocado na
     * lista de {@link UfscarServerError} de ResponseError, caso não acha conexão, o código de
     * ResponseError será {@link HttpURLConnection#HTTP_UNAVAILABLE} com a lista de ServerError
     * contendo o mesmo código e a mensagem contida em strings.xml
     *
     * @param error Erro proveniente da Volley
     * @return Erro da requisição transformado, pode ser null se o servidor responder com
     * {@link HttpURLConnection#HTTP_CREATED} ou {@link HttpURLConnection#HTTP_OK}
     */
    @Nullable
    public static UfscarResponseError parseError(VolleyError error) {
        Context context = getContext();

        if (error != null) {
            UfscarResponseError responseError = new UfscarResponseError();
            List<UfscarServerError> errorList = new ArrayList<>();
            int headerCode;
            UfscarServerError serverError = new UfscarServerError();
            if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                if (Constants.UfscarError.FAILED_AUTHENTICATION_RESPONSE.
                        equals(error.getMessage())) {
                    headerCode = HttpURLConnection.HTTP_UNAUTHORIZED;
                    serverError.setCode(Constants.UfscarError.AUTH_INVALID_TOKEN);
                    serverError.setMessage(context.getResources().
                            getString(R.string.invalid_token_error));
                    errorList.add(serverError);
                } else {
                    headerCode = HttpURLConnection.HTTP_UNAVAILABLE;
                    serverError.setCode(HttpURLConnection.HTTP_UNAVAILABLE);
                    serverError.setMessage(context.getResources().
                            getString(R.string.no_connection_error));
                    errorList.add(serverError);
                }
            } else if (error instanceof AuthFailureError){
                    headerCode = HttpURLConnection.HTTP_UNAUTHORIZED;
                    serverError.setCode(Constants.UfscarError.AUTH_INVALID_TOKEN);
                    serverError.setMessage(context.getResources().
                            getString(R.string.invalid_token_error));
                    errorList.add(serverError);
            } else if (error instanceof ServerError){
                headerCode = HttpURLConnection.HTTP_NOT_FOUND;
                serverError.setCode(Constants.UfscarError.PAGE_NOT_FOUND);
                serverError.setMessage(context.getResources().
                        getString(R.string.internal_server_error));
                errorList.add(serverError);
            } else if (error.networkResponse != null) {
                headerCode = error.networkResponse.statusCode;
                if (headerCode == HttpURLConnection.HTTP_CREATED ||
                        headerCode == HttpURLConnection.HTTP_OK) {
                    return null;
                } else {
                    try {
                        JSONObject responseErrorObject = new JSONObject(
                                new String(error.networkResponse.data,
                                        Constants.UfscarError.CHAR_SET_UTF8));

                        JSONArray errorArray = responseErrorObject.optJSONArray(
                                Constants.UfscarError.JSON_ERRORS_KEY);
                        if (errorArray != null) {
                            for (int i = 0; i < errorArray.length(); i++) {
                                if (errorArray.get(i) != null &&
                                        errorArray.get(i) instanceof JSONObject) {
                                    JSONObject singleError = (JSONObject) errorArray.get(i);
                                    serverError = new UfscarServerError(singleError);
                                    errorList.add(serverError);
                                }
                            }
                        } else {
                            JSONObject errorObject = responseErrorObject.
                                    optJSONObject(Constants.UfscarError.JSON_ERRORS_KEY);
                            if (errorObject != null) {
                                serverError = new UfscarServerError(errorObject);
                                errorList.add(serverError);
                            }
                        }
                    } catch (Exception e) {
                        serverError.setCode(Constants.UfscarError.
                                GENERIC_FAIL_PARSING_JSON);
                        serverError.setMessage("");
                        errorList.clear();
                        errorList.add(serverError);
                    }
                }
            } else {
                return getGenericError(Constants.UfscarError.DEFAULT_HEADER_CODE);
            }

            responseError.setStatusCode(headerCode);
            responseError.setServerErrorList(errorList);
            return responseError;
        } else {
            return getGenericError(Constants.UfscarError.DEFAULT_HEADER_CODE);
        }
    }

    private static Context getContext() {
        return UfscarApplication.getInstance();
    }

    /**
     * Cria uma {@link UfscarResponseError} para um erro genérico, sempre com o código
     * {@link HttpURLConnection#HTTP_INTERNAL_ERROR} e contendo uma lista, ou vazia, ou com
     * um {@link UfscarServerError} contendo o código passado no parâmetro e uma mensagem de
     * erro genérico vindo de strings.xml
     *
     * @param code Código do erro genérico
     * @return Uma resposta para um erro genérico
     * @see Constants.UfscarError#DEFAULT_HEADER_CODE
     */
    private static UfscarResponseError getGenericError(int code) {
        UfscarResponseError responseError = new UfscarResponseError();
        List<UfscarServerError> errorList = new ArrayList<>();
        UfscarServerError serverError = new UfscarServerError();

        responseError.setStatusCode(HttpURLConnection.HTTP_INTERNAL_ERROR);
        serverError.setCode(code);
        serverError.setMessage(getContext().getResources().getString(R.string.generic_error));
        errorList.add(serverError);

        responseError.setServerErrorList(errorList);
        return responseError;
    }
}