package mobile.ufscar.br.app.config;

import mobile.ufscar.br.app.logic.MenuRestaurantServiceImpl;

public class ConstantsActions {

    /**
     * Action usada quando se quer verificar o cardápio para poder mandar notificação
     *
     * @see mobile.ufscar.br.app.logic.receivers.CardapioReceiver
     */
    public static final String CHECK_CARDAPIO = "mobile.ufscar.br.app.CHECK_CARDAPIO";
    /**
     * Action usada no {@link MenuRestaurantServiceImpl#getMenu},
     * indica que os dados estão atualizados
     */
    public static final String MENU_IS_UP_TO_DATE_INTENT =
            "mobile.ufscar.br.app.MENU_IS_UP_TO_DATE";
    /**
     * Action usada no {@link MenuRestaurantServiceImpl#getMenu},
     * indica que é DOMINGO
     */
    public static final String MENU_IS_SUNDAY_INTENT = "mobile.ufscar.br.app.MENU_IS_SUNDAY";
    /**
     * Action usada no {@link MenuRestaurantServiceImpl#getMenu},
     * indica sucesso em pegar os dados
     */
    public static final String MENU_NOTIFICATION_INTENT = "mobile.ufscar.br.app.MENU_NOTIFICATION";
    /**
     * Action usada no {@link MenuRestaurantServiceImpl#getMenu},
     * indica falha em pegar os dados
     */
    public static final String MENU_FAILED_INTENT = "mobile.ufscar.br.app.MENU_FAILED";
    /**
     * Action usada no {@link MenuRestaurantServiceImpl#getMenu},
     * indica sucesso em pegar a hora, na intent da action tem um extra de chave
     * {@link #CURRENT_TIME} contendo o long com a hora atual
     */
    public static final String TIME_SUCCESS_INTENT = "mobile.ufscar.br.app.TIME_SUCCESS";
    /**
     * Action usada no {@link MenuRestaurantServiceImpl#getMenu},
     * indica falha em pegar a hora
     */
    public static final String TIME_FAILURE_INTENT = "mobile.ufscar.br.app.TIME_FAILURE";
    /**
     * Chave do long contando a hora atual, mandado na intent com a action
     * {@link #TIME_SUCCESS_INTENT}
     */
    public static final String CURRENT_TIME = "current_time";
}
