package mobile.ufscar.br.app.model;

/**
 * Contém as preferências do usuário
 */

public class ConfigurationModel {

    boolean mNotification;
    boolean mDataUsage;

    public ConfigurationModel() {
    }

    public ConfigurationModel(boolean notification, boolean dataUsage) {
        this.mNotification = notification;
        this.mDataUsage = dataUsage;
    }

    public boolean isNotification() {
        return mNotification;
    }

    public boolean isDataUsageBlocked() {
        return mDataUsage;
    }

    public void setNotification(boolean notification) {
        this.mNotification = notification;
    }

    public void setDataUsageBlocked(boolean dataUsage) {
        this.mDataUsage = dataUsage;
    }

    @Override
    public String toString() {
        return "ConfigurationModel{" +
                "notification=" + mNotification + "," +
                "dataUsage=" + mDataUsage +
                '}';
    }
}
