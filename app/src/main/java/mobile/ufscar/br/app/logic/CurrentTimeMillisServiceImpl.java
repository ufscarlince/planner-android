package mobile.ufscar.br.app.logic;

import android.content.Context;
import android.content.Intent;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import mobile.ufscar.br.app.config.Constants;
import mobile.ufscar.br.app.config.ConstantsActions;
import mobile.ufscar.br.app.utils.ServiceResponseError;

/**
 * Busca o horário atual da API {@link <a href=>https://sistemas.ufscar.br/sagui-api/time</a>}
 */

public class CurrentTimeMillisServiceImpl implements CurrentTimeMillisService {

    /**
     * Busca o horário atual da API, que retorna o horário em um {@link SimpleDateFormat} e a partir de GMT,
     * o horário é transformado em milisegundos aqui.
     *
     * @param listener Listener que ouvirá caso a busca dê certo ou não, pode ser null, nesse caso
     *                 será enviados broadcasts junto de actions
     * @see ConstantsActions#TIME_SUCCESS_INTENT
     * @see ConstantsActions#TIME_FAILURE_INTENT
     * @see mobile.ufscar.br.app.config.Constants.Urls#CURRENT_TIME
     */
    @Override
    public void getCurrentTime(final CurrentTimeServiceListener listener) {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET,
                Constants.Urls.CURRENT_TIME,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        String responseDate;
                        try {
                            responseDate = response.getString(Constants.TimeRequest.JSON_TIME_KEY);

                            SimpleDateFormat simpleDateFormat = new SimpleDateFormat
                                    (Constants.TimeRequest.RECEIVED_TIME_FORMAT);
                            simpleDateFormat.setTimeZone(TimeZone
                                    .getTimeZone(Constants.TimeRequest.TIMEZONE_ID));
                            Date date;
                            date = simpleDateFormat.parse(responseDate);
                            long currentTime = date.getTime()
                                    - Constants.TimeRequest.TIMEZONE_ADJUST;
                            if (listener == null) {
                                Intent intent = new Intent(ConstantsActions.TIME_SUCCESS_INTENT);
                                intent.putExtra(ConstantsActions.CURRENT_TIME, currentTime);
                                getContext().sendBroadcast(intent);
                            } else listener.onGetTimeSuccess(currentTime);
                        }
                        catch (JSONException | ParseException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (listener == null) {
                            getContext().sendBroadcast(new Intent(
                                    ConstantsActions.TIME_FAILURE_INTENT));
                        } else {
                            listener.onGetTimeFailure(ServiceResponseError.parseError(error));
                        }
                    }
                }
        );
        VolleyManager.getInstance().addToRequestQueue(jsonObjectRequest);
    }

    private static Context getContext() {return UfscarApplication.getInstance();}
}