package mobile.ufscar.br.app.logic;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.widget.RemoteViews;

import java.util.Calendar;
import java.util.Locale;

import mobile.ufscar.br.app.R;
import mobile.ufscar.br.app.config.Constants;
import mobile.ufscar.br.app.ui.MainActivity;

/**
 * Classe que cria um {@link AppWidgetProvider} para o DailyWidget, usada para dar funções
 * aos botãos e atualizar o Widget
 */

public class DailyWidgetProvider extends AppWidgetProvider {

    /**
     * Metodo que atualiza o Widget, chamado através do método Utility.updateWidget
     *
     * @param context do app
     * @param appWidgetManager Instancia do appWidgetManager
     * @param appWidgetIds com os ids dos widgets desse app
     */

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager,
                         int[] appWidgetIds) {
        for (int i = 0; i < appWidgetIds.length; ++i) {
//            Cria o servico de Remote Views (DailyWidgetService) para servir
//            de adapter da list view
            Intent svcIntent = new Intent(context, DailyWidgetService.class);
//            Passa o id do widget para o remote service
            svcIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetIds[i]);
            svcIntent.setData(Uri.parse(svcIntent.toUri(Intent.URI_INTENT_SCHEME)));

//            Seleciona o layout do Widget
            RemoteViews remoteViews = new RemoteViews(context.getPackageName(),
                    R.layout.daily_widget);
//            Seleciona o Adapter Remoto para essa list view
            remoteViews.setRemoteAdapter(appWidgetIds[i], R.id.listViewWidget,
                    svcIntent);
//            Seleciona uma view vazia caso nao tenha nenhum dado disponivel
            remoteViews.setEmptyView(R.id.listViewWidget, R.id.widget_empty_view);

//            Coloca a data no titulo do Widget
            String titleText = UfscarApplication.getInstance().
                    getApplicationContext().getString(R.string.app_name);
            Calendar sCalendar = Calendar.getInstance();
            Locale locale = new Locale("pt","BR");
            String dayLongName = sCalendar.getDisplayName(
                    Calendar.DAY_OF_WEEK,
                    Calendar.LONG,
                    locale);
            dayLongName = dayLongName.substring(0,1)
                    .toUpperCase().concat(dayLongName.substring(1));
            dayLongName = dayLongName.concat(", " +
                    Integer.toString(sCalendar.get(Calendar.DAY_OF_MONTH)) + " ");
            String monthName = sCalendar.getDisplayName(
                    Calendar.MONTH,
                    Calendar.LONG,
                    locale);
            monthName = monthName.substring(0,1)
                    .toUpperCase().concat(monthName.substring(1));
            remoteViews.setTextViewText(R.id.tvWidgetTitle, titleText
                    + " - " + dayLongName + monthName);

            Intent launchIntent = new Intent(context, MainActivity.class);
            launchIntent.putExtra(Constants.DailyWidget.WIDGET_INTENT,
                    Constants.MainPagerAdapter.DAILY_VISION);

            PendingIntent configPendingIntent = PendingIntent.getActivity(context, 0,
                    launchIntent, 0);
            remoteViews.setOnClickPendingIntent(R.id.btWidgetAdd, configPendingIntent);

//            Cria uma notificacao ao servico do Widget para que a lista
//            seja criada novamente quando os dados forem alterados.
            appWidgetManager.notifyAppWidgetViewDataChanged(appWidgetIds, R.id.listViewWidget);
            appWidgetManager.updateAppWidget(appWidgetIds[i], remoteViews);

        }
        super.onUpdate(context, appWidgetManager, appWidgetIds);

    }
}