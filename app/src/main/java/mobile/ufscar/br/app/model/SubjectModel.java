package mobile.ufscar.br.app.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Classe suporte para fazer o parsing do JSON vindo da API do SIGA
 */

public class SubjectModel {

    @SerializedName("id")
    long mId;
    @SerializedName("atividade")
    String mNome;
    @SerializedName("turma")
    String mTurma;
    @SerializedName("periodo")
    int mPeriodo;
    @SerializedName("ano")
    int mAno;
    @SerializedName("horarios")
    List<HorarioModel> mHorarios;

    public long getId() {
        return mId;
    }

    public void setId(long mId) {
        this.mId = mId;
    }

    public String getNome() {
        return mNome;
    }

    public void setNome(String mNome) {
        this.mNome = mNome;
    }

    public String getTurma() {
        return mTurma;
    }

    public void setTurma(String mTurma) {
        this.mTurma = mTurma;
    }

    public int getPeriodo() {
        return mPeriodo;
    }

    public void setPeriodo(int mPeriodo) {
        this.mPeriodo = mPeriodo;
    }

    public int getAno() {
        return mAno;
    }

    public void setAno(int mAno) {
        this.mAno = mAno;
    }

    public List<HorarioModel> getHorarios() {
        return mHorarios;
    }

    public void setHorarios(List<HorarioModel> mHorarios) {
        this.mHorarios = mHorarios;
    }

    public class HorarioModel {
        @SerializedName("dia")
        String mDia;
        @SerializedName("inicio")
        String mInicio;
        @SerializedName("fim")
        String mFim;
        @SerializedName("sala")
        String mSala;

        public String getDia() {
            return mDia;
        }

        public void setDia(String dia) {
            this.mDia = dia;
        }

        public String getInicio() {
            return mInicio;
        }

        public void setInicio(String inicio) {
            this.mInicio = inicio;
        }

        public String getFim() {
            return mFim;
        }

        public void setFim(String fim) {
            this.mFim = fim;
        }

        public String getSala() {
            return mSala;
        }

        public void setSala(String sala) {
            this.mSala = sala;
        }
    }
}
