package mobile.ufscar.br.app.logic;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import mobile.ufscar.br.app.config.Constants;
import mobile.ufscar.br.app.model.LoginResponseModel;
import mobile.ufscar.br.app.utils.ServiceResponseError;
import mobile.ufscar.br.app.utils.Utility;

/**
 * Classe responsável por implementar o método login
 * {@link LoginService#getLoginToken(LoginListener, JSONObject)}
 * Realizando assim a requisição no servidor, utilizando Volley
 */

public class LoginServiceImpl implements LoginService {

    /**
     * Método utilizado para realizar a autenticação do usuário
     *
     * @param loginListener Contendo os métodos a serem implementados em caso de sucesso
     *                      ou falha
     * @param loginBody Corpo da requisição feita ao servidor
     *                  Formato: {"password": "RA/CPF", "username": "SENHA"}
     * @see LoginListener
     */
    @Override
    public void getLoginToken(final LoginListener loginListener, final JSONObject loginBody) {

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.POST,
                Constants.Urls.LOGIN,
                loginBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        loginListener.loginSuccessful
                                ((LoginResponseModel)Utility.parseJsonToObject(
                                        response,
                                        LoginResponseModel.class));
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        loginListener.loginFailed(ServiceResponseError.parseError(error));
                    }
                }
        );
        VolleyManager.getInstance().addToRequestQueue(jsonObjectRequest);
    }
}