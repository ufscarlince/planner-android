package mobile.ufscar.br.app.ui;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

import mobile.ufscar.br.app.logic.CacheManager;

/**
 * Adapter para as tabs criadas em {@link MainActivity}
 */

public class MainPagerAdapter extends FragmentPagerAdapter {

    private final List<Fragment> mFragmentList = new ArrayList<>();
    private DailyVisionFragment mDailyVisionFragment;

    /**
     * Contrutor da classe, coloca todas as Tabs na Lista
     * A sequência das abas é intrínsecamente relacionada às constantes em
     * {@link mobile.ufscar.br.app.config.Constants.MainPagerAdapter}
     *
     * Verifica se o mapa está ativado, caso esteja o fragmento da map já é instanceado
     * diretamente e caso não esteja é instanceado o controlador de acesso ao mapa
     *
     * @see #mFragmentList
     * @see CacheManager#isEnableMap()
     * @see MapEnableFragment
     * @see MapActivity
     */
    public MainPagerAdapter(FragmentManager fm) {
        super(fm);
        addFragment(mDailyVisionFragment = new DailyVisionFragment());
        addFragment(new MenuRestaurantFragment());
        addFragment(new RadioFragment());

//        Verificando se o serviço do mapa está ou não ativado
        if (CacheManager.getInstance().isEnableMap()) {
            addFragment(new MapFragment());
        } else {
            addFragment(new MapContainerFragment());
        }

    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    /**
     * Método que adiciona fragmentos na lista de Fragmentos
     */
    private void addFragment(Fragment fragment) {
        mFragmentList.add(fragment);
    }

    /**
     * Método que coloca a tab no dia atual na tab {@link DailyVisionFragment}
     * usado quando o App é lançado através do Widget
     */
    public void setToCurrentDay(){
        mDailyVisionFragment.setTabSelection();
    }

    @Override
    public CharSequence getPageTitle(int position) {
//        Return Null, pois as Tabs possuem somente ícones
        return null;
    }
}