package mobile.ufscar.br.app.ui;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import mobile.ufscar.br.app.model.DailyBottomSheetModel;

/**
 * Os objetos {@link DailyBottomSheetModel} são mapeados para
 * essa classe, visando assim a reutilização das {@link android.view.View} criadas
 * em {@link mobile.ufscar.br.app.ui.DailyBottomSheetAdapter}
 *
 * @see DailyBottomSheetAdapter#getView(int, View, ViewGroup)
 */

public class ViewHolderBottomSheetDaily {

    private TextView mTvBottomSheet;
    private ImageView mIvBottomSheet;

    public ViewHolderBottomSheetDaily(TextView tvBottomSheet, ImageView ivBottomSheet) {
        this.mTvBottomSheet = tvBottomSheet;
        this.mIvBottomSheet = ivBottomSheet;
    }

    public TextView getTvBottomSheet() {
        return mTvBottomSheet;
    }

    public void setTvBottomSheet(TextView mTvBottomSheet) {
        this.mTvBottomSheet = mTvBottomSheet;
    }

    public ImageView getIvBottomSheet() {
        return mIvBottomSheet;
    }

    public void setmIvBottomSheet(ImageView mIvBottomSheet) {
        this.mIvBottomSheet = mIvBottomSheet;
    }
}