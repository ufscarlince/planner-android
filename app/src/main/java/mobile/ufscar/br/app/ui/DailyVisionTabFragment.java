package mobile.ufscar.br.app.ui;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import mobile.ufscar.br.app.R;
import mobile.ufscar.br.app.logic.CacheManager;
import mobile.ufscar.br.app.logic.Manager;
import mobile.ufscar.br.app.logic.UfscarApplication;
import mobile.ufscar.br.app.model.DailyBottomSheetModel;
import mobile.ufscar.br.app.model.EventModel;
import mobile.ufscar.br.app.utils.Utility;

import mobile.ufscar.br.app.config.Constants.DailyTabFragment;

/**
 * Um fragment que contém todos os eventos de um dia específico
 */

public class DailyVisionTabFragment extends Fragment {

    private static final String POSITION_KEY = "position";
    private static final String LOG_TAG = DailyVisionTabFragment.class.getName();
    private static final String NO_POSITION_LOG_MESSAGE =
            "Não foi atribuído posição para este fragment";
    private ListView mLvDaily;
    private int mDay;
    private ListView mLvBottomSheet;
    private BottomSheetDialog mBottomSheetDialog;
    private CoordinatorLayout mCoordinatorLayout;

    private Context getApplication() {
        return UfscarApplication.getInstance();
    }

    /**
     * Pega o Fragment que será usado numa página específica
     *
     * @return DailyVisionTabFragment da posição passada no parâmetro
     */
    public static DailyVisionTabFragment getInstance(int position) {
        DailyVisionTabFragment fragment = new DailyVisionTabFragment();
        Bundle args = new Bundle();
        args.putInt(POSITION_KEY, position);
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Método que seta a {@link #mLvBottomSheet} que é utilizada em {@link #mBottomSheetDialog}
     * As posição dos itens na ListView importa e é definida da seguinte maneira
     * 0 - {@link DailyTabFragment#MAP_EVENT}
     * 1 - {@link DailyTabFragment#EDIT_EVENT}
     * 2 - {@link DailyTabFragment#DELETE_EVENT}
     *
     * @see #bottomSheetDialog(int)
     */
    private void createLvBottomSheet() {
//        Criando o adapter com os items que compoe o BottomSheet
        List<DailyBottomSheetModel> dailyBottomSheetModelList = new ArrayList<>();
        dailyBottomSheetModelList.add(new DailyBottomSheetModel(
                getString(R.string.daily_vision_map_msg), R.drawable.location));
        dailyBottomSheetModelList.add(new DailyBottomSheetModel(
                getString(R.string.daily_vision_edit_msg), R.drawable.shape));
        dailyBottomSheetModelList.add(new DailyBottomSheetModel(
                getString(R.string.daily_vision_delete_msg), R.drawable.close));

        DailyBottomSheetAdapter dailyBottomSheetAdapter = new
                DailyBottomSheetAdapter(dailyBottomSheetModelList);
        mLvBottomSheet.setAdapter(dailyBottomSheetAdapter);
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_tab_daily_vision, container, false);

        mCoordinatorLayout = (CoordinatorLayout) layout.findViewById(R.id.clDailVision);

        mBottomSheetDialog = new BottomSheetDialog(getContext());

        View viewBottomSheet = inflater.inflate(R.layout.daily_bottom_sheet, container, false);
        mLvBottomSheet = (ListView) viewBottomSheet.findViewById(R.id.lvBottomSheet);
        mBottomSheetDialog.setContentView(viewBottomSheet);

        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey(POSITION_KEY)) {
            mDay = bundle.getInt(POSITION_KEY);

            mLvDaily = (ListView) layout.findViewById(R.id.lvDaily);
        } else {
            Log.w(LOG_TAG, NO_POSITION_LOG_MESSAGE);
        }

        //        Faz a chamada do BottomSheet
        mLvDaily.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                EventAdapter eventAdapter = (EventAdapter) mLvDaily.getAdapter();

                View viewBottomSheet = inflater.inflate(R.layout.daily_bottom_sheet, container,
                        false);
                mLvBottomSheet = (ListView) viewBottomSheet.findViewById(R.id.lvBottomSheet);
                mBottomSheetDialog.setContentView(viewBottomSheet);

                if (eventAdapter.getItem(position).isRealEvent()) {
                    createLvBottomSheet();
                    bottomSheetDialog(position);
                    mBottomSheetDialog.show();
                    return true;
                }
                return false;
            }
        });

        return layout;
    }

    @Override
    public void onPause() {
        super.onPause();
        mBottomSheetDialog.dismiss();
    }

    @Override
    public void onStart() {
        mLvDaily.setAdapter(getEventAdapterDaily(getContext(), mDay));

//        Faz a chamada do Activity passando como parâmetro um objeto do tipo EventModel
        mLvDaily.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                Caso o horario estaja vazio, cria um evento novo
                EventAdapter eventAdapter = (EventAdapter) mLvDaily.getAdapter();
                if (!eventAdapter.getItem(position).isRealEvent()) {
                    Intent intent = toEditVisionEmpty(getActivity(),
                            eventAdapter.getItem(position));
                    startActivity(intent);
                }
            }
        });
        super.onStart();
    }

    /**
     * Método que trata as opções presentes no {@link #mBottomSheetDialog}
     * A switch trabalha de acordo com as posiçoes da {@link #mLvBottomSheet}
     * definidas em {@link #createLvBottomSheet()}
     *
     * @param eventPosition posição do evento selecionado
     * @see #createLvBottomSheet()
     * @see DailyTabFragment#DELETE_EVENT
     * @see DailyTabFragment#EDIT_EVENT
     * @see DailyTabFragment#MAP_EVENT
     */
    private void bottomSheetDialog(final int eventPosition) {

        mLvBottomSheet.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                EventAdapter eventAdapter = (EventAdapter) mLvDaily.getAdapter();
                switch (position) {
                    case DailyTabFragment.DELETE_EVENT: {
                        mBottomSheetDialog.dismiss();
                        getConfirmDeleteAlert(eventPosition).show();
                        break;
                    }
                    case DailyTabFragment.EDIT_EVENT: {
                        int eventNumber = Manager.getEventPosition(
                                eventAdapter.getItem(eventPosition));
                        if (eventNumber != -1) {
                            Intent intent = toEditVisionEvent(getActivity(), eventNumber);
                            startActivity(intent);
                        }
                        break;
                    }
                    case DailyTabFragment.MAP_EVENT: {
                        Intent intent = new Intent(getContext(), MapActivity.class);
                        intent.putExtra(MapFragment.LOCATION_NAME_KEY,
                                eventAdapter.getItem(eventPosition).getLocal().getName());
                        intent.putExtra(MapFragment.LOCATION_LATITUDE_KEY,
                                eventAdapter.getItem(eventPosition).getLocal().getLatitude());
                        intent.putExtra(MapFragment.LOCATION_LONGITUDE_KEY,
                                eventAdapter.getItem(eventPosition).getLocal().getLongitude());
                        intent.putExtra(MapActivity.VISUALIZE_MAP, true);
                        startActivity(intent);
                        break;
                    }
                }
            }
        });
    }

    /**
     * PopUp perguntando ao usuário se deseja deletar o evento
     *
     * @return AlertDialog esperando confirmação do usuário
     */
    private AlertDialog getConfirmDeleteAlert(final int eventPosition) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.confirm_delete_alert);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                EventAdapter eventAdapter = (EventAdapter) mLvDaily.getAdapter();
                EventModel eventModel = eventAdapter.getItem(eventPosition);
                CacheManager.getInstance().removeEventCache(eventModel);
                Utility.updateWidget(getContext());
                mLvDaily.setAdapter(getEventAdapterDaily(getActivity(), mDay));
                Utility.showSnackbar(mCoordinatorLayout,
                        getString(R.string.daily_vision_event_delete_msg));
            }
        });
        builder.setNeutralButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        return builder.create();
    }

    /**
     * Pega um evento vazio e o embrulha para ser mandado para {@link EditVisionActivity}
     *
     * @return {@link Intent} para EditVisionActivity com os parâmetros que ela precisa
     */
    private Intent toEditVisionEmpty(Context context, EventModel eventModel) {
        Intent intent = new Intent(context, EditVisionActivity.class);
        intent.putExtra(EditVisionActivity.START, eventModel.getSchedule().getStartTime());
        intent.putExtra(EditVisionActivity.END, eventModel.getSchedule().getFinishTime());
        intent.putExtra(EditVisionActivity.DAY, eventModel.getSchedule().getWeekDay());
        return intent;
    }

    /**
     * Pega a posição do {@link EventModel} na lista de eventos armazenados no Cache e o embrulha
     * para ser mandado para {@link EditVisionActivity}
     *
     * @return {@link Intent} para EditVisionActivity com os parâmetros que ela precisa
     * @see CacheManager#getEventCache
     */
    private Intent toEditVisionEvent(Context context, int eventNumber) {
        Intent intent = new Intent(context, EditVisionActivity.class);
        intent.putExtra(EditVisionActivity.EVENT_NUMBER, eventNumber);
        return intent;
    }

    /**
     * Pega algum dia e pega o {@link EventAdapter} desse dia específico
     *
     * @return EventAdapter com os eventos reais e falsos para popular uma ListView
     */
    private EventAdapter getEventAdapterDaily(Context context, int day) {
        List<EventModel> itemsDia = Manager.getEventDaily(day);
        List<EventModel> items = Manager.getEventEmpty(itemsDia, day);
        return new EventAdapter(items, context);
    }
}