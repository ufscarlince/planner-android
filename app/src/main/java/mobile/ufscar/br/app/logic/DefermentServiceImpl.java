package mobile.ufscar.br.app.logic;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import mobile.ufscar.br.app.config.Constants;
import mobile.ufscar.br.app.model.EventModel;
import mobile.ufscar.br.app.model.LocalModel;
import mobile.ufscar.br.app.model.ScheduleModel;
import mobile.ufscar.br.app.model.SubjectModel;
import mobile.ufscar.br.app.utils.ServiceResponseError;
import mobile.ufscar.br.app.utils.Utility;

/**
 * Classe que implementa {@link DefermentService}, usada para buscar o deferimento da API
 * do SIGA
 */

public class DefermentServiceImpl implements DefermentService {
    
    /**
     * Método que efetivamente chama o serviço da Volley que busca o deferimento
     *
     * @param listener Listener do tipo EventServiceListener
     * @param headers Header da requisição, contendo o token do usuario
     *                (utilizar Utility.getAuthorizationHeader para gerar o header)
     * @param body corpo da requisição, deve ser null
     */
    @Override
    public void getDeferment(final EventServiceListener listener,
                             final Map<String, String> headers,
                             JSONObject body) {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET,
                Constants.Urls.DEFERMENT,
                body,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        listener.loadCompleted(subjectsToEvents(
                                parseJsonToSubjectList(response)));
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        Tratamento dos possíveis erros
                        listener.loadFailed(ServiceResponseError.parseError(error));
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headers;
            }
        };
        VolleyManager.getInstance().addToRequestQueue(jsonObjectRequest);
    }

    /**
     * Metodo responsavel por transformar o JSON de resposta da volley em uma lista de
     * {@link SubjectModel}
     * @param response JSON com a resposta do servidor.
     * @return lista de SubjectModel
     */
    private List<SubjectModel> parseJsonToSubjectList(JSONObject response){
//        Parseia o JSON em uma lista de subjects
        String finalJson = response.toString();
        JSONObject jsonObject;
        JSONArray subjectsJsonArray = null;
        try {
            jsonObject = new JSONObject(finalJson);
            subjectsJsonArray = jsonObject.getJSONArray(Constants
                    .DEFERMENT_JSON_PARSING_KEY);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        List<SubjectModel> subjectModelList = new ArrayList<>();
        Gson gson = new Gson();
        if (subjectsJsonArray != null) {
            for (int i = 0; i < subjectsJsonArray.length(); i++) {
                JSONObject subjectJson = null;
                try {
                    subjectJson = subjectsJsonArray.getJSONObject(i);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                SubjectModel subjectModel = null;
                if (subjectJson != null) {
                    subjectModel = gson.fromJson(subjectJson.toString(),
                            SubjectModel.class);
                }
                subjectModelList.add(subjectModel);
            }
        }
//                        Transforma os subjects em events e chama o metodo que
//                        atualiza a lista do cache
        return subjectModelList;
    }

    /**
     * Transforma uma lista de SubjectModel em uma lista de EventModel.
     *
     * @return List de EventModel com os valores de cada SubjectModel da lista.
     */
    private List<EventModel> subjectsToEvents(List<SubjectModel> subjectModels) {
        List<EventModel> eventModelList = new ArrayList<>();
        for (int i = 0; i < subjectModels.size(); i++) {
            SubjectModel subjectModel = subjectModels.get(i);

            List<ScheduleModel> scheduleModels = new ArrayList<>();
            for (int j = 0; j < subjectModel.getHorarios().size(); j++) {
                scheduleModels.add(new ScheduleModel(Utility.dayToInt(
                        subjectModel.getHorarios().get(j).getDia()),
                        TimeToInt(subjectModel.getHorarios().get(j).getInicio()),
                        TimeToInt(subjectModel.getHorarios().get(j).getFim()),
                        subjectModel.getHorarios().get(j).getSala()));
            }
            scheduleModels = sortSchedules(scheduleModels);
            scheduleModels = compatSchedules(scheduleModels);

            for (int j = 0; j < scheduleModels.size(); j++) {
                String name = Utility.formatName(subjectModel.getNome());
                if (scheduleModels.get(j).getSala() != null) {
                    String local = scheduleModels.get(j).getSala();
                    if (local.contains(Constants.LOCAL_AUX) &&
                            local.contains(Constants.COMPLEMENT_AUX)) {
                        double latitude;
                        double longitude;
                        String localName = local.substring(0,Constants.LOCAL_NAME_SIZE);
                        String complement = local.substring(
                                local.length() - Constants.COMPLEMENT_SIZE,local.length());
                        switch (localName) {
                            case "AT01":
                                latitude = Constants.AT01_LATITUDE;
                                longitude = Constants.AT01_LONGITUDE;
                                break;
                            case "AT02":
                                latitude = Constants.AT02_LATITUDE;
                                longitude = Constants.AT02_LONGITUDE;
                                break;
                            case "AT03":
                                latitude = Constants.AT03_LATITUDE;
                                longitude = Constants.AT03_LONGITUDE;
                                break;
                            case "AT04":
                                latitude = Constants.AT04_LATITUDE;
                                longitude = Constants.AT04_LONGITUDE;
                                break;
                            case "AT05":
                                latitude = Constants.AT05_LATITUDE;
                                longitude = Constants.AT05_LONGITUDE;
                                break;
                            case "AT06":
                                latitude = Constants.AT06_LATITUDE;
                                longitude = Constants.AT06_LONGITUDE;
                                break;
                            case "AT07":
                                latitude = Constants.AT07_LATITUDE;
                                longitude = Constants.AT07_LONGITUDE;
                                break;
                            case "AT08":
                                latitude = Constants.AT08_LATITUDE;
                                longitude = Constants.AT08_LONGITUDE;
                                break;
                            case "AT09":
                                latitude = Constants.AT09_LATITUDE;
                                longitude = Constants.AT09_LONGITUDE;
                                break;
                            case "AT10":
                                latitude = Constants.AT10_LATITUDE;
                                longitude = Constants.AT10_LONGITUDE;
                                break;
                            default:
                                latitude = Constants.MapFragment.UFSCAR_LATITUDE;
                                longitude = Constants.MapFragment.UFSCAR_LONGITUDE;
                                break;
                        }
                        EventModel eventModel = new EventModel(name, scheduleModels.get(j),
                                new LocalModel(localName, complement, Constants.SAO_CARLOS,
                                        latitude, longitude));
                        eventModelList.add(eventModel);
                    } else {
                        EventModel eventModel = new EventModel(name, scheduleModels.get(j),
                                new LocalModel(local, "", Constants.SAO_CARLOS,
                                        Constants.MapFragment.UFSCAR_LATITUDE,
                                        Constants.MapFragment.UFSCAR_LONGITUDE));
                        eventModelList.add(eventModel);
                    }
                } else {
                    EventModel eventModel = new EventModel(name, scheduleModels.get(j),
                            new LocalModel(subjectModel
                                    .getHorarios().get(j).getSala(), "", Constants.SAO_CARLOS,
                                    Constants.MapFragment.UFSCAR_LATITUDE,
                                    Constants.MapFragment.UFSCAR_LONGITUDE));
                    eventModelList.add(eventModel);
                }
            }
        }
        return eventModelList;
    }

    /**
     * Transforma uma String com um horário em um int
     *
     * @param time String com um horário (format "(X)X:XX:XX PM" ou "(X)X:XX:XX AM"/ X é um inteiro)
     * @return int com a hora do horário passado
     */
    private int TimeToInt(String time) {
        int divider = time.indexOf(":");
        if (divider != -1) {
            String hour;
            hour = String.copyValueOf(time.toCharArray(), 0, divider);
            return Integer.parseInt(hour);
        }
        return -1;
    }

    /**
     * Ordena os horarios em ordem crescende de dia da semana e hora.
     *
     * @param scheduleModels List não ordenada de Schedules
     * @return List com os Schedules ordenados
     */
    private List<ScheduleModel> sortSchedules(List<ScheduleModel> scheduleModels) {
        List<ScheduleModel> scheduleModelList = new ArrayList<>();
        int listSize = scheduleModels.size();
        for (int j=0; j < listSize; j++) {
            ScheduleModel scheduleAux = scheduleModels.get(0);
            int auxIndex = 0;
            for (int i = 1; i < scheduleModels.size(); i++) {
                if ((scheduleModels.get(i).getWeekDay()*24+scheduleModels.get(i).getStartTime()) <
                        scheduleAux.getWeekDay()*24+scheduleAux.getStartTime()) {
                    scheduleAux = scheduleModels.get(i);
                    auxIndex = i;
                }
            }
            scheduleModelList.add(scheduleAux);
            scheduleModels.remove(auxIndex);
        }
        return scheduleModelList;
    }

    /**
     * Pega uma lista de horários e junta os horários seguidos
     *
     * @param scheduleModels List ordenada de Schedules
     * @return List com os Schedules compactos
     */
    private List<ScheduleModel> compatSchedules(List<ScheduleModel> scheduleModels) {
        List<ScheduleModel> scheduleModelList = new ArrayList<>();

        if (!scheduleModels.isEmpty()) {
            ScheduleModel scheduleAux = scheduleModels.get(0);
            int i = 0;
            while (i + 1 < scheduleModels.size()) {
                i++;
                if (scheduleModels.get(i).getWeekDay() == scheduleAux.getWeekDay() &&
                        scheduleModels.get(i).getStartTime() == scheduleAux.getFinishTime()) {
                    scheduleAux.setFinishTime(scheduleModels.get(i).getFinishTime());
                } else {
                    scheduleModelList.add(scheduleAux);
                    scheduleAux = scheduleModels.get(i);
                }
            }
            scheduleModelList.add(scheduleAux);
        }
        return scheduleModelList;
    }
}