package mobile.ufscar.br.app.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.support.design.widget.BottomSheetBehavior;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import mobile.ufscar.br.app.R;
import mobile.ufscar.br.app.logic.UfscarApplication;
import mobile.ufscar.br.app.model.DailyBottomSheetModel;

/**
 * Adapter para popular o {@link BottomSheetBehavior} da {@link DailyVisionTabFragment}
 */

public class DailyBottomSheetAdapter extends BaseAdapter {

    private List<DailyBottomSheetModel> mDailyBottomSheetModelList;
    private LayoutInflater mLayoutInflater;

    public DailyBottomSheetAdapter(List<DailyBottomSheetModel> dailyBottomSheetModelList) {
        this.mDailyBottomSheetModelList = dailyBottomSheetModelList;
        this.mLayoutInflater = LayoutInflater.from(getApplication());
    }

    private Context getApplication() {
        return UfscarApplication.getInstance();
    }

    @Override
    public int getCount() {
        return mDailyBottomSheetModelList.size();
    }

    @Override
    public Object getItem(int position) {
        return mDailyBottomSheetModelList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolderBottomSheetDaily viewHolderBottomSheetDaily;
        DailyBottomSheetModel dailyBottomSheetModel = mDailyBottomSheetModelList.get(position);

        View view = convertView;
        if (view == null) {
            view = mLayoutInflater.inflate(R.layout.daily_bottom_sheet_cell, parent, false);

            TextView tvBottomSheet = (TextView) view.findViewById(R.id.tvBottomSheet);
            ImageView ivBottomSheet = (ImageView) view.findViewById(R.id.ivBottomSheet);

            viewHolderBottomSheetDaily = new ViewHolderBottomSheetDaily(tvBottomSheet,
                    ivBottomSheet);
            view.setTag(viewHolderBottomSheetDaily);
        } else {
            viewHolderBottomSheetDaily = (ViewHolderBottomSheetDaily) view.getTag();
        }

        viewHolderBottomSheetDaily.getTvBottomSheet().setText(
                dailyBottomSheetModel.getTitle());

        viewHolderBottomSheetDaily.getIvBottomSheet().setImageResource(
                dailyBottomSheetModel.getImage());

        return view;
    }
}