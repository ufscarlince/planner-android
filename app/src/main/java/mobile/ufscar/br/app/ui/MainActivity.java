package mobile.ufscar.br.app.ui;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.ArrayList;

import mobile.ufscar.br.app.R;
import mobile.ufscar.br.app.config.Constants;
import mobile.ufscar.br.app.logic.RadioService;
import mobile.ufscar.br.app.logic.CacheLocation;
import mobile.ufscar.br.app.logic.CacheManager;
import mobile.ufscar.br.app.model.EventModel;
import mobile.ufscar.br.app.utils.Utility;

/**
 * Classe que gera a tela inicial do aplicativo, com suas respectivas Tabs
 *
 * @see mobile.ufscar.br.app.R.layout#activity_main
 * @see mobile.ufscar.br.app.R.layout#content_main
 * @see MainPagerAdapter
 */

public class MainActivity extends AppCompatActivity {

    private TabLayout mTlMain;
    private ViewPager mVpMain;
    private Toolbar mToolbar;
    private int[] tabIcons = {
            R.drawable.ic_calendar_selected,
            R.drawable.ic_ru_selected,
            R.drawable.ic_radio_selected,
            R.drawable.ic_map_selected
    };
    private int[] tabIconsUnselected = {
            R.drawable.ic_calendar_unselected,
            R.drawable.ic_ru_unselected,
            R.drawable.ic_radio_unselected,
            R.drawable.ic_map_unselected
    };
    private final MainPagerAdapter mAdapter = new MainPagerAdapter(getSupportFragmentManager());

    private boolean mCloseApp = false;
    public static boolean mRadioStarting = false;
    private Handler mHandler = new Handler();

//    Após passado um tempo, retorna a variavel para o seu valor inicial
    private final Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            mCloseApp = false;
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mToolbar = (Toolbar) findViewById(R.id.tbMainActivity);
        setSupportActionBar(mToolbar);

//        Setando a view Pager com o adapter com todas as tabs
        mVpMain = (ViewPager) findViewById(R.id.vpMainActivity);
        mVpMain.setAdapter(mAdapter);

        mTlMain = (TabLayout) findViewById(R.id.tlMainActivity);
        mTlMain.setupWithViewPager(mVpMain);
        setupTabIcons(Constants.MainPagerAdapter.DAILY_VISION);

//        Açoes que irão acontecer na seleção, e alteração de uma TAB
        mTlMain.setOnTabSelectedListener(
                new TabLayout.ViewPagerOnTabSelectedListener(mVpMain) {
                    @Override
                    public void onTabSelected(TabLayout.Tab tab) {
                        super.onTabSelected(tab);
                        int position = tab.getPosition();
//                        Icone Branco, na Tab atual
                        tab.setIcon(tabIcons[tab.getPosition()]);
			            if (isValidTabPosition(position)) setToolbarTitle(position);

//                        Verificando se é o primeiro acesso ao mapa
                        if (CacheManager.getInstance().getFirstMap() && (tab.getPosition() ==
                                Constants.MainPagerAdapter.MAPS) &&
                                !Utility.isMobileDataBlocked(getApplicationContext())) {
                            alertMapShow().show();
                        }
                    }

                    @Override
                    public void onTabUnselected(TabLayout.Tab tab) {
                        super.onTabUnselected(tab);
//                        Icone Preto, na Tab que estava seleciona
                        tab.setIcon(tabIconsUnselected[tab.getPosition()]);
                    }

                    @Override
                    public void onTabReselected(TabLayout.Tab tab) {
                        super.onTabReselected(tab);
                    }
                }
        );

//        Se for o primeiro acesso ou se o status do usuário for falso, entra no IF
        if (CacheManager.getInstance().getFirstLaunchCache() ||
                !CacheManager.getInstance().getLoginStatusUser()) {

//            Se for o primeiro acesso vai para o ELSE
            if (!CacheManager.getInstance().getLoginStatusUser() && !CacheManager.getInstance().
                    getFirstLaunchCache()){

                CacheLocation.getInstance().setDefaultLocations();
                CacheManager.getInstance().setEventCache(new ArrayList<EventModel>());

//                Chamando Login
                Intent intent = new Intent(this, LoginActivity.class);
                startActivity(intent);

            } else {
                CacheLocation.getInstance().setDefaultLocations();
                CacheManager.getInstance().setEventCache(new ArrayList<EventModel>());

//                Chamando Onboarding
                Intent intent = new Intent(this, OnboardingActivity.class);
                startActivity(intent);
            }
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent intent = getIntent();
        if (intent != null) {
            int tabru = intent.getIntExtra(Constants.Notification.MENU_RESTAURANT_TAB_KEY, -1);
            int tabradio = intent.getIntExtra(Constants.Notification.RADIO_TAB_KEY, -1);
            int widgetIntent = intent.getIntExtra(Constants.DailyWidget.WIDGET_INTENT, -1);
            if (widgetIntent != -1) {
                mVpMain.setCurrentItem(widgetIntent);
                mAdapter.setToCurrentDay();
            }
            else if (tabru != -1) {
                mVpMain.setCurrentItem(tabru);
            } else if(tabradio != -1) {
                    mVpMain.setCurrentItem(tabradio);
            }
        }
    }

    /**
     * Método que atribui os ícones na TabLayout, sendo que a Tab atual deve ter um icone branco
     * {@link Constants#NUM_TABS indica o número máximo de Tabs}
     *
     * @param selectedTab Int que representa a Tab que é a atual
     * @see #isValidTabPosition
     */
    private void setupTabIcons(int selectedTab) {
        if (isValidTabPosition(selectedTab)) {
            mTlMain.getTabAt(Constants.MainPagerAdapter.DAILY_VISION)
                    .setIcon(tabIconsUnselected[Constants.MainPagerAdapter.DAILY_VISION]);
            mTlMain.getTabAt(Constants.MainPagerAdapter.MENU_RESTAURANT)
                    .setIcon(tabIconsUnselected[Constants.MainPagerAdapter.MENU_RESTAURANT]);
            mTlMain.getTabAt(Constants.MainPagerAdapter.UFSCAR_RADIO)
                    .setIcon(tabIconsUnselected[Constants.MainPagerAdapter.UFSCAR_RADIO]);
            mTlMain.getTabAt(Constants.MainPagerAdapter.MAPS)
                    .setIcon(tabIconsUnselected[Constants.MainPagerAdapter.MAPS]);
            mTlMain.getTabAt(selectedTab).setIcon(tabIcons[selectedTab]);
            setToolbarTitle(selectedTab);
        }
    }

    /**
     * Modifica o título da toolbar dependendo da posição da tab passada por parâmetro
     *
     * @param position indica qual tab está selecionada no momento
     * @throws IllegalStateException Caso a posição da tab não seja válida
     * @see #isValidTabPosition
     */
    private void setToolbarTitle(int position) throws IllegalStateException {
        if (getSupportActionBar() != null) {
            switch (position) {
                case Constants.MainPagerAdapter.DAILY_VISION:
                    getSupportActionBar().setTitle(R.string.daily_vision_title);
                    break;
                case Constants.MainPagerAdapter.MENU_RESTAURANT:
                    getSupportActionBar().setTitle(R.string.menu_restaurant_title);
                    break;
                case Constants.MainPagerAdapter.UFSCAR_RADIO:
                    getSupportActionBar().setTitle(R.string.radio_title);
                    break;
                case Constants.MainPagerAdapter.MAPS:
                    getSupportActionBar().setTitle(R.string.maps_title);
                    break;
                default:
                    throw new IllegalStateException("Invalid tab position:" + position);
            }
        }
    }

    /**
     * Verifica se a posição passada pelo parâmetro é uma posição válida da tab
     *
     * @param position posição que se queria verificar
     * @return true se for posição válida e false caso contrário
     */
    private boolean isValidTabPosition(int position) {

        return position == Constants.MainPagerAdapter.DAILY_VISION ||
                position == Constants.MainPagerAdapter.MENU_RESTAURANT ||
                position == Constants.MainPagerAdapter.UFSCAR_RADIO ||
                position == Constants.MainPagerAdapter.MAPS;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_config) {
            Intent intent = new Intent(this, ConfigActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Método que exibe um {@link AlertDialog} para o usuário informando que o serviço do mapa
     * pode utilizar o pacote de dados
     *
     * @return Um Alert para o usuário
     * @see CacheManager#setFirstMap(boolean)
     * @see CacheManager#setEnableMap(boolean)
     * @see MapEnableFragment
     */
    AlertDialog alertMapShow() {

        AlertDialog mAlertDialog;

        mAlertDialog = new AlertDialog.Builder(this, R.style.AlertMapStyle).create();

        mAlertDialog.setMessage(this.getResources().getString(R.string.
                map_first_dialog_mensagem));

        mAlertDialog.setButton(AlertDialog.BUTTON_NEGATIVE,
                getResources().getString(R.string.cancel),
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
//                        Dialog não aparece mais para o usuário
                        CacheManager.getInstance().setFirstMap(false);
                    }
                });

        mAlertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getResources().getString(R.string.ok),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        CacheManager.getInstance().setFirstMap(false);
                        CacheManager.getInstance().setEnableMap(true);
                        mAdapter.getItem(Constants.MainPagerAdapter.MAPS).getChildFragmentManager()
                                .beginTransaction().replace(R.id.flContainer, new MapFragment())
                                .commit();
                    }
                });

        mAlertDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
//                Dialog não aparece mais para o usuário
                CacheManager.getInstance().setFirstMap(false);
            }
        });

        return mAlertDialog;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        Encerra a thread que executa o reset da variavel mCloseApp
        if(mHandler != null) mHandler.removeCallbacks(mRunnable);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(RadioService.isStarting()) {
            stopService(new Intent(this, RadioService.class));
        }
    }

    @Override
    public void onBackPressed() {
        if(mCloseApp) {
            super.onBackPressed();
            return;
        }

        if(mRadioStarting &&
                mTlMain.getSelectedTabPosition() == Constants.MainPagerAdapter.UFSCAR_RADIO){
            stopService(new Intent(this, RadioService.class));
            mRadioStarting = false;
        } else {
            mCloseApp = true;
            Toast.makeText(this,
                    Constants.MainActivity.EXIT_ACTIVITY_MESSAGE, Toast.LENGTH_SHORT).show();
            mHandler.postDelayed(mRunnable, Constants.MainActivity.EXIT_ACTIVITY_TIME);
        }
    }
}