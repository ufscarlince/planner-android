package mobile.ufscar.br.app.logic;

import mobile.ufscar.br.app.model.UfscarResponseError;

/**
 * Callback usado por {@link CurrentTimeMillisService} quando houve sucesso ou não em buscar o
 * horário da API.
 */

public interface CurrentTimeServiceListener {

    /**
     * Callback quando houve sucesso em buscar o horário
     *
     * @param currentTime Horário buscado na API
     */
    void onGetTimeSuccess(long currentTime);

    /**
     * Callback quando não houve sucesso em buscar o horário
     *
     * @param error da requisição
     */
    void onGetTimeFailure(UfscarResponseError error);
}