package mobile.ufscar.br.app.logic;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import mobile.ufscar.br.app.config.Constants;
import mobile.ufscar.br.app.model.ConfigurationModel;
import mobile.ufscar.br.app.model.EventModel;
import mobile.ufscar.br.app.model.RestaurantMenuModel;
import mobile.ufscar.br.app.model.LoginResponseModel;

/**
 * Usa {@link SharedPreferences} para salvar dados.
 */

public class CacheManager {

    private static CacheManager sInstance;

    private CacheManager() {
    }

    public static CacheManager getInstance() {
        if (sInstance == null) {
            sInstance = new CacheManager();
        }
        return sInstance;
    }

    private Context getContext() {
        return UfscarApplication.getInstance();
    }

    /**
     * Coloca um evento na lista de eventos
     *
     * @return boolean true, se a inserção deu certo, caso contrário, false
     * @see EventModel
     */
    public boolean addEventCache(EventModel eventModel) {
        List<EventModel> list = getEventCache();
        if (eventModel != null) {
            list.add(eventModel);
            setEventCache(list);
            return true;
        }
        return false;
    }

    /**
     * Altera um evento da lista de eventos
     *
     * @param position   int com a posição do evento a ser editado
     * @param eventModel EventoModel a ser colocado no lugar do anterior.
     * @return boolean true, se a alteração deu certo, caso contrário, false
     */
    public boolean editEventCache(int position, EventModel eventModel) {
        List<EventModel> list = getEventCache();
        if (eventModel != null && position < list.size() && position >= 0) {
            list.set(position, eventModel);
            setEventCache(list);
            return true;
        }
        return false;
    }

    /**
     * Remove um evento da lista de eventos
     *
     * @param position int com a posição do evento a ser removido.
     * @return boolean true, se a remoção deu certo, caso contrário, false
     */
    public boolean removeEventCache(int position) {
        List<EventModel> list = getEventCache();
        if (position < list.size() && position >= 0) {
            list.remove(position);
            setEventCache(list);
            return true;
        }
        return false;
    }

    /**
     * Remove um evento da lista de eventos
     *
     * @param eventModel EventModel a ser removido.
     * @return boolean true, se a remoção deu certo, caso contrário, false
     */
    public boolean removeEventCache(EventModel eventModel) {
        List<EventModel> list = getEventCache();
        for (int i = 0; i < list.size(); i++) {
            if (list.remove(eventModel)) {
                setEventCache(list);
                return true;
            }
        }
        return false;
    }

    /**
     * Coloca uma lista de {@link EventModel} em um {@link SharedPreferences}
     */
    public void setEventCache(@NonNull List<EventModel> list) {
        if (getContext() != null) {
            SharedPreferences preferences = getContext().
                    getSharedPreferences(Constants.CacheManager.CACHE_NAME, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();

//            Transforma a lista em JSON
            String listJsonString = new Gson().toJson(list);
            editor.putString(Constants.CacheManager.CACHE_EVENT_KEY, listJsonString);
            editor.apply();
        }
    }

    /**
     * Retorna os {@link EventModel} salvos no {@link SharedPreferences}
     *
     * @return List com os eventos salvos, ou uma lista vazia caso não tenha eventos
     */
    public List<EventModel> getEventCache() {
        SharedPreferences preferences = getContext().
                getSharedPreferences(Constants.CacheManager.CACHE_NAME, Context.MODE_PRIVATE);

        List<EventModel> list  = new ArrayList<>();
        String listJsonString = preferences.getString(Constants.CacheManager.CACHE_EVENT_KEY, null);
        if (listJsonString != null) {
            Type type = new TypeToken<List<EventModel>>() {}.getType();
            list = new Gson().fromJson(listJsonString, type);
        }

        return list;
    }

    /**
     * Coloca uma lista de {@link RestaurantMenuModel} em um {@link SharedPreferences}
     */
    public void setMenuCache(List<RestaurantMenuModel> list) {
        SharedPreferences preferences = getContext().
                getSharedPreferences(Constants.CacheManager.CACHE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

//        Transforma a lista em JSON
        String listJsonString = new Gson().toJson(list);
        editor.putString(Constants.CacheManager.CACHE_MENU_KEY, listJsonString);
        editor.apply();
    }

    /**
     * Retorna os {@link RestaurantMenuModel} salvos no {@link SharedPreferences}
     *
     * @return List com os RestaurantMenuModel salvos ou uma lista vazia caso não tenha nada salvo
     */
    public List<RestaurantMenuModel> getMenuCache() {
        SharedPreferences preferences = getContext().
                getSharedPreferences(Constants.CacheManager.CACHE_NAME, Context.MODE_PRIVATE);

        List<RestaurantMenuModel> list = new ArrayList<>();
        String listJsonString = preferences.getString(Constants.CacheManager.CACHE_MENU_KEY, null);
        if (listJsonString != null) {
            Type type = new TypeToken<List<RestaurantMenuModel>>() {}.getType();
            list = new Gson().fromJson(listJsonString, type);
        }

        return list;
    }

    /**
     * Salva um {@link LoginResponseModel} no cache
     */
    public void setLoginCache(LoginResponseModel loginResponseModel) {
        SharedPreferences preferences = getContext().
                getSharedPreferences(Constants.CacheManager.CACHE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        String loginResponseJson = new Gson().toJson(loginResponseModel);
        editor.putString(Constants.CacheManager.CACHE_LOGIN_KEY, loginResponseJson);
        editor.apply();
    }

    /**
     * Recupera o {@link LoginResponseModel} salvado no cache
     *
     * @return LoginResponse salvo no cache ou null, se o cache estiver vazio
     */
    public LoginResponseModel getLoginCache() {
        SharedPreferences preferences = getContext().
                getSharedPreferences(Constants.CacheManager.CACHE_NAME, Context.MODE_PRIVATE);

        LoginResponseModel loginResponseModel = null;
        String loginResponseJson = preferences.getString(
                Constants.CacheManager.CACHE_LOGIN_KEY, null);
        if (loginResponseJson != null) {
            loginResponseModel = new Gson().fromJson(loginResponseJson, LoginResponseModel.class);
        }

        return loginResponseModel;
    }

    /**
     * Salva um {@link ConfigurationModel} no cache
     */
    public void setConfigCache(ConfigurationModel configurationModel) {
        SharedPreferences preferences = getContext().
                getSharedPreferences(Constants.CacheManager.CACHE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        String configJson = new Gson().toJson(configurationModel);
        editor.putString(Constants.CacheManager.CACHE_CONFIG_KEY, configJson);
        editor.apply();
    }

    /**
     * Recupera o {@link ConfigurationModel} salvo no cache
     *
     * @return ConfigurationModel salvo no cache ou null, se o cache estiver vazio
     */
    public ConfigurationModel getConfigCache() {
        SharedPreferences preferences = getContext().
                getSharedPreferences(Constants.CacheManager.CACHE_NAME, Context.MODE_PRIVATE);

        ConfigurationModel configurationModel = null;
        String configJson = preferences.getString(Constants.CacheManager.CACHE_CONFIG_KEY, null);
        if (configJson != null) {
            configurationModel = new Gson().fromJson(configJson, ConfigurationModel.class);
        }

        return configurationModel;
    }

    /**
     * Salva um booleano no cache
     *
     * @param firstLaunch indica se é a primeira inicialização do aplicativo
     */
    public void setFirstLaunchCache(boolean firstLaunch) {
        SharedPreferences preferences = getContext().
                getSharedPreferences(Constants.CacheManager.CACHE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        editor.putBoolean(Constants.CacheManager.CACHE_FIRST_LAUNCH_KEY, firstLaunch);
        editor.apply();
    }

    /**
     * Recupera o booleano salvo no cache
     *
     * @return Se não estiver nada salvo no cache (por exemplo, no primeiro uso do aplicativo),
     * retornará true caso contrário, retornará o que estiver salvo no cache
     */
    public boolean getFirstLaunchCache() {
        SharedPreferences preferences = getContext().
                getSharedPreferences(Constants.CacheManager.CACHE_NAME, Context.MODE_PRIVATE);

        return preferences.getBoolean(Constants.CacheManager.CACHE_FIRST_LAUNCH_KEY, true);
    }

    /**
     * Salva um {@link Boolean} no cache, que indica o statusLogin atual do usuário.
     *
     * @param statusUser indica se o usuário vai precisar ou não realizar o Login na abertura do
     *                   aplicativo
     *                   Recebe: {@link Boolean#FALSE} se o usuário precisar fazer o login
     *                   Recebe: {@link Boolean#TRUE} se o nao precisar fazer o login
     * @see mobile.ufscar.br.app.ui.LoginActivity
     */
    public void setLoginStatusUser(boolean statusUser){
        SharedPreferences preferences = getContext().
                getSharedPreferences(Constants.CacheManager.CACHE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        editor.putBoolean(Constants.CacheManager.CACHE_STATUS_LOGIN_USER, statusUser);
        editor.apply();
    }

    /**
     * Busca um {@link Boolean} salvo no cache, no caso o statusLogin do usuário
     *
     * @return {@link Boolean#TRUE} se a tela de login NÃO deve aparecer para o usuário
     *         {@link Boolean#FALSE} se a tela de login deve aparecer para o usuário
     * @see mobile.ufscar.br.app.ui.LoginActivity
     * @see mobile.ufscar.br.app.config.Constants.CacheManager#CACHE_STATUS_LOGIN_USER
     */
    public boolean getLoginStatusUser(){

        SharedPreferences preferences = getContext().getSharedPreferences(
                Constants.CacheManager.CACHE_NAME, Context.MODE_PRIVATE);

        return preferences.getBoolean(Constants.CacheManager.CACHE_STATUS_LOGIN_USER, false);

    }

    /**
     * Salva um {@link Boolean} no cache, que indica o defermentStatus do usuário
     *
     * @param defermentStatus indica se o usuário vai precisar ou não realizar o Login na abertura
     *                        do aplicativo
     *                        Recebe: {@link Boolean#FALSE} se a busca pelo deferimento
     *                        ainda não aconteceu.
     *                        Recebe: {@link Boolean#TRUE} se a busca pelo deferimento já
     *                        aconteceu com sucesso.
     * @see mobile.ufscar.br.app.ui.LoginActivity
     */
    public void setDefermentStatus(boolean defermentStatus){
        SharedPreferences preferences = getContext().
                getSharedPreferences(Constants.CacheManager.CACHE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(Constants.CacheManager.CACHE_DEFERMENT_STATUS, defermentStatus);
        editor.apply();
    }

    /**
     * Salva um {@link Boolean} no cache
     *
     * @param firstMap verifica se é a primeira utilização do
     * {@link mobile.ufscar.br.app.ui.MapFragment}
     * @see CacheManager#getFirstMap()
     */
    public void setFirstMap(boolean firstMap){

        SharedPreferences preferences = getContext().
                getSharedPreferences(Constants.CacheManager.CACHE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(Constants.CacheManager.CACHE_FIRST_TAB_MAP_KEY, firstMap);
        editor.apply();
    }

    /**
     * Busca um {@link Boolean} salvo no cache, que representa o defermentStatus.
     *
     * @return {@link Boolean#TRUE} se a busca pelo deferimento já aconteceu com sucesso.
     *         {@link Boolean#FALSE} se a busca pelo deferimento ainda não aconteceu.
     * @see mobile.ufscar.br.app.ui.LoginActivity
     * @see mobile.ufscar.br.app.config.Constants.CacheManager#CACHE_DEFERMENT_STATUS
     */
    public boolean getDefermentStatus() {

        SharedPreferences preferences = getContext().getSharedPreferences(
                Constants.CacheManager.CACHE_NAME, Context.MODE_PRIVATE);

        return preferences.getBoolean(Constants.CacheManager.CACHE_DEFERMENT_STATUS, false);
    }

    /**
     * Recupera um {@link Boolean} no cache com o status de primeiro acesso do mapa
     *
     * @return Se não tiver nada salvo no cache, retorna {@link Boolean#TRUE} que indica que o
     * usuário vai acessar o map pela primeira vez, caso contrário retorna o que está salvo no
     * cache
     * @see CacheManager#setFirstMap(boolean)
     */
    public boolean getFirstMap(){

        SharedPreferences preferences = getContext().
                getSharedPreferences(Constants.CacheManager.CACHE_NAME, Context.MODE_PRIVATE);

        return preferences.getBoolean(Constants.CacheManager.CACHE_FIRST_TAB_MAP_KEY, true);
    }

    /**
     * Salva um {@link Boolean} no cache que indica se o mapa foi ativado ou não pelo usuário
     *
     * @param mapEnable que define se o map está ou não ativado
     * @see mobile.ufscar.br.app.ui.MapEnableFragment
     * @see CacheManager#isEnableMap()
     */
    public void setEnableMap(boolean mapEnable){

        SharedPreferences preferences = getContext().
                getSharedPreferences(Constants.CacheManager.CACHE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        editor.putBoolean(Constants.CacheManager.CACHE_ENABLE_MAP_KEY, mapEnable);
        editor.apply();
    }

    /**
     * Recupera um {@link Boolean} do cache, que indica se o mapa foi ativado ou não pelo usuário
     *
     * @return  {@link Boolean} que indica se o mapa está ou não ativado
     * @see CacheManager#setEnableMap(boolean)
     */
    public boolean isEnableMap(){

        SharedPreferences preferences = getContext().
                getSharedPreferences(Constants.CacheManager.CACHE_NAME, Context.MODE_PRIVATE);

        return preferences.getBoolean(Constants.CacheManager.CACHE_ENABLE_MAP_KEY, false);
    }
}