package mobile.ufscar.br.app.ui;

import android.widget.TextView;

/**
 * Os eventos sao mapeados para esta classe no metodo getView em {@link EventAdapter}, com ela
 * pode-se reutilizar as views das celulas de Evento e Evento Vazio.
 */

public class ViewHolderEvent {

    private TextView mTvScheduleEvent;
    private TextView mTvScheduleEmptyCell;
    private TextView mTvNameEventCell;
    private TextView mTvLocation;

    public ViewHolderEvent(TextView tvScheduleEvent, TextView tvNameEventCell, TextView tvLocation) {
        this.mTvScheduleEvent = tvScheduleEvent;
        this.mTvNameEventCell = tvNameEventCell;
        this.mTvLocation = tvLocation;
    }

    public ViewHolderEvent(TextView tvScheduleEmptyCell) {
        this.mTvScheduleEmptyCell = tvScheduleEmptyCell;
    }

    public TextView getTvScheduleEvent() {
        return mTvScheduleEvent;
    }

    public TextView getTvScheduleEmptyCell() { return mTvScheduleEmptyCell; }

    public TextView getTvNameEventCell() {
        return mTvNameEventCell;
    }

    public TextView getTvLocation() {
        return mTvLocation;
    }
}
