package mobile.ufscar.br.app.ui;

import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Calendar;

import mobile.ufscar.br.app.R;
import mobile.ufscar.br.app.config.Constants;
import mobile.ufscar.br.app.logic.EventServiceListener;
import mobile.ufscar.br.app.logic.UfscarApplication;

/**
 * Fragmento da dailyVision, utiliza {@link DailyPagerAdapter} para gerar as abas dos dias e em
 * cada dia utiliza {@link EventAdapter} para mostrar os compromissos ao usuário, realiza
 * requisições ao servidor para restaurar as aulas vindas do SIGA e ouve as respostas delas
 * através de {@link EventServiceListener}
 */

public class DailyVisionFragment extends Fragment {

    private DailyPagerAdapter mDailyPagerAdapter;
    private ViewPager mVpDaily;
    private TabLayout mTabLayout;
    private int mCurrentDayOfTheWeek;
    private Calendar mCalendar;

    private UfscarApplication getApplication() {
        return UfscarApplication.getInstance();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCurrentDayOfTheWeek = currentDay();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_daily_vision, container, false);

        mVpDaily = (ViewPager) view.findViewById(R.id.vpDaily);
        mDailyPagerAdapter = new DailyPagerAdapter(getChildFragmentManager());
        mVpDaily.setAdapter(mDailyPagerAdapter);
        mTabLayout = (TabLayout) view.findViewById(R.id.tlDaily);
        mTabLayout.setupWithViewPager(mVpDaily);
        mTabLayout.setOnTabSelectedListener(
                new TabLayout.ViewPagerOnTabSelectedListener(mVpDaily) {
                    @Override
                    public void onTabSelected(TabLayout.Tab tab) {
                        LinearLayout tabView = (LinearLayout) tab.getCustomView();
                        if (tabView != null) {
                            TextView tabIcon = (TextView) tabView.
                                    findViewById(R.id.tvDailyVisionTabIcon);
                            setTabIconColor(tabIcon, Constants.DailyVisionFragment.SELECTED);
                            mVpDaily.setCurrentItem(tab.getPosition());
                        } else throw getNoCustomViewException(tab.getPosition());
                    }

                    @Override
                    public void onTabUnselected(TabLayout.Tab tab) {
                        LinearLayout tabView = (LinearLayout) tab.getCustomView();
                        if (tabView != null) {
                            TextView tabIcon = (TextView) tabView.
                                    findViewById(R.id.tvDailyVisionTabIcon);
                            if (tab.getPosition() == mCurrentDayOfTheWeek) {
                                setTabIconColor(tabIcon, Constants.DailyVisionFragment.CURRENT_DAY);
                            } else {
                                setTabIconColor(tabIcon,
                                        Constants.DailyVisionFragment.NOT_SELECTED);
                            }
                        } else throw getNoCustomViewException(tab.getPosition());
                    }
                });

        setupTabs();
        mVpDaily.setCurrentItem(mCurrentDayOfTheWeek);

        return view;
    }

    /**
     * Retorna o dia atual da semana, valores possíveis especificados em {@link Constants},
     * além disso {@link #mCalendar} é construído de forma que está sempre na segunda daquela
     * semana. Caso seja domingo, começa a contar da outra semana
     *
     * @return dia atual da semana, valores possíveis em {@link Constants}
     */
    private int currentDay() {
        mCalendar = Calendar.getInstance();
        int dayOfTheWeek = mCalendar.get(Calendar.DAY_OF_WEEK);
        switch (dayOfTheWeek) {
            case Calendar.MONDAY:
                return Constants.SEGUNDA;
            case Calendar.TUESDAY:
                mCalendar.add(Calendar.DAY_OF_MONTH, -1);
                return Constants.TERCA;
            case Calendar.WEDNESDAY:
                mCalendar.add(Calendar.DAY_OF_MONTH, -2);
                return Constants.QUARTA;
            case Calendar.THURSDAY:
                mCalendar.add(Calendar.DAY_OF_MONTH, -3);
                return Constants.QUINTA;
            case Calendar.FRIDAY:
                mCalendar.add(Calendar.DAY_OF_MONTH, -4);
                return Constants.SEXTA;
            case Calendar.SATURDAY:
                mCalendar.add(Calendar.DAY_OF_MONTH, -5);
                return Constants.SABADO;
            default:
                mCalendar.add(Calendar.DAY_OF_MONTH, -6);
                return Constants.DOMINGO;
        }
    }

    /**
     * Retorna o dia do mês do dia daquela semana, {@link #mCalendar} é construído de forma que
     * está sempre na segunda daquela semana
     *
     * @return dia do mês do dia especificado em {@code daysAfterMonday}
     * @see #currentDay
     */
    private int getDayOfTheMonth(int daysAfterMonday) {
        mCalendar.add(Calendar.DAY_OF_MONTH, daysAfterMonday);
        int day = mCalendar.get(Calendar.DAY_OF_MONTH);
        mCalendar.add(Calendar.DAY_OF_MONTH, -daysAfterMonday);
        return day;
    }

    /**
     * Especifica os ícones das tabs e o título das mesmas
     */
    private void setupTabs() {
        for (int i = 0; i < mDailyPagerAdapter.getCount(); i++) {
            LinearLayout tabView = (LinearLayout)
                    LayoutInflater.from(getContext()).inflate(R.layout.tab_daily_vision, null);
            TextView tvTitle = (TextView) tabView.findViewById(R.id.tvDailyVisionTabTitle);
            tvTitle.setText(mDailyPagerAdapter.getPageTitle(i));
            TextView tvIcon = (TextView) tabView.findViewById(R.id.tvDailyVisionTabIcon);
            tvIcon.setText(getDayOfTheMonth(i) + "");
            if (mCurrentDayOfTheWeek == i) {
                setTabIconColor(tvIcon, Constants.DailyVisionFragment.SELECTED);
            } else setTabIconColor(tvIcon, Constants.DailyVisionFragment.NOT_SELECTED);
            TabLayout.Tab tab = mTabLayout.getTabAt(i);
            if (tab != null) tab.setCustomView(tabView);
            else throw getIndexOutOfBoundsException(i, mDailyPagerAdapter.getCount());
        }
    }

    /**
     * Especifica a cor de texto e de background do ícone de uma tab específica
     *
     * @param tabIcon o ícone da tab que se quer alterar
     * @param status  estado do ícone para o qual se quer mudar, pode ser
     *                {@link Constants.DailyVisionFragment#SELECTED},
     *                {@link Constants.DailyVisionFragment#NOT_SELECTED} ou
     *                {@link Constants.DailyVisionFragment#CURRENT_DAY}
     * @throws IllegalStateException caso {@code status} for inválido
     */
    private void setTabIconColor(TextView tabIcon, int status) {
        GradientDrawable bgIcon = (GradientDrawable) tabIcon.getBackground();
        switch (status) {
            case Constants.DailyVisionFragment.SELECTED:
                tabIcon.setTextColor(ContextCompat.getColor(getApplication(),
                        R.color.colorDailyVisionTabSelectedText));
                bgIcon.setColor(ContextCompat.getColor(getApplication(),
                        R.color.colorLipstick));
                break;
            case Constants.DailyVisionFragment.NOT_SELECTED:
                tabIcon.setTextColor(ContextCompat.getColor(getApplication(),
                        R.color.colorBlack));
                bgIcon.setColor(ContextCompat.getColor(getApplication(),
                        R.color.colorMainBackground));
                break;
            case Constants.DailyVisionFragment.CURRENT_DAY:
                tabIcon.setTextColor(ContextCompat.getColor(getApplication(),
                        R.color.colorBlack));
                bgIcon.setColor(ContextCompat.getColor(getApplication(),
                        R.color.colorDailyVisionTabCurrentDayIconBackground));
                break;
            default:
                throw new IllegalArgumentException("Invalid status: " + status);
        }
    }

    /**
     * Exceção para quando a tab selecionada não tem CustomView
     *
     * @return NullPointerException com a tab selecionada
     */
    private NullPointerException getNoCustomViewException(int tab) {
        throw new NullPointerException("tab " + tab + " has no CustomView");
    }

    /**
     * Exceção para quando se tenta acessar uma tab que não existe
     *
     * @return NullPointerException com a tab que houve tentativa de acesso
     */
    private IndexOutOfBoundsException getIndexOutOfBoundsException(int index, int size) {
        return new IndexOutOfBoundsException("Invalid index " + index + ", size is " + size);
    }

    /**
     * Coloca a tab no dia atual, usado quando o App é lançado através do Widget
     * {@link mobile.ufscar.br.app.logic.DailyWidgetProvider}
     */
    public void setTabSelection(){
        if(mVpDaily != null) {
            mVpDaily.setCurrentItem(currentDay());
        }
    }
}
