package mobile.ufscar.br.app.logic;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import mobile.ufscar.br.app.utils.Utility;

/**
 * Receiver Para o alarme que atualiza o Widget.
 */
public class WidgetAlarmReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Utility.updateWidget(context);
    }
}
