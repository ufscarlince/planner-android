package mobile.ufscar.br.app.logic;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.wifi.WifiManager;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.v4.app.Fragment;
import android.support.v4.media.session.MediaSessionCompat;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.RemoteViews;

import java.io.IOException;

import mobile.ufscar.br.app.R;
import mobile.ufscar.br.app.config.Constants;
import mobile.ufscar.br.app.logic.receivers.RadioControlReceiver;
import mobile.ufscar.br.app.ui.MainActivity;
import mobile.ufscar.br.app.utils.Utility;

/**
 * Um {@link Service} que controla a rádio e ao começar a tocar cria uma notificação para que
 * o usuário possa interagir com a reprodução
 */

public class RadioService extends Service {

    private static final String LOG_TAG = RadioService.class.getName();
    public static final String PLAY_INTENT = "mobile.ufscar.br.app.logic.PLAY_INTENT";
    public static final String PAUSE_INTENT = "mobile.ufscar.br.app.logic.PAUSE_INTENT";
    public static final String DESTROY_INTENT = "mobile.ufscar.br.app.logic.DESTROY_INTENT";
    private static final String RADIO_URL = "http://www.radio.ufscar.br:8000/radioufscar.ogg";
    private static final String WIFI_LOCK = "lock";
    private ComponentName mRadioControlReceiverName = new ComponentName(
            "mobile.ufscar.br.app.logic.receivers", RadioControlReceiver.class.getName());
    private RadioServiceListener mRadioServiceListener;
    private MediaSessionCompat mMediaSessionCompat;
    private AudioManager mAudioManager;
    private MediaPlayer mMediaPlayer;
    private boolean mAtLeastLollipop;
//    Indica que mMediaPlayer está inicializado e parado
    private boolean mStopped;
//    Indica que mMediaPlayer está inicializando
    private static boolean mStarting;
//    Indica que mMediaPlayer está inicializado
    private boolean mReady;
    private boolean mHasListener;
    private WifiManager.WifiLock mWifiLock;
    private final IBinder mBinder = new RadioBinder();

//    Quando a rádio ficar pronta, a rádio começara a tocar
    private MediaPlayer.OnPreparedListener mMediaPlayerHandler =
            new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    if (requestAudioFocus() == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
                        mWifiLock.acquire();
                        mStopped = false;
                        mStarting = false;
                        mReady = true;
                        mMediaSessionCompat.setActive(true);
                        playRadio();
                        showNotification(true);
                    } else stopRadio();
                }
            };

//    Se algum erro acontecer, a rádio será destroida
    private MediaPlayer.OnErrorListener mMediaPlayerErrorHandler =
            new MediaPlayer.OnErrorListener() {
                @Override
                public boolean onError(MediaPlayer mp, int what, int extra) {
                    if (!Utility.checkConnection(getApplicationContext())) {
                        if (mHasListener) mRadioServiceListener.setDragonRadioLayout();
                    } else {
                        if (mHasListener) mRadioServiceListener.radioFailed();
                    }
                    stopRadio();
                    return true;
                }
            };

//    Tratamento de quando há mudança no AudioFocus
    private AudioManager.OnAudioFocusChangeListener mAudioFocusListener =
            new AudioManager.OnAudioFocusChangeListener() {
                @Override
                public void onAudioFocusChange(int focusChange) {
                    switch (focusChange) {
                        case AudioManager.AUDIOFOCUS_GAIN:
                            if (mMediaPlayer == null) initMediaPlayer();
                            else if (!mMediaPlayer.isPlaying()) playRadio();
                            mMediaPlayer.setVolume
                                    (Constants.RadioService.MEDIA_PLAYER_LEFT_VOLUME,
                                            Constants.RadioService.MEDIA_PLAYER_RIGHT_VOLUME);
                            break;
                        case AudioManager.AUDIOFOCUS_LOSS:
//                            Destrói a rádio ao perder focus por um longo periodo
                            if (mMediaPlayer.isPlaying()) mMediaPlayer.stop();
                            stopRadio();
                            break;
                        case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                            if (mMediaPlayer.isPlaying()) pauseRadio();
                            break;
                        case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                            if (mMediaPlayer.isPlaying()) mMediaPlayer.setVolume(
                                    Constants.RadioService.MEDIA_PLAYER_LEFT_VOLUME,
                                    Constants.RadioService.MEDIA_PLAYER_RIGHT_VOLUME);
                            break;
                        default:
                            Log.d(LOG_TAG, Constants.RadioService.RADIO_AUDIO_FOCUS_CHANGE_DEFAULT
                                    + focusChange);
                    }
                }
            };

    public MediaPlayer getMediaPlayer() {
        return mMediaPlayer;
    }

    public boolean isReady() {
        return mReady;
    }

    public boolean isStopped() {
        return mStopped;
    }

    public static boolean isStarting() {
        return mStarting;
    }

    public RadioService() {
    }

    public class RadioBinder extends Binder {
        public RadioService getService() {
            return RadioService.this;
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mAtLeastLollipop = Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
        initMediaPlayer();

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        if (mMediaPlayer != null) stopSound();
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    /**
     * Começa o processo de inicialização da rádio, finalizando a inicialização assincronamente
     */
    private void initMediaPlayer() {
        mStopped = true;
        mReady = false;
        mHasListener = false;
        mWifiLock = ((WifiManager) getSystemService(Context.WIFI_SERVICE))
                .createWifiLock(WifiManager.WIFI_MODE_FULL, WIFI_LOCK);
        mAudioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
        configureMediaSessionCompat();
        mMediaPlayer = new MediaPlayer();
        mMediaPlayer.setOnPreparedListener(mMediaPlayerHandler);
        mMediaPlayer.setOnErrorListener(mMediaPlayerErrorHandler);
        mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mMediaPlayer.setWakeMode(getApplicationContext(), PowerManager.PARTIAL_WAKE_LOCK);
        try {
            mMediaPlayer.setDataSource(RADIO_URL);
        } catch (IOException e) {
            Log.w(LOG_TAG, e);
        }
        mStarting = true;
        mMediaPlayer.prepareAsync();
    }

    /**
     * Destrói a rádio, o service inteiro será destroido
     */
    public void stopRadio() {
        stopSound();
        stopSelf();
    }

    /**
     * Desliga a rádio, liberando a memória utilizada para o {@link MediaPlayer} de forma que
     * ligar a rádio de novo alocará nova memória
     */
    private void stopSound() {
        if (mMediaPlayer != null) {
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
        if (mMediaSessionCompat != null) {
            mMediaSessionCompat.setActive(false);
            mMediaSessionCompat.release();
        }
        mStopped = true;
        mReady = false;
        stopForeground(true);
        if (mWifiLock.isHeld()) mWifiLock.release();
        mAudioManager.abandonAudioFocus(mAudioFocusListener);
        if (mHasListener) mRadioServiceListener.radioStopped();
    }

    /**
     * Faz a rádio tocar (se estiver pausado, da continuação à rádio
     * e se estiver parado carrega de novo)
     */
    public void playRadio() {
        if (mStopped) {
            if (mHasListener) mRadioServiceListener.radioStartingToPlay();
            mMediaPlayer.prepareAsync();
        } else {
            mMediaPlayer.start();
            if (mHasListener) mRadioServiceListener.radioPlaying();
        }
//        Altera o status da notificação
        showNotification(true);
    }

    /**
     * Para de tocar a rádio sem destroí-la, dar play fará a rádio começar rapidamente de onde
     * tenha parado, não de onde a rádio está atualmente
     */
    public void pauseRadio() {
        mMediaPlayer.pause();
        if (mHasListener) mRadioServiceListener.radioPaused();
    }

    /**
     * Registra um listener que vai interagir com a rádio
     */
    public void registerClient(Fragment fragment) {
        this.mRadioServiceListener = (RadioServiceListener) fragment;
        mHasListener = true;
    }

    /**
     * Tira qualquer listener que esteja vinculado com a rádio
     */
    public void unRegisterClient() {
        this.mRadioServiceListener = null;
        mHasListener = false;
    }

    /**
     * Configura como será tratado os toques em devices que interagem com media
     */
    private void configureMediaSessionCompat() {
        mMediaSessionCompat = new MediaSessionCompat(this, LOG_TAG,
                mRadioControlReceiverName, null);
        mMediaSessionCompat.setFlags(MediaSessionCompat.FLAG_HANDLES_MEDIA_BUTTONS
                | MediaSessionCompat.FLAG_HANDLES_TRANSPORT_CONTROLS);
        mMediaSessionCompat.setCallback(new MediaSessionCompat.Callback() {
            @Override
            public void onPlay() {
                playRadio(RadioService.this);
            }

            @Override
            public void onPause() {
                pauseRadio(RadioService.this);
            }

            @Override
            public void onStop() {
                destroyRadio(RadioService.this);
            }

            @Override
            public boolean onMediaButtonEvent(Intent mediaButtonEvent) {
                KeyEvent keyEvent = mediaButtonEvent.getParcelableExtra(Intent.EXTRA_KEY_EVENT);
                if (keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyEvent.getKeyCode() == KeyEvent.KEYCODE_HEADSETHOOK) {
                        playOrPauseRadio(RadioService.this);
                        return true;
                    }
                }
                return super.onMediaButtonEvent(mediaButtonEvent);
            }
        });
    }

    private int requestAudioFocus() {
        return mAudioManager.requestAudioFocus(mAudioFocusListener, AudioManager.STREAM_MUSIC,
                AudioManager.AUDIOFOCUS_GAIN);
    }

    /**
     * Notificação que controla a rádio, todos os ajustes são feitos em {@link RadioControlReceiver}
     *
     * @param playingRadio: Boleano que controla o botão play/pause da notificacao em caso de true
     *                    a radio está tocando, logo o botão visível é o pause, em caso de false a
     *                    rádio está pausada e o botão visível é o de play
     * @see mobile.ufscar.br.app.R.layout#radio_notification
     * @see mobile.ufscar.br.app.R.id#ibNotificationPlayPause
     */
    public void showNotification(boolean playingRadio) {

        RemoteViews notificationView = new RemoteViews(this.getPackageName(),
                R.layout.radio_notification);

        if (playingRadio) {
            Intent pauseIntent = new Intent(PAUSE_INTENT);
            notificationView.setOnClickPendingIntent(R.id.ibNotificationPlayPause,
                    PendingIntent.getBroadcast(this, 0, pauseIntent, 0));
            notificationView.setImageViewResource(R.id.ibNotificationPlayPause,
                    R.drawable.ic_notification_radio_pause);

        } else {
            Intent playIntent = new Intent(PLAY_INTENT);
            notificationView.setOnClickPendingIntent(R.id.ibNotificationPlayPause,
                    PendingIntent.getBroadcast(this, 0, playIntent, 0));
            notificationView.setImageViewResource(R.id.ibNotificationPlayPause,
                    R.drawable.ic_notification_radio_play);
        }

        Intent destroyIntent = new Intent(DESTROY_INTENT);
        notificationView.setOnClickPendingIntent(R.id.ibNotificationDestroy,
                PendingIntent.getBroadcast(this, 0, destroyIntent, 0));


        Notification.Builder builder = new Notification.Builder(this)
                .setSmallIcon(R.mipmap.ic_radio)
                .setWhen(System.currentTimeMillis())
                .setContent(notificationView);

        if (mAtLeastLollipop) {
            builder.setVisibility(Notification.VISIBILITY_PUBLIC);
        }

        Notification notification = builder.build();

        Intent notificationIntent = new Intent(this, MainActivity.class);

        notificationIntent.putExtra(Constants.Notification.RADIO_TAB_KEY,
                Constants.MainPagerAdapter.UFSCAR_RADIO);

        notification.contentIntent = PendingIntent.getActivity(this, 0, notificationIntent,
                PendingIntent.FLAG_CANCEL_CURRENT);

        notification.flags |= Notification.FLAG_ONGOING_EVENT;

        startForeground(Constants.RADIO_NOTIFICATION_ID, notification);

    }

    /**
     * Dialog para ser mostrado quando houver algum problema com a rádio
     */
    public static Dialog radioFailedAlert(Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(R.string.radio_failed_alert_message)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
//                        Somente sai do alert
                    }
                });
        return builder.create();
    }

    /**
     * Verifica se é possível dar play na rádio e, se possível, toca a rádio
     */
    public static void playRadio(RadioService radioService) {
        MediaPlayer mediaPlayer = radioService.getMediaPlayer();
        if (mediaPlayer != null && !mediaPlayer.isPlaying()) radioService.playRadio();
    }

    /**
     * Verifica se é possível dar pause na rádio e, se possível, para a rádio
     */
    public static void pauseRadio(RadioService radioService) {
        MediaPlayer mediaPlayer = radioService.getMediaPlayer();
        if (mediaPlayer != null && mediaPlayer.isPlaying()) radioService.pauseRadio();
    }

    /**
     * Verifica se é possível interagir com a rádio e, se possível, toca a rádio se estiver
     * parada ou para a rádio se estiver tocando
     */
    public static void playOrPauseRadio(RadioService radioService) {
        MediaPlayer mediaPlayer = radioService.getMediaPlayer();
        if (mediaPlayer != null) {
            if (mediaPlayer.isPlaying()) radioService.pauseRadio();
            else radioService.playRadio();
        }
    }

    /**
     * Verifica se a rádio está criada e, se tiver, termina o serviço, liberando toda a memória
     * usada para tocar a rádio
     */
    public static void destroyRadio(RadioService radioService) {
        MediaPlayer mediaPlayer = radioService.getMediaPlayer();
        if (mediaPlayer != null) radioService.stopRadio();
    }
}