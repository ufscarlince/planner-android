package mobile.ufscar.br.app.ui;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;

import mobile.ufscar.br.app.R;
import mobile.ufscar.br.app.config.ConstantsActions;
import mobile.ufscar.br.app.logic.CacheManager;
import mobile.ufscar.br.app.logic.UfscarApplication;
import mobile.ufscar.br.app.model.ConfigurationModel;
import mobile.ufscar.br.app.model.EventModel;
import mobile.ufscar.br.app.utils.Utility;

/**
 * Fragment que mostra as preferências do usuário e dá a opção de restaurar o horário das
 * disciplinas
 */

public class ConfigFragment extends Fragment {

    SwitchCompat mNotification;
    SwitchCompat mDataUsage;
    TextView mResetEvent;
    TextView mLoginLogoutEvent;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_config, container, false);

        mNotification = (SwitchCompat) view.findViewById(R.id.cbNotification);
        ConfigurationModel configurationModel = getCacheManager().getConfigCache();
        mNotification.setChecked(configurationModel.isNotification());

        mNotification.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                CacheManager cacheManager = getCacheManager();
                ConfigurationModel configurationModel = cacheManager.getConfigCache();
                configurationModel.setNotification(isChecked);
                cacheManager.setConfigCache(configurationModel);
                if (isChecked) {
//                    Seta o alarme para o próximo periodo
                    getContext().sendBroadcast(
                            new Intent(ConstantsActions.MENU_IS_UP_TO_DATE_INTENT));
                }
            }
        });

        mDataUsage = (SwitchCompat) view.findViewById(R.id.cbDataUsage);
        mDataUsage.setChecked(configurationModel.isDataUsageBlocked());
        mDataUsage.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                CacheManager cacheManager = getCacheManager();
                ConfigurationModel configurationModel = cacheManager.getConfigCache();
                configurationModel.setDataUsageBlocked(isChecked);
                cacheManager.setConfigCache(configurationModel);
            }
        });

        mResetEvent = (TextView) view.findViewById(R.id.tvResetEvent);
        mResetEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utility.isMobileDataBlocked(getContext()))
                    getDataBlockedAlert(getDefaultAlert()).show();
                else
                    getDefaultAlert().show();
            }
        });

        mLoginLogoutEvent = (TextView) view.findViewById(R.id.tvLoginLogout);
        if (CacheManager.getInstance().getDefermentStatus()) {
            mLoginLogoutEvent.setText(R.string.config_logout_event);
            mLoginLogoutEvent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Utility.isMobileDataBlocked(getContext()))
                        getDataBlockedAlert(getLogoutAlert()).show();
                    else
                        getLogoutAlert().show();
                }
            });
        } else {
            mLoginLogoutEvent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(Utility.isMobileDataBlocked(getContext()))
                        getDataBlockedAlert(getLoginAlert()).show();
                    else
                        getLoginAlert().show();
                }
            });
        }

        return view;
    }

    private Context getApplication() {
        return UfscarApplication.getInstance();
    }

    private CacheManager getCacheManager() {
        return CacheManager.getInstance();
    }

    /**
     * PopUp pedindo confirmação de repopulação das células com os dados do SIGA
     *
     * @return AlertDialog com o pedido de confirmação
     */
    private AlertDialog getDefaultAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext(),
                R.style.DefaultDialogStyle);

        builder.setMessage(R.string.default_alert);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                CacheManager cacheManager = getCacheManager();
                cacheManager.setEventCache(new ArrayList<EventModel>());
                cacheManager.setLoginStatusUser(false);
                cacheManager.setDefermentStatus(false);
                Utility.updateWidget(getApplication());
                Intent intent = new Intent(getContext(), LoginActivity.class);
                startActivity(intent);
                getActivity().finish();
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        return builder.create();
    }

    /**
     * PopUp pedindo confirmação de Logout do usuário
     *
     * @return AlertDialog com o pedido de confirmação
     */
    private AlertDialog getLogoutAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext(),
                R.style.DefaultDialogStyle);

        builder.setMessage(R.string.logout_alert);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                CacheManager cacheManager = getCacheManager();
                cacheManager.setEventCache(new ArrayList<EventModel>());
                cacheManager.setLoginCache(null);
                cacheManager.setDefermentStatus(false);
                Utility.updateWidget(getApplication());
                Intent intent = new Intent(getContext(), LoginActivity.class);
                startActivity(intent);
                getActivity().finish();
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        return builder.create();
    }

    /**
     * PopUp pedindo confirmação de Logout do usuário
     *
     * @return AlertDialog com o pedido de confirmação
     */
    private AlertDialog getLoginAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext(),
                R.style.DefaultDialogStyle);

        builder.setMessage(R.string.login_alert);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                CacheManager cacheManager = getCacheManager();
                Utility.updateWidget(getApplication());
                Intent intent = new Intent(getContext(), LoginActivity.class);
                startActivity(intent);
                getActivity().finish();
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        return builder.create();
    }

    /**
     * PopUp pedindo confirmação da ativação do uso de dados pelo aplicativo
     *
     * @param secondAlert AlertDialog para exibição imediatamente após a confirmação
     * @return AlertDialog com o pedido de confirmação
     */
    private AlertDialog getDataBlockedAlert(final AlertDialog secondAlert) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext(),
                R.style.DefaultDialogStyle);

        builder.setMessage(R.string.data_usage_block_login_msg);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mDataUsage.setChecked(false);
                secondAlert.show();
            }
        });
        builder.setNegativeButton(R.string.cancel, null);
        return builder.create();
    }
}