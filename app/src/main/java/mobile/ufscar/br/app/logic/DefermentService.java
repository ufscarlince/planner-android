package mobile.ufscar.br.app.logic;

import org.json.JSONObject;
import java.util.Map;

/**
 * Interface da requisição para buscar o deferimento do SIGA
 */

public interface DefermentService {

    /**
     * Busca o deferimento do SIGA
     *
     * @param listener que contem os métodos para o caso de sucesso ou falha
     * @param headers da requisição
     * @param body da requisição, padrão null
     */
    void getDeferment(final EventServiceListener listener,
                      final Map<String, String> headers,
                      final JSONObject body);
}