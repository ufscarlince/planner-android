package mobile.ufscar.br.app.ui;

import android.support.v7.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Map;

import mobile.ufscar.br.app.R;
import mobile.ufscar.br.app.config.Constants;
import mobile.ufscar.br.app.logic.CacheManager;
import mobile.ufscar.br.app.logic.DefermentService;
import mobile.ufscar.br.app.logic.DefermentServiceImpl;
import mobile.ufscar.br.app.logic.EventServiceListener;
import mobile.ufscar.br.app.logic.LoginListener;
import mobile.ufscar.br.app.logic.LoginService;
import mobile.ufscar.br.app.logic.LoginServiceImpl;
import mobile.ufscar.br.app.logic.UfscarApplication;
import mobile.ufscar.br.app.logic.VolleyManager;
import mobile.ufscar.br.app.model.ConfigurationModel;
import mobile.ufscar.br.app.model.EventModel;
import mobile.ufscar.br.app.model.LoginResponseModel;
import mobile.ufscar.br.app.model.UfscarResponseError;
import mobile.ufscar.br.app.model.UfscarServerError;
import mobile.ufscar.br.app.utils.Utility;

/**
 * Classe que trata o Login efetuado pelo usuário
 */

// TODO: 18/8/16 - Serviço de deferimento está pronto, porém não pode ser testado
// TODO: 18/8/16 - API deferimento nao está pegando)

public class LoginActivityFragment extends Fragment implements LoginListener {

    private EditText mEtUsername;
    private EditText mEtPassword;
    private TextView mTvNoLogin;
    private Button mBtnLogin;
    private LoginService mLoginService;
    private JSONObject mLoginBody;
    private ProgressDialog mProgressDialog;
    private AlertDialog mDialogNoLogin;
    private ScrollView mScrollView;

    private Context getApplication() {
        return UfscarApplication.getInstance();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        if (!Utility.isMobileDataBlocked(getContext()))
            mLoginService = new LoginServiceImpl();
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_login, container, false);
        mScrollView = (ScrollView) rootView.findViewById(R.id.svFragmentLogin);
        mEtUsername = (EditText) rootView.findViewById(R.id.etUserName);
        mEtPassword = (EditText) rootView.findViewById(R.id.etPassword);
        mTvNoLogin = (TextView) rootView.findViewById(R.id.tvNoLogin);
        mBtnLogin = (Button) rootView.findViewById(R.id.btnLogin);

        mProgressDialog = new ProgressDialog(getContext());
        mProgressDialog.setIndeterminateDrawable(getResources().
                getDrawable(R.drawable.red_progress_bar));
        mProgressDialog.setIndeterminate(true);

        mProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                VolleyManager.getInstance().cancelAll(Constants.REQUEST_DEFAULT_TAG);
                cleanDataLogin();
            }
        });

        mDialogNoLogin = new AlertDialog.Builder(getContext(), R.style.AlertNoLoginStyle).create();
        mDialogNoLogin.setButton(AlertDialog.BUTTON_NEGATIVE, getApplication().getResources().
                getString(R.string.ok),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        cleanDataLogin();
                    }
                });
        mDialogNoLogin.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                cleanDataLogin();
            }
        });

        mEtUsername.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                mEtUsername.setFocusableInTouchMode(true);
                return false;
            }
        });

        mEtPassword.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                mEtPassword.setFocusableInTouchMode(true);
                return false;
            }
        });

        mTvNoLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callHomeTab();
            }
        });

        mBtnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Verificando se os campos estão preenchidos em frontend
//                Campos em branco ou somente com espaços são invalidos
                if (mEtUsername.getText().toString().equals("") ||
                        mEtUsername.getText().toString().trim().isEmpty()) {

                    Utility.showSnackbar(mScrollView, getApplication().getResources().
                            getString(R.string.login_fail_input_user_name));

                } else if (mEtPassword.getText().toString().equals("") ||
                        mEtPassword.getText().toString().trim().isEmpty()) {

                    Utility.showSnackbar(mScrollView, getApplication().getResources().
                            getString(R.string.login_fail_input_password));

                } else {
//                    Verifica se o uso dos Dados está desabilitado
                    if (Utility.isMobileDataBlocked(getContext())) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(getContext(),
                                R.style.DefaultDialogStyle);
                        builder.setMessage(R.string.data_usage_block_login_msg);
                        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                CacheManager cacheManager = CacheManager.getInstance();
                                ConfigurationModel configurationModel = cacheManager
                                        .getConfigCache();
                                configurationModel.setDataUsageBlocked(false);
                                cacheManager.setConfigCache(configurationModel);

                                final String userName = mEtUsername.getText().toString();
                                final String password = mEtPassword.getText().toString();
//                    Corpo da requisição a ser feita(JSONObject a ser passado)
                                mLoginBody = loginBody(userName, password);
                                mProgressDialog.setMessage(getApplication().getResources().
                                        getString(R.string.authenticating));
                                mProgressDialog.show();
                                getLoginToken();
                            }
                        });
                        builder.setNegativeButton(R.string.cancel, null);
                        builder.show();
                    } else {
                        final String userName = mEtUsername.getText().toString();
                        final String password = mEtPassword.getText().toString();
//                    Corpo da requisição a ser feita(JSONObject a ser passado)
                        mLoginBody = loginBody(userName, password);
                        mProgressDialog.setMessage(getApplication().getResources().
                                getString(R.string.authenticating));
                        mProgressDialog.show();
                        getLoginToken();
                    }
                }
            }
        });
        return rootView;
    }

    /**
     * Método que retorna o padrão JsonObject utilizado no serviço de Login
     * Formato JSONObject: {"password": "RA/CPF", "username": "SENHA"}
     *
     * @param userName String com o userName do usuário
     * @param password String com o password do usuário
     * @return JSONObject da requisição com o Token
     */
    private JSONObject loginBody(String userName, String password) {
//        JsonObject que é passado na requisição
        JSONObject jsonObjectUser = new JSONObject();

//        Tratamento de possíveis excecoes
        try {
            jsonObjectUser.put(Constants.USER_NAME_LOGIN, userName);
            jsonObjectUser.put(Constants.USER_PASSWORD_LOGIN, password);
        } catch (JSONException e) {
            Log.d(Constants.JSON_LOGIN_ERROR_TAG, e.getMessage());
        }
        return jsonObjectUser;
    }

    /**
     * Método que limpa os dados digitados pelo usuário em
     * {@link mobile.ufscar.br.app.R.layout#fragment_login}
     * quando esse não consegui realizar a autenticação e algum erro é
     * apresentado
     */
    private void cleanDataLogin() {
        mEtPassword.setText(null);
        mDialogNoLogin.dismiss();
        mProgressDialog.dismiss();
        mEtUsername.setFocusable(false);
        mEtPassword.setFocusable(false);
    }

    /**
     * Método que chama a requisição do Login
     */
    void getLoginToken() {
//        Passando os parametros para implementação do método login, da interface
        mLoginService.getLoginToken(this, mLoginBody);
    }

    /**
     * Método que chama a {@link MainActivity}
     * Altera no {@link CacheManager#setLoginStatusUser(boolean)} (boolean)} para {@link Boolean#FALSE}
     * para assim no próximo acesso do aplicativo não aparecer assim a tela
     * {@link LoginActivityFragment}
     */
    private void callHomeTab() {
        Intent intent = new Intent(getContext(), MainActivity.class);
        CacheManager.getInstance().setLoginStatusUser(true);
        startActivity(intent);
        getActivity().finish();
    }

    /**
     * Método chamado quando não foi possível fazer o login ou não foi possível carregar o
     * deferimento
     *
     * @param error Erro que ocorreu na requisição, pode ser na requisição do login ou na
     *              do Deferimento
     * @see #loginFailed(UfscarResponseError)
     * @see #defermentService()
     * @see #mDialogNoLogin
     */
    private void alertNoLogin(UfscarResponseError error) {
        List<UfscarServerError> errorList = error.getServerErrorList();

        if (errorList.size() > 0) {
            mDialogNoLogin.setMessage(errorList.get(0).getMessage());
        } else {
            mDialogNoLogin.setMessage(getApplication().getResources().
                    getString(R.string.generic_error));
        }
        mDialogNoLogin.show();
    }

    /**
     * Método utilizado para chamar o serviço que busca o deferimento do SIGA
     *
     * @see DefermentService
     * @see DefermentServiceImpl
     */
    private void defermentService() {

        mProgressDialog.setMessage(getApplication().getResources().
                getString(R.string.deferment_loading));

        Map<String, String> mHeaders = Utility.getAuthorizationHeader();

        EventServiceListener eventServiceListener = new EventServiceListener() {
            @Override
            public void loadCompleted(List<EventModel> eventModelList) {
//                Cache atualizado
                CacheManager.getInstance().setEventCache(eventModelList);
                CacheManager.getInstance().setDefermentStatus(true);
                Utility.updateWidget(getApplication());
                mProgressDialog.dismiss();
                callHomeTab();
            }

            @Override
            public void loadFailed(UfscarResponseError error) {
                alertNoLogin(error);
            }
        };
        DefermentService defermentService = new DefermentServiceImpl();
        defermentService.getDeferment(eventServiceListener, mHeaders, null);
    }

    /**
     * No caso de sucesso na autenticação do usuário
     * Salva o Token de acesso no Cache
     *
     * @param loginResponseModel model a ser salvo no Cache
     * @see LoginResponseModel
     * @see CacheManager#setLoginCache(LoginResponseModel)
     */
    @Override
    public void loginSuccessful(LoginResponseModel loginResponseModel) {
        CacheManager.getInstance().setLoginCache(loginResponseModel);
        defermentService();
    }

    /**
     * Insucesso na autenticação do usuário
     *
     * @param error Erro que ocorreu na requisição. Sendo tanto um erro retornado pelo servidor ou
     *              algum erro ocorrido internamente durante a requisição.
     * @see UfscarResponseError
     */
    @Override
    public void loginFailed(UfscarResponseError error) {
        alertNoLogin(error);
    }
}