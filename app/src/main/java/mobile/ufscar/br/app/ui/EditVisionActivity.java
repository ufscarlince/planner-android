package mobile.ufscar.br.app.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageButton;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import java.util.ArrayList;
import java.util.List;

import mobile.ufscar.br.app.R;
import mobile.ufscar.br.app.config.Constants;
import mobile.ufscar.br.app.logic.CacheLocation;
import mobile.ufscar.br.app.logic.CacheManager;
import mobile.ufscar.br.app.logic.Manager;

import mobile.ufscar.br.app.model.EventModel;
import mobile.ufscar.br.app.model.LocalModel;
import mobile.ufscar.br.app.utils.Utility;

/**
 * Activity utilizada para a criação/edição de eventos provenientes de {@link DailyVisionFragment}.
 * É iniciada de duas formas: ao tocar em uma célula vazia e ao tocar e manter em uma célula com
 * evento e selecionar a opção Editar em {@link DailyVisionTabFragment}. Possui um
 * {@link AutoCompleteTextView} populado de duas formas: quando qualquer campus é selecionado, é
 * populado com os locais correspondentes ao campus encontrados em {@link CacheLocation}. Quando a
 * opção "Outros" é selecionada, é populado com os resultados da busca utilizando GooglePlaces API.
 */

public class EditVisionActivity extends AppCompatActivity {

    private static final String LOG_TAG = EditVisionActivity.class.getName();
    public static final String DAY = "dia";
    public static final String START = "start";
    public static final String END = "end";
    public static final String EVENT_NUMBER = "eventNumber";
    public static final String PLACE_PICK = "placePick";
    public static final String NONE = "Nenhum";
    private int mEventDay;
    private int mEnd;
    private int mStart;
    private int mFirstAvailableHour;
    private int mLastAvailableHour;
    private int mEditStartTime = -1;
    private int mEditFinishTime = -1;
    private int mEventNumber;
    private EventModel mEventEdit;
    private boolean mEventType;
    private String mDayName;
    private String mCampusName;
    ArrayAdapter<String> mAdapterLocation;

    private Toolbar mToolbar;
    private ImageButton mEditCancel;
    private TextView mEditSave;

    private TextView mWeekDay;
    private TextInputEditText mEventName;
    private TextInputLayout mEventNameLayout;
    private Spinner mStartTime;
    private Spinner mFinishTime;
    private Spinner mCampus;
    private AutoCompleteTextView mLocation;
    private ImageButton mShowOnMap;
    private TextInputEditText mComplement;
    private TextInputLayout mComplementLayout;
    private ScrollView mScrollView;

    GoogleApiClient mGoogleApiClient;
    private PlacesAutoCompleteAdapter mPlacesAdapter;
    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback;
    private static final String PLACE_LOG_TAG = "Place";
    private static final String PLACE_QUERY_NOT_COMPLETED_MESSAGE =
            "Consulta de local não foi concluída. Erro: ";
    private static final int REQUEST_PLACE_PICKER = 1;
    private static final int REQUEST_MAP_PICKER = 2;
    private double mLatitudeSpecific = Constants.MapFragment.UFSCAR_LATITUDE;
    private double mLongitudeSpecific = Constants.MapFragment.UFSCAR_LONGITUDE;
    private static final LatLngBounds BOUNDS_UFSCAR = new LatLngBounds(
            new LatLng(Constants.SaoCarlosBounds.SAO_CARLOS_SOUTH_LATITUDE,
                    Constants.SaoCarlosBounds.SAO_CARLOS_WEST_LOGINTUDE),
            new LatLng(Constants.SaoCarlosBounds.SAO_CARLOS_NORTH_LATITUDE,
                    Constants.SaoCarlosBounds.SAO_CARLOS_EAST_LONGITUDE));


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Places.GEO_DATA_API)
                .build();

        setContentView(R.layout.activity_edit_vision);

        mToolbar = (Toolbar) findViewById(R.id.tbEditVision);
        setSupportActionBar(mToolbar);

        mEditCancel = (ImageButton) findViewById(R.id.ibEditCancel);
        mEditSave = (TextView) findViewById(R.id.tvEditSave);

        mScrollView = (ScrollView) findViewById(R.id.svActivityEditVision);

        mWeekDay = (TextView) findViewById(R.id.tvWeekDay);

        mEventName = (TextInputEditText) findViewById(R.id.tiedtEventName);
        mEventNameLayout = (TextInputLayout) findViewById(R.id.tilEventName);

        mStartTime = (Spinner) findViewById(R.id.spnStartTime);
        mFinishTime = (Spinner) findViewById(R.id.spnFinishTime);

        mCampus = (Spinner) findViewById(R.id.spnCampus);

        mLocation = (AutoCompleteTextView) findViewById(R.id.actvLocation);
        mShowOnMap = (ImageButton) findViewById(R.id.ibLocation);

        mComplement = (TextInputEditText) findViewById(R.id.tiedtComplement);
        mComplementLayout = (TextInputLayout) findViewById(R.id.tilComplement);

        mComplement.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                mComplement.setFocusableInTouchMode(true);
                mEventName.setFocusableInTouchMode(true);
                mLocation.setFocusableInTouchMode(true);
                return false;
            }
        });

        mLocation.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                mLocation.setFocusableInTouchMode(true);
                mComplement.setFocusableInTouchMode(true);
                mEventName.setFocusableInTouchMode(true);
                return false;
            }
        });

        mEventName.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                mEventName.setFocusableInTouchMode(true);
                mLocation.setFocusableInTouchMode(true);
                mComplement.setFocusableInTouchMode(true);
                return false;
            }
        });


//        Usado para determinar se e uma edicao ou a criacao de um novo evento.
        mEventType = false;

        ArrayAdapter<CharSequence> adapterCampi = ArrayAdapter.createFromResource(this,
                R.array.campi_array, android.R.layout.simple_spinner_item);
        adapterCampi.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mCampus.setAdapter(adapterCampi);
        mCampus.setSelection(Constants.SAO_CARLOS);

//        Verifica se o intent possui um dia,
//        o que significa que trata se da criacao de um novo evento.
        if (intent != null && intent.hasExtra(DAY)) {
            mEventDay = intent.getIntExtra(DAY, -1);
            ChangeLocationMethod(true);
            mEventNumber = -1;
        }
//        Verifica se o intent possui horario de inicio e fim,
//        o que significa que trata se da criacao de um novo evento.
        if (intent != null && intent.hasExtra(START) && intent.hasExtra(END)) {
            mFirstAvailableHour = intent.getIntExtra(START, 0);
            mLastAvailableHour = intent.getIntExtra(END, 23);
//            Preenche as primeiras opcoes dos spinners de horario de acordo com os parametros da
//            célula vazia selecionada.
            scheduleStartTime(mFirstAvailableHour, mLastAvailableHour);
            scheduleFinishTime(mFirstAvailableHour, mLastAvailableHour);
        }

//        Verifica se o intent possui o numero do evento a ser editado.
        if (intent != null && intent.hasExtra(EVENT_NUMBER)) {
            mEventNumber = intent.getIntExtra(EVENT_NUMBER, 0);
            mEventType = true;

//            Recupera as informações do evento a ser editado.
            mEventEdit = Manager.getEventInPosition(mEventNumber);
            mEventDay = mEventEdit.getSchedule().getWeekDay();
            mStart = mEventEdit.getSchedule().getStartTime();
            mEnd = mEventEdit.getSchedule().getFinishTime();
            int eventCampus = mEventEdit.getLocal().getCampus();

//            Encontra os horarios disponiveis para o usuario escolher
//            antes do horario original do evento.
            EventModel auxEvent;
            int index = 0;
            List<EventModel> itemsDia = Manager.getEventDaily(mEventDay);
            itemsDia = Manager.getEventEmpty(itemsDia, mEventDay);
            auxEvent = itemsDia.get(index);

            while ((index < itemsDia.size() - 1) &&
                    auxEvent.getSchedule().getFinishTime() != mStart) {
                index++;
                auxEvent = itemsDia.get(index);
            }
            if (auxEvent.getSchedule().getFinishTime() == mStart) {
                if (!auxEvent.isRealEvent()) {
                    mFirstAvailableHour = auxEvent.getSchedule().getStartTime();
                } else {
                    mFirstAvailableHour = mStart;
                }
            } else {
                mFirstAvailableHour = mStart;
            }
//            Encontra os horarios disponiveis para o usuario escolher
//            depois do horario original do evento.
            index = 0;
            auxEvent = itemsDia.get(index);

            while ((index < itemsDia.size() - 1) && auxEvent.getSchedule().getStartTime() != mEnd) {
                index++;
                auxEvent = itemsDia.get(index);
            }
            if (auxEvent.getSchedule().getStartTime() == mEnd) {
                if (!auxEvent.isRealEvent()) {
                    mLastAvailableHour = auxEvent.getSchedule().getFinishTime();
                } else {
                    mLastAvailableHour = mEnd;
                }
            } else {
                mLastAvailableHour = mEnd;
            }
            mEditStartTime = mStart - mFirstAvailableHour;
            mEditFinishTime = mEnd - mStart - 1;

//            Chama a funcao que preenche o evento com os dados do evento a ser editado.
            completeEvent();
        }

        switch (mEventDay) {
            case Constants.SEGUNDA:
                mDayName = "Segunda";
                break;
            case Constants.TERCA:
                mDayName = "Terça";
                break;
            case Constants.QUARTA:
                mDayName = "Quarta";
                break;
            case Constants.QUINTA:
                mDayName = "Quinta";
                break;
            case Constants.SEXTA:
                mDayName = "Sexta";
                break;
            case Constants.SABADO:
                mDayName = "Sábado";
                break;
            case Constants.DOMINGO:
                mDayName = "Domingo";
                break;
            default:
                Log.w(LOG_TAG, Constants.EditVision.DEFAULT_WEEK_DAY);
        }

        if (mEventDay == Constants.SABADO || mEventDay==Constants.DOMINGO)
            mWeekDay.setText(mDayName);
        else
            mWeekDay.setText(mDayName+"-Feira");


        mCampus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (mCampus.getSelectedItem().toString().equals(NONE)) {
                    ChangeLocationMethod(false);
                    if (mEventType) {
                        completeEvent();
                        mEventType = false;
                    }
                } else {
                    ChangeLocationMethod(true);
                    if (mEventType) {
                        completeEvent();
                        mEventType = false;
                    }
                }
                mCampusName = mCampus.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

//        Controla as opcoes do FinishTime spinner de acordo com a seleção do StartTime spinner
        mStartTime.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String starting = mStartTime.getSelectedItem().toString();
                scheduleFinishTime(Integer.parseInt(starting.substring(0, 2)), mLastAvailableHour);
                if (mStart == Integer.parseInt(starting.substring(0, 2))) {
                    mFinishTime.setSelection(mEditFinishTime, false);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                int a=3;
            }
        });

        mEditCancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                finish();
            }
        });

        mEditSave.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String startTime = mStartTime.getSelectedItem().toString();
                String finishTime = mFinishTime.getSelectedItem().toString();
                int campus = (int) mCampus.getSelectedItemId();
                LocalModel localModel;
                String name = mEventName.getText().toString();
                String day = mDayName;
                String complement = mComplement.getText().toString();

                if (mCampusName.equals(NONE)) {
                    localModel = new LocalModel(mLocation.getText().toString(),
                            complement, Constants.OUTROS, mLatitudeSpecific, mLongitudeSpecific);
                } else {
                    localModel = nameToLocal(mLocation.getText().toString(),
                            complement, campus);
                }

                if (name.equals("") || name.trim().isEmpty() || name.length() == 0) {
                    mEventNameLayout.setError(Constants.EditVision.BLANK_EVENT_NAME_WARNING);
                    mEventName.requestFocus();
                } else {
                    try {
                        EventModel eventModel = Utility.toEventModel(name, localModel, day,
                                startTime, finishTime);
                        if (mEventNumber == -1) {
                            CacheManager.getInstance().addEventCache(eventModel);
                        } else {
                            CacheManager.getInstance().editEventCache(mEventNumber,
                                    eventModel);
                        }
                        Utility.updateWidget(getApplicationContext());
                    } catch (NumberFormatException e) {
                        Log.w(LOG_TAG, Constants.EditVision.INCORRECT_STRING_FORMAT, e);
                    }
                    onBackPressed();
                }
            }
        });

//        Caso a opção "Outros" esteja selecionada, o botão iniciará o PlacePicker, caso contrário,
//        iniciará ShowMapActivity com o local escolhido marcado
        mShowOnMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Utility.isMobileDataBlocked(getApplicationContext()) &&
                        Utility.checkConnection(getApplicationContext())) {
                    if (!mCampus.getSelectedItem().toString().equals(NONE)) {
                        LocalModel local = CacheLocation.getInstance().getSingleLocal(
                                mLocation.getText().toString(), (int) mCampus.getSelectedItemId());

                        Intent intent = new Intent(getBaseContext(), MapActivity.class);
                        intent.putExtra(MapFragment.LOCATION_NAME_KEY,
                                local.getName());
                        intent.putExtra(MapFragment.LOCATION_LATITUDE_KEY,
                                local.getLatitude());
                        intent.putExtra(MapFragment.LOCATION_LONGITUDE_KEY,
                                local.getLongitude());
                        int campusId = (int) mCampus.getSelectedItemId();
                        intent.putExtra(MapFragment.CAMPUS_NAME_KEY, campusId);
                        startActivityForResult(intent, REQUEST_MAP_PICKER);
                    } else {
                        try {
                            PlacePicker.IntentBuilder intentBuilder =
                                    new PlacePicker.IntentBuilder();

                            Intent intent = intentBuilder.build(EditVisionActivity.this);
                            startActivityForResult(intent, REQUEST_PLACE_PICKER);

                        } catch (GooglePlayServicesRepairableException e) {
                            // ...
                        } catch (GooglePlayServicesNotAvailableException e) {
                            // ...
                        }
                    }
                }
            }
        });

        if(Utility.isMobileDataBlocked(getApplicationContext()) ||
                !Utility.checkConnection(getApplicationContext())) {
            mShowOnMap.setVisibility(View.GONE);
        }

        mEventName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                    mEventNameLayout.setCounterEnabled(true);
                else mEventNameLayout.setCounterEnabled(false);
            }
        });

        mComplement.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                    mComplementLayout.setCounterEnabled(true);
                else mComplementLayout.setCounterEnabled(false);
            }
        });

        mEventName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(!(mEventName.getText().toString().length() == 0))
                    mEventNameLayout.setError("");
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    /**
     * Controla as opcoes da spinner mSpinnerStartTime,
     * recebe o horário de início e final de um evento (int Inicio, int Fim).
     */
    void scheduleStartTime(int starTimeS, int startTimeF) {
        ArrayList<String> schedule = new ArrayList<>();
        for (int i = starTimeS; i < startTimeF; i++) {
            schedule.add(Utility.hourToString(i) + ":00");
        }

        ArrayAdapter<String> adapterStart = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, schedule);
        adapterStart.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mStartTime.setAdapter(adapterStart);
    }

    /**
     * Controla as opcoes da spinner mSpinnerFinishTime,
     * recebe o horário de início e final de um evento (int Inicio, int Fim).
     */
    void scheduleFinishTime(int finishTimeS, int finishTimeF) {
        ArrayList<String> schedule = new ArrayList<>();
        for (int i = (finishTimeS + 1); i < finishTimeF; i++) {
            schedule.add(Utility.hourToString(i) + ":00");
        }
//        Atribuindo a ultima posição do array corretamente, tratando o caso (24:00).
        if (finishTimeF == 24) {
            schedule.add(Utility.hourToString(23) + ":59");
        } else {
            schedule.add(Utility.hourToString(finishTimeF) + ":00");
        }

        ArrayAdapter<String> adapterStart = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, schedule);
        adapterStart.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mFinishTime.setAdapter(adapterStart);
    }

    /**
     * Controla com quais dados o {@link AutoCompleteTextView} mLocation será populado,
     * com os dados de {@link CacheLocation} ou os resultados da GooglePlaces API.
     */
    void ChangeLocationMethod(boolean isDefault) {
        if(!mEventType)
            mLocation.setText("");
        
        if (isDefault) {
            if (mGoogleApiClient.isConnected())
                mGoogleApiClient.disconnect();

            mAdapterLocation = new ArrayAdapter<>(
                    EditVisionActivity.this, android.R.layout.simple_spinner_item,
                    getDefaultLocations(mCampus.getSelectedItem().toString()));

            mAdapterLocation.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mLocation.setAdapter(mAdapterLocation);
        }
        else {
            if(!Utility.checkConnection(getApplicationContext())) {
                List<String> list = new ArrayList<>();
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                        android.R.layout.simple_dropdown_item_1line, list);
                mLocation.setAdapter(adapter);
                Utility.showSnackbar(mScrollView, getResources()
                        .getString(R.string.no_connection_error));
            } else if (!Utility.isMobileDataBlocked(getApplicationContext())) {
                mGoogleApiClient.connect();
                mUpdatePlaceDetailsCallback = new ResultCallback<PlaceBuffer>() {
                    @Override
                    public void onResult(PlaceBuffer places) {
                        if (!places.getStatus().isSuccess()) {
                            Log.e(PLACE_LOG_TAG, PLACE_QUERY_NOT_COMPLETED_MESSAGE +
                                    places.getStatus().toString());
                            return;
                        }
//                    Seleciona o primeiro objeto do buffer.
                        final Place place = places.get(0);
                        LatLng latlongSpecific = place.getLatLng();
                        mLatitudeSpecific = latlongSpecific.latitude;
                        mLongitudeSpecific = latlongSpecific.longitude;
                    }
                };

                mPlacesAdapter = new PlacesAutoCompleteAdapter(this,
                        android.R.layout.simple_list_item_1, mGoogleApiClient,
                        BOUNDS_UFSCAR, null, mScrollView);

                mLocation.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        final PlacesAutoCompleteAdapter.PlaceAutocomplete item =
                                mPlacesAdapter.getItem(position);
                        final String placeId = String.valueOf(item.placeId);
                        PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                                .getPlaceById(mGoogleApiClient, placeId);
                        placeResult.setResultCallback(mUpdatePlaceDetailsCallback);
                    }
                });
                mLocation.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        if (!mGoogleApiClient.isConnected()) {
                            mGoogleApiClient.connect();
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                    }
                });
                mLocation.setAdapter(mPlacesAdapter);
            } else {
                List<String> list = new ArrayList<>();
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                        android.R.layout.simple_dropdown_item_1line, list);
                mLocation.setAdapter(adapter);
                Utility.showSnackbar(mScrollView, getResources()
                        .getString(R.string.data_usage_blocked_dragon_msg));
            }
        }
    }

    /**
     * Preenche a activity EditVision com os dados de um evento, no caso da Editar evento
     * recebe void.
     */
    void completeEvent() {
        mEventDay = mEventEdit.getSchedule().getWeekDay();
        mEventName.setText(mEventEdit.getName());
        mStart = mEventEdit.getSchedule().getStartTime();
        mEnd = mEventEdit.getSchedule().getFinishTime();
        mLatitudeSpecific = mEventEdit.getLocal().getLatitude();
        mLongitudeSpecific = mEventEdit.getLocal().getLongitude();

        int eventCampus = mEventEdit.getLocal().getCampus();
        mCampus.setSelection(eventCampus);
        mComplement.setText(mEventEdit.getLocal().getComplement());
        mLocation.setText(mEventEdit.getLocal().getName());
        scheduleStartTime(mFirstAvailableHour, mLastAvailableHour);
        scheduleFinishTime(mFirstAvailableHour, mLastAvailableHour);
        mStartTime.setSelection(mEditStartTime);

        String starting = mStartTime.getSelectedItem().toString();
        scheduleFinishTime(Integer.parseInt(starting.substring(0, 2)), mLastAvailableHour);
        if (mStart == Integer.parseInt(starting.substring(0, 2))) {
            mFinishTime.setSelection(mEditFinishTime, false);
        }
    }

    /**
     * Transforma uma lista de LocalModel em uma lista com os nomes desses LocalModel.
     */
    private List<String> getNameLocations(List<LocalModel> list) {
        List<String> strings = new ArrayList<>();

        for (int i = 0; i < list.size(); i++) {
            strings.add(list.get(i).getName());
        }

        return strings;
    }

    /**
     * Pega a lista de locais para ser usada no spinner.
     */
    private List<String> getDefaultLocations(String campus) {
        String[] possibleCampi = getResources().getStringArray(R.array.campi_array);
        if (campus.equals(possibleCampi[0])) {
            if (CacheLocation.isCampus(Constants.ARARAS)) {
                return getNameLocations(CacheLocation.getInstance().getLocations(Constants.ARARAS));
            }
        }

        if (campus.equals(possibleCampi[1])) {
            if (CacheLocation.isCampus(Constants.LAGOA_DO_SINO)) {
                return getNameLocations(CacheLocation.getInstance().
                        getLocations(Constants.LAGOA_DO_SINO));
            }
        }

        if (campus.equals(possibleCampi[2])) {
            if (CacheLocation.isCampus(Constants.SAO_CARLOS)) {
                return getNameLocations(CacheLocation.getInstance().
                        getLocations(Constants.SAO_CARLOS));
            }
        }
//        Sorocaba é o único que falta.
        return getNameLocations(CacheLocation.getInstance().getLocations(Constants.SOROCABA));
    }

    /**
     * Procura na lista de locais o LocalModel com o nome passado no parâmetro.
     */
    private LocalModel nameToLocal(String name, String complement, int campus) {
        List<LocalModel> localModels = new ArrayList<>();
        switch (campus) {
            case Constants.ARARAS: {
                if (CacheLocation.isCampus(Constants.ARARAS)) {
                    localModels = CacheLocation.getInstance().getLocations(Constants.ARARAS);
                }
                break;
            }
            case Constants.LAGOA_DO_SINO: {
                if (CacheLocation.isCampus(Constants.LAGOA_DO_SINO)) {
                    localModels = CacheLocation.getInstance().getLocations(Constants.LAGOA_DO_SINO);
                }
                break;
            }
            case Constants.SAO_CARLOS: {
                if (CacheLocation.isCampus(Constants.SAO_CARLOS)) {
                    localModels = CacheLocation.getInstance().getLocations(Constants.SAO_CARLOS);
                }
                break;
            }
            case Constants.SOROCABA: {
                if (CacheLocation.isCampus(Constants.SOROCABA)) {
                    localModels = CacheLocation.getInstance().getLocations(Constants.SOROCABA);
                }
                break;
            }
        }

        for (int i = 0; i < localModels.size(); i++) {
            if (name.equals(localModels.get(i).getName())) {
                LocalModel localModel = localModels.get(i);
                localModel.setComplement(complement);
                return localModel;
            }
        }

        return new LocalModel(name, complement, campus,
                Constants.MapFragment.UFSCAR_LATITUDE, Constants.MapFragment.UFSCAR_LONGITUDE);
    }

    /**
     * Função iniciada após o PlacePicker fechar.
     * Adquire o nome, endereço e localização (latitude e longitude) do local selecionado.
     */
    @Override
    protected void onActivityResult(int requestCode,
                                    int resultCode, Intent data) {

        if (requestCode == REQUEST_PLACE_PICKER && resultCode == Activity.RESULT_OK) {
//            O usuário escolhe um local. Extrai o nome e endereço.
            final Place place = PlacePicker.getPlace(data, this);

            final String name = place.getName().toString();
            final String address = place.getAddress().toString();
            String attributions = PlacePicker.getAttributions(data);
            if (attributions == null) {
                attributions = "";
            }

            LatLng latlongSpecific = place.getLatLng();
            mLatitudeSpecific = latlongSpecific.latitude;
            mLongitudeSpecific = latlongSpecific.longitude;

            mLocation.setText(name + " " + address);

        } else {
            if(requestCode == REQUEST_MAP_PICKER && resultCode == Activity.RESULT_OK) {
                if(data != null && data.hasExtra(PLACE_PICK)) {
                    mLocation.setText(data.getStringExtra(PLACE_PICK));
                }
            } else {
                super.onActivityResult(requestCode, resultCode, data);
            }
        }
    }

    @Override
    protected void onStart() {
        if (mEventType && mCampus.getSelectedItem().toString().equals(NONE))
            mGoogleApiClient.connect();

        super.onStart();
    }

    @Override
    protected void onStop() {
        if (mGoogleApiClient.isConnected())
            mGoogleApiClient.disconnect();

        super.onStop();
    }
}