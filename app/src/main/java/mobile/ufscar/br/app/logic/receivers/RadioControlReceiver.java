package mobile.ufscar.br.app.logic.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.util.Log;
import android.view.KeyEvent;

import mobile.ufscar.br.app.logic.RadioService;

/**
 * Um {@link BroadcastReceiver} que lida com tudo que acontece com {@link RadioService} através
 * da notificação ou entrada do usuário
 */

public class RadioControlReceiver extends BroadcastReceiver {

    private static final String LOG_TAG = RadioControlReceiver.class.getName();

    public RadioControlReceiver() {
        super();
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        RadioService.RadioBinder radioBinder = (RadioService.RadioBinder) peekService(context,
                new Intent(context, RadioService.class));
        if (radioBinder != null) {
            RadioService radioService = radioBinder.getService();

            switch (intent.getAction()) {
                case Intent.ACTION_MEDIA_BUTTON:
                    KeyEvent keyEvent = intent.getParcelableExtra(Intent.EXTRA_KEY_EVENT);
                    if (keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
                        switch (keyEvent.getKeyCode()) {
                            case KeyEvent.KEYCODE_MEDIA_PLAY:
                                RadioService.playRadio(radioService);
                                break;
                            case KeyEvent.KEYCODE_MEDIA_PAUSE:
                                RadioService.pauseRadio(radioService);
                                break;
                            case KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE:
                                RadioService.playOrPauseRadio(radioService);
                                break;
                            case KeyEvent.KEYCODE_MEDIA_STOP:
                                RadioService.destroyRadio(radioService);
                                break;
                            case KeyEvent.KEYCODE_HEADSETHOOK:
                                RadioService.playOrPauseRadio(radioService);
                                break;
                            default:
//                                Nenhuma outra ação será tratada, as acões ignoradas são:
//                                KeyEvent.KEYCODE_MUTE, KeyEvent.KEYCODE_MEDIA_FAST_FORWARD
//                                KeyEvent.KEYCODE_MEDIA_PREVIOUS, KeyEvent.KEYCODE_MEDIA_REWIND,
//                                KeyEvent.KEYCODE_MEDIA_RECORD, KeyEvent.KEYCODE_MEDIA_NEXT
                        }
                    }
                    break;
                case RadioService.PLAY_INTENT:
                    RadioService.playRadio(radioService);
                    radioService.showNotification(true);
                    break;
                case RadioService.PAUSE_INTENT:
                    RadioService.pauseRadio(radioService);
                    radioService.showNotification(false);
                    break;
                case RadioService.DESTROY_INTENT:
                    RadioService.destroyRadio(radioService);
                    break;
                case AudioManager.ACTION_AUDIO_BECOMING_NOISY:
                    RadioService.pauseRadio(radioService);
                    break;
                default:
//                    Nenhuma outra ação será tratada
                    Log.d(LOG_TAG, "Unexpected action: " + intent.getAction());
            }
        }
    }
}